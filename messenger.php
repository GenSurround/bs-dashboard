<?php include 'includes/_head.php'; ?>
<?php 
    if($_GET["p"] != "") {
        $page = $_GET["p"]; 
    } else {
       $page = 'messenger_main_file' ;
    }
?>
<body  class="dashboard dashboard-messenger">
    <?php include 'includes/_header.php'; ?>
    <?php include 'includes/_top-bar.php'; ?>
    <div  class="bs-site-wrapper">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                     <div class="messenger-container">
                        <?php include 'pages/_messenger/_'.$page.'.php'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'includes/_footer.php'; ?>