var map;
var windowWidth = window.innerWidth;

function initialize() {

    var mapOptions = {
        zoom: 4,
        center: new google.maps.LatLng(41.850033, -87.6500523),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map-canvas-locations"), mapOptions);

    var locations = [
        ['Newport Beach, CA', 33.621081, -117.930103, 0],
        ['San Rafael, CA', 37.971360, -122.505240, 1],
        ['Aventura, FL', 25.962177, -80.147306, 2],
        ['Boynton Beach, FL', 26.514717, -80.146026, 3],
        ['Coconut Grove, FL', 25.730725, -80.241480, 4],
        ['Dania, FL', 26.052420, -80.148660, 5],
        ['Delray Beach, FL', 26.462580, -80.065020, 6],
        ['Fort Lauderdale, FL', 26.103077, -80.125128, 7],
        ['Hobe Sound, FL', 27.075444, -80.142707, 8],
        ['Homestead, FL', 25.529400, -80.387880, 9],
        ['Hypoluxo, FL', 26.575320, -80.076780, 10],
        ['Islamorada, FL', 24.922935, -80.631906, 11],
        ['Jupiter, FL', 26.934810, -80.117318, 12],
        ['Key Biscayne, FL', 25.708980, -80.177520, 13],
        ['Key Largo, FL', 25.130000, -80.410000, 14],
        ['Lantana, FL', 26.575320, -80.076780, 15],
        ['Lighthouse Point, FL', 26.277900, -80.147820, 16],
        ['Miami, FL', 25.765925, -80.191087, 17],
        ['Miami Beach, FL', 25.816597, -80.126961, 18],
        ['Naples, FL', 26.188770, -81.805360, 19],
        ['New Port Richey, FL', 28.236773, -82.728887, 20],
        ['North Miami Beach, FL', 25.940364, -80.139706, 21],
        ['North Palm Beach, FL', 26.851500, -80.062680, 22],
        ['Palm Beach Gardens, FL', 26.844420, -80.089320, 23],
        ['Palmetto, FL', 27.576780, -82.552740, 24],
        ['Pompano Beach, FL', 26.232540, -80.099220, 25],
        ['Riviera Beach, FL', 26.787900, -80.070660, 26],
        ['Saint Petersburg, FL', 27.732420, -82.668010, 27],
        ['Tequesta, FL', 26.987220, -80.101860, 28],
        ['Annapolis, MD', 38.945220, -76.491420, 29],
        ['Boston, MA', 42.378660, -71.015880, 30],
        ['Weehawken, NJ', 40.767775, -74.030093, 31],
        ['Jacksonville, NC', 34.774266, -77.380179, 32],
        ['Portsmouth, RI', 41.577100, -71.265200, 33],
        ['Warwick, RI', 41.702703, -71.444247, 34],
    ];

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
}
google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addDomListener(window, "resize", function() {

    if (windowWidth < 768) {
        map.setZoom(3);
    } else {
        map.setZoom(4);
    }

    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
});
