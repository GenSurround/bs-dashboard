var $screen_xs = 480,
    $screen_sm = 768,
    $screen_md = 992,
    $screen_lg = 1200;
video = true;
var videoEl = $('#vidhome').get(0);


// for default-alt
var standardHeight = $('#form-standard-search .toggle-form').outerHeight();


function resizeVideoOnload() {


    var wWidth = $(window).width(),
        wHeight = $(window).height(),
        limit = 900;

    if ($('.agents').length > 0) {
        limit = 600;
    }


    if (wWidth >= $screen_sm) { // DESKTOP
        var tHeight = $('.slideshow-overlay').outerHeight(true),
            dHeight = wHeight - $("#what-how-overview").outerHeight(true) - 45;
        dHeight = (dHeight > limit) ? limit : dHeight;

        if (dHeight > 400) {
            $('.flexslider-fullwidth, .flexslider-fullwidth .slides > li, .slider-row').css('height', dHeight);

        }



    } else { // MOBILE
        var landscapeDiff = 0;
        if (wHeight < wWidth) {
            landscapeDiff = 80;
        }
        var mHeight = wHeight - $('#what-how-overview .lead').outerHeight() - $('header').outerHeight() - 16;
        $('.flexslider-fullwidth, .flexslider-fullwidth .slides > li').css('height', mHeight - standardHeight);
        CompleteSecHeight = $('#complete-home-search').outerHeight()
        $('.slideshow-overlay').height(CompleteSecHeight);
    }

}



window.onpaint = resizeVideoOnload();




$(document).ready(function() {



    if ($('.trip-section.list-steps').length > 0) {
        $('body').addClass('boat-trip-details');
    };




    $('.rating-label').each(function(index, el) {
        if ($(this).html().toLowerCase().indexOf("(0)") >= 0) {
            $(this).css({
                opacity: 0,
                visibility: 'hidden'
            });
        }
    });

    $('#pending-reviews .rating-label').each(function(index, el) {
        if ($(this).html().toLowerCase().indexOf("(0)") >= 0) {
            $(this).css({
                opacity: 1,
                visibility: 'visible'
            });
        }
    });



    setTimeout(function() {
        $('.rateit-range').each(function(index, el) {
            if ($(this).attr('aria-valuenow') == 0) {
                $(this).css({
                    opacity: 0,
                    visibility: 'hidden'
                });
                $(this).parent().css({
                    opacity: 0,
                    visibility: 'hidden'
                });
                if ($(this).parent().parent().hasClass('rating')) {
                    $(this).parent().parent().css({
                        display: 'none'
                    })
                }
            }
        });

        $('#pending-reviews .rateit-range').each(function(index, el) {
            if ($(this).attr('aria-valuenow') == 0) {
                $(this).css({
                    opacity: 1,
                    visibility: 'visible'
                });
                $(this).parent().css({
                    opacity: 1,
                    visibility: 'visible'
                });
                if ($(this).parent().parent().hasClass('rating')) {
                    $(this).parent().parent().css({
                        display: 'block'
                    })
                }
            }
        });
    }, 400);




    $('.white-tooltip .icon').click(function(event) {
        event.preventDefault();
    });







    $('#how-it-works').height($('.how-it-works').outerHeight());
    $('#merger-notification').height($('.merger-notification').outerHeight());

    $('#how-it-works-open').click(function(event) {
        event.preventDefault();
        $('body, html').animate({
            scrollTop: 0
        }, 300);
        $('#how-it-works').toggleClass('closed');
    });
    $('#how-it-works-close').click(function(event) {
        event.preventDefault();
        $('#how-it-works').toggleClass('closed');
    });

    $('#merger-notification-close').click(function(event) {
        event.preventDefault();
        $('#merger-notification').toggleClass('closed');
    });

    if (window.location.href.indexOf("#login") >= 0) {
        $('#bs-modal-login').modal('show');
    }

    if (window.location.href.indexOf("#loginpanel") >= 0) {
        $('#bs-modal-login').modal('show');
    }

    if (window.location.href.indexOf("#merger") >= 0) {
        $('#bs-modal-merger').modal('show');
    }

    //CUSTOM SLIDE MOBILE MENU FUNCTION
    /*var dragCheck = false;


    $('#bs-navbar-mobile').draggable({
        axis: "x",
        containment: 'parent',
        drag: function(){
         dragCheck = true;
            },
            stop: function() {
            var offset = $(this).offset();
                var xPos = offset.left;
                var yPos = offset.top;
                var limitClose = $(window).outerWidth() - ($('#bs-navbar-mobile').outerWidth() / 1.8);
                var posNav = $(window).outerWidth() - $('#bs-navbar-mobile').outerWidth();
                if (xPos > limitClose){

                    //$('#bs-navbar-mobile').animate({left:$(window).width()- $('#bs-navbar-mobile').outerWidth()}, 300, 'easeOutExpo', function(){
                        $('body').removeClass('body-menu-open').parent().removeClass('html-menu-open'),
                    $('.bs-navbar-mobile-parent').removeClass('open'),
                    $('.navbar-toggle').removeClass('on');
                    setTimeout(function(){
                        $('#bs-navbar-mobile').css({left:posNav})
                    }, 400)
                }else if(xPos < limitClose){

                    $('#bs-navbar-mobile').animate({left:posNav}, 300, 'easeOutExpo')
                }
                dragCheck = false;
          }

    });

    $('#bs-navbar-mobile a').each(function(index, el) {
           $(this) .bind('touchend click', function(){
               var hrefLink = $(this).attr('href');
               if(dragCheck == false){
                    window.location = hrefLink;

               }
            });
    });*/




    $(".bs-site-wrapper").click(function() {});
    $(document).off("click.bb.stowaway"),


        $(document).on("click.bb.stowaway", ".navbar-toggle", function(event) {
            return event.preventDefault(),
                $('body').addClass('body-menu-open').parent().addClass('html-menu-open'),
                $('.bs-navbar-mobile-parent').addClass('open'),
                $('.mobile-search-drop').removeClass('open'),
                $(this).addClass('on');
        })

    $(document).on("click.bb.stowaway", "body.body-menu-open :not(.bs-navbar-mobile-parent, .bs-navbar-mobile-parent *)", function(e) {
        return e.preventDefault(), e.stopPropagation(), $("body").removeClass("body-menu-open").parent().removeClass('html-menu-open'), $('.navbar-toggle').removeClass('on'), $('.bs-navbar-mobile-parent').removeClass('open');
    })


    //CUSTOM SLIDE MOBILE MENU FUNCTION FOR DASHBORAD

    $(document).off("click.bb.dashway"),

        $(document).on("click.bb.dashway", ".dashboard-navbar-toggle", function(event) {
            return event.preventDefault(),
                $('body').addClass('body-dashmenu-open').parent().addClass('html-dashmenu-open'),
                $('.mobile-search-drop').removeClass('open');
            $(this).addClass('on');
        })

    $(document).on("click.bb.dashway", "body.body-dashmenu-open :not(.dashboard-navbar-mobile, .dashboard-navbar-mobile *)", function(e) {
        return e.preventDefault(), e.stopPropagation(), $("body").removeClass("body-dashmenu-open").parent().removeClass('html-dashmenu-open'), $('.navbar-toggle').removeClass('on');
    });



    if ($('.dashboard-nav').length > 0) {
        if ($(window).width() > 620) {
            $('#bs-navbar-mobile').css({
                top: 115
            });

        } else if ($(window).width() < 620) {
            $('#bs-navbar-mobile').css({
                top: 98
            });
        }
    };



    $('.mobile-search-drop input#mobile_all').click(function() {
        $('.mobile-search-drop').addClass('open');

    });

    $('.mobile-search-drop .fa-close').click(function() {
        $('.mobile-search-drop .fa-close').css({
            display: 'none'
        });
        $('.mobile-search-drop').addClass('open');
        $('.mobile-search-drop input#mobile_all').val('');
        $('.mobile-search-drop input#mobile_all').focus();
    });


    $('.mobile-search-dropdown-toggle').click(function(event) {
        event.preventDefault();
        $('.mobile-search-drop').toggleClass('open');

        if ($('.mobile-search-drop').hasClass('open')) {
            setTimeout(function() {
                $('.mobile-search-drop input#mobile_all').focus();
            }, 400);
        } else {
            setTimeout(function() {
                $('.mobile-search-drop input#mobile_all').blur();
            }, 300);
        }


    });

    $('.mobile-search-drop input#mobile_all').keyup(function(event) {
        $('.mobile-search-drop .fa-close').css({
            display: 'block'
        });
    });







    /*Get Active Section Text and replace title of menu*/
    if ($('.dashboard #ListYourBoatMenu').length > 0) {
        var activeSectionMenuText = $('.dropdown-menu').find('li a.active').html();
        $('.dashboard #ListYourBoatMenu').html(activeSectionMenuText + '<p class="pull-right caret"></p>');


    }

    if ($('.ismobilecheckbox').length > 0) {
        //$("#PhnVerify").addClass('hidden');
        $('.ismobilecheckbox').click(function() {
            $("#PhnVerify").toggleClass('hidden');
        });
    }





    $('a[data-find]').on('click', function(event) {
        event.preventDefault();
        var origin = $(this).data('origin'),
            find = $(this).data('find');

        $('#' + origin).toggleClass('hidden');
        $('#' + find).toggleClass('hidden');


        var originalsimpleSearchLocVal = $('#' + origin).find('input._location').val();
        var originaladvancedSearchLocVal = $('#' + origin).find('#cap-search-form input._location').val();
        var simpleSearchLocVal = '';
        var advancedSearchLocVal = '';
        if (originalsimpleSearchLocVal != '') {
            simpleSearchLocVal = $('#' + origin).find('input.search-boat-location').val();
        }
        if (originalsimpleSearchLocVal != '') {
            advancedSearchLocVal = $('#' + origin).find('#cap-search-form input.search-boat-location').val();
        }
        advancedSearchLocVal = $('#' + origin).find('#cap-search-form input.search-boat-location').val();

        if (find == "form-standard-search") {
            $('.cap-search').removeClass('active-tab');
            $('.boat-searc').addClass('active-tab');
            $('#cap-search-form').removeClass('show-tab').removeAttr('style');
            $('#boat-search-form').addClass('show-tab').attr('style', 'visibility:visible');
            $('.pac-0').remove();
            locationField = false;

        }

        if (find == 'form-advanced-search') {
            $('.pac-1').remove();
            $('.pac-2').remove();
            $('.pac-3').remove();
            secondLocField = true;




        }

        if ($('#cap-search-form').hasClass('show-tab')) {

            $('#' + find).find('input.search-boat-location').val(advancedSearchLocVal);
            $('#' + find).find('input#location').val(advancedSearchLocVal);

            if (!advancedSearchLocVal == '') {
                if (find == 'form-advanced-search') {
                    console.log('advanced')
                    $('.pac-1').remove();
                    $('.pac-2').remove();
                    $('.pac-3').remove();

                    $('#' + find).find('#boat-search-form button[type="submit"]').text('Start your adventure').prop('disabled', false);

                } else if (find == 'form-standard-search') {
                    $('#' + find).find('button[type="submit"]').text('Great choice, let\'s go').prop('disabled', false);
                }

            } else {
                $('#' + find).find('#boat-search-form button[type="submit"]').text('Search').prop('disabled', true);
                $('#' + find).find('button[type="submit"]').text('Search').prop('disabled', true);
                $('#' + find).find('input.search-boat-location').val('');
                $('#' + find).find('input#location').val('');
            }


        } else {
            $('#' + find).find('input.search-boat-location').val(simpleSearchLocVal);
            $('#' + find).find('input#location').val(simpleSearchLocVal);
            if ($('#' + find).find('input#location').val()) {
                $('#' + find).find('button[type="submit"]').prop('disabled', false);
                if (find == 'form-advanced-search') {
                    $('#' + find).find('button[type="submit"]').text('Start your adventure');
                } else if (find == 'form-standard-search') {
                    $('#' + find).find('button[type="submit"]').text('Great choice, let\'s go');
                }
            } else {
                $('#' + find).find('button[type="submit"]').prop('disabled', true);
                $('#' + find).find('button[type="submit"]').text('Search');
            }
        }





        showTabHeight = $('#' + find).find('#boat-search-form').height();
        $('#' + find).find('.home-toggle-form').height(showTabHeight);

        if (jQuery('.flexslider-fullwidth')) {
            if ($('.home-video_2').length > 0) {
                resizeHomeSlideshowSecond();
            } else {
                resizeHomeSlideshow();
            }
        }
    });
    //  $( '.nav-tabs' ).tabCollapse();


    $('.buttons-tabs .menu-button').each(function(index, el) {
        $(this).click(function(event) {
            event.preventDefault();
            $(this).addClass('active-tab');
            $(this).parent().siblings().find('a').removeClass('active-tab')
            targetTab = $(this).attr('href');
            $('.home-toggle-form').find(targetTab).siblings().addClass('hide-tab hide-tab-action').removeClass('show-tab');
            setTimeout(function() {
                $('.home-toggle-form').find(targetTab).siblings().css('visibility', 'hidden');
                $('.home-toggle-form').find(targetTab).siblings().removeClass('hide-tab-action');

            }, 300);
            $('.home-toggle-form').find(targetTab).css('visibility', 'visible');
            setTimeout(function() {
                $('.home-toggle-form').find(targetTab).addClass('show-tab').removeClass('hide-tab');
                visibleTabH = $('.home-toggle-form .show-tab').height();
                $('#form-advanced-search .home-toggle-form').height(visibleTabH);
            }, 400);
        });
    });

    function resizeTabsHome() {
        boatSearchH = $('.show-tab').outerHeight();
        $('#form-advanced-search .home-toggle-form').height(boatSearchH);
        $('#form-standard-search .home-toggle-form').css({
            height: 'auto'
        })

    }

    $(window).resize(function() {
        resizeTabsHome();
        if ($(window).width() > 992) {
            $('.mobile-search-drop').removeClass('open');
            $('.locationField input').attr('id', 'boatLocationFilteringMobile');
            $('.locationFieldSide input').attr('id', 'boatLocationFiltering');

        } else if ($(window).width() < 992) {
            $('.locationField input').attr('id', 'boatLocationFiltering');
            $('.locationFieldSide input').attr('id', 'boatLocationFilteringNull');
        };



    });

    setTimeout(function() {
        resizeTabsHome();
    }, 600);

    //$('#filter').stick_in_parent();
    $('#filter').stick_in_parent({
        recalc_every: 1
    });










    $('input[type=date]').each(function(index, input) {
        var $input = $(input);

        $(input).css('z-index', 1050);

        $input.attr('type', 'text').datepicker({
            beforeShow: function(input, inst) {
                var widget = $(inst).datepicker('widget');

                widget.css({
                    'margin-left': $(input).parent().outerWidth(true) - widget.outerWidth(true)
                });
            }
        });
    });

    $(".password-tip .icon").click(function() {
        //event.preventDefault();
        $(".password-tip").tooltip('toggle');
    });

    $(".tip .icon").click(function() {
        //event.preventDefault();
        $(".tip").tooltip('toggle');
    });




    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('input[readonly]').on('focus', function(ev) {
            $(this).trigger('blur');
        });

        if ($('#vidhome').length > 0) {
            if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                videoEl.pause();
                $('video #mp4').attr('src', '');
                $('video #webm').attr('src', '');
                $('video #ogg').attr('src', '');
            } else {
                $('video #mp4').attr('src', '/videos/boatsettervideo.mp4');
                $('video #webm').attr('src', '/videos/boatsettervideo.webm');
                $('video #ogg').attr('src', '/videos/boatsettervideo.ogv');
                videoEl.load();
                videoEl.play();
            }
        }



    });

    $(document).ready(function() {
        $('[data-toggle="popover"]').popover();
    });

    $('input[type=range]').each(function(index, input) {
        var $input = $(input),
            $range = $input.attr('data-range'),
            $min = parseInt($input.attr('data-min'), 10) || 0,
            $max = parseInt($input.attr('data-max'), 10) || 100,
            $minvalue = parseInt($input.attr('val-min'), 10) || 0,
            $maxvalue = parseInt($input.attr('val-max'), 10) || 0,
            $step = parseInt($input.attr('data-step'), 10) || 1;

        if (typeof $range !== typeof undefined && $range == 'true') {
            var $name = $input.attr('name');
            $input.siblings('.range-label').attr('title', $input.siblings('.range-label').text());
            $input.siblings('.range-label').text(String.format($input.siblings('.range-label').attr('title'), $minvalue, $maxvalue));
            $slider = $('<div />').slider({
                range: true,
                min: $min,
                max: $max,
                values: [$minvalue, $maxvalue],
                step: $step,
                slide: function(event, ui) {
                    $input.siblings('.range-label').text(String.format($input.siblings('.range-label').attr('title'), ui.values[0], ui.values[1]));

                    $(input).siblings('input[name="' + $name + '-min"]').val(ui.values[0]);
                    $(input).siblings('input[name="' + $name + '-max"]').val(ui.values[1]);
                },
            });

        } else {
            $slider = $('<div />').slider({
                range: $range,
                min: $min,
                max: $max,
                value: $value,
                step: $step,
            });

        } // endif

        $input.after($slider).hide();
    });



    $('.dropdown-form').on('click', function(event) {
        event.stopPropagation();
    });

    $('.dropdown-menu a[data-toggle="tab"]').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();

        $(this).tab('show');
    });


    $('#top-destinations').slick({
        arrows: false,
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: false,
        centerPadding: '0px',
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                arrows: true,
                slidesToShow: 4,
                slidesToScroll: 4,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                arrows: true,
                slidesToShow: 5,
                slidesToScroll: 5,
                infinite: true,
                dots: false
            }
        }]

    });


    $('#boats-overview').slick({
        arrows: false,
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: false,
        centerPadding: '0px',
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: false
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                arrows: true,
                slidesToShow: 4,
                slidesToScroll: 4,
                infinite: true,
                dots: false
            }
        }]

    });

    $('#testomonials-overview').slick({
        arrows: false,
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: false,
        centerPadding: '0px',
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                arrows: false,
                dots: true
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                arrows: true,
                dots: true
            }
        }]
    });

    $('#new-captain-slider').slick({
        arrows: false,
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: true,
        centerPadding: '0px',
        infinite: true,
        dots: true,
        // autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }]

    });

    var toggle_newCaptain = $('#bs-new-captain-toggle'),
        newCaptainPanel = $('#bs-new-captain');

    newCaptainPanel.on('shown.bs.collapse', function() {
        toggle_newCaptain.addClass('btn-knockout');
        toggle_newCaptain.blur();
    });
    newCaptainPanel.on('hidden.bs.collapse', function() {
        toggle_newCaptain.removeClass('btn-knockout');
    });

    toggle_newCaptain.trigger("click");




    $('#new-boat-slider').slick({
        arrows: false,
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: true,
        centerPadding: '0px',
        infinite: true,
        dots: true,
        // autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }]

    });

    var toggle_newBoat = $('#bs-new-boat-toggle'),
        newBoat = $('#bs-new-boat');

    newBoat.on('shown.bs.collapse', function() {
        toggle_newBoat.addClass('btn-knockout');
        toggle_newBoat.blur();
    });
    newBoat.on('hidden.bs.collapse', function() {
        toggle_newBoat.removeClass('btn-knockout');
    });

    toggle_newBoat.trigger("click");


    $('#lyb-images-slider').slick({
        arrows: false,
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: true,
        centerPadding: '0px',
        infinite: true,
        dots: true,
        // autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }]

    });



    $('#related-gallery').slick({
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: false,
        centerPadding: '0px',
        infinite: true,
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                arrows: true,
                dots: true
            }
        }]

    });








    $('#boat-details-gallery').slick({
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"></a>',
        lazyload: 'ondemand',
        mobileFirst: true,
        initialSlide: 0,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        centerMode: true,
        centerPadding: '0px',
        arrows: true,
        dots: true,
        adaptiveHeight: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '#boat-details-nav',
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                fade: true,
                dots: false
            }
        }]
    });

    if ($('#boat-details-gallery').length > 0) {
        if ($(window).width() < $screen_md) {
            setTimeout(function() {
                currentHeight = $('#boat-details-gallery .slick-slide').outerHeight();
                $('#boat-details-gallery .slick-list').css({
                    height: currentHeight
                });
            }, 500);
        }
    }








    if ($("#boat-details-nav .thumb-gal").length < 6) {
        $(document).on("click", ".gal .slick-next", function() {
            var $selector = $('#boat-details-nav').find('.slick-slide');

            var $next = $('#boat-details-nav').find('.slick-current').next();

            if ($next.length == 0) $next = $('#boat-details-nav').find('.slick-slide').first();
            $selector.removeClass('slick-current');
            $next.addClass('slick-current');

        });
        $(document).on("click", ".slick-prev", function() {
            var $selector = $('#boat-details-nav').find('.slick-slide');

            var $prev = $('#boat-details-nav').find('.slick-current').prev();

            if ($prev.length == 0) $prev = $('#boat-details-nav').find('.slick-slide').last();
            $selector.removeClass('slick-current');
            $prev.addClass('slick-current');

        });



    } else {

        $(document).off("click", ".gal .slick-prev");
        $(document).off("click", ".gal .slick-next");

    }






    $('#boat-details-nav').slick({
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        slidesToShow: 5,
        slidesToScroll: 1,
        lazyload: 'ondemand',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        asNavFor: '#boat-details-gallery',
        variableWidth: false,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                arrows: true,
                centerMode: true,
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                centerMode: true,
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
                centerMode: true,
            }
        }]
    });


    //
    // CAPTAIN PROFILE SLIDERS



    $('#captain-details-gallery').slick({
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        autoplay: true,
        autoplaySpeed: 2000,
        centerMode: false,
        centerPadding: '0px',
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '#captain-details-nav',
        responsive: [{
            breakpoint: $screen_md,
            settings: {
                fade: true,
                dots: false
            }
        }]
    });








    $('#captain-details-nav').slick({
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        centerPadding: '0px',
        focusOnSelect: true,
        asNavFor: '#captain-details-gallery',
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                arrows: true,
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 5,
            }
        }]
    });

    //
    // END CAPTAIN PROFILE SLIDERS

    var homeSlideshowActive = false;

    if ($('.flexslider').length > 0) {
        resizeHomeSlideshow();
    }


    // For Multiple Modals: Include data-second-modal="true" on html markup
    // $('*[data-second-modal="true"]').on('hidden.bs.modal', function() {
    $('.modal').on('hidden.bs.modal', function() {
        if ($('.modal-backdrop').is(":visible"))
            $('body').addClass('modal-open');
    });
    // For Multiple Modals: Include data-second-modal="true" on html markup


    // TOP NOTIFICATION: Include data-top-notification="true" on html markup
    window.setTimeout(function() {
        $('*[data-top-notification="true"]').collapse('show');
    }, 1000);
    window.setTimeout(function() {
        $('*[data-top-notification="autohide"]').collapse('show');
    }, 1000);
    if ($('*[data-top-notification="autohide"]').length > 0) {
        window.setTimeout(function() {
            $('*[data-top-notification="autohide"]').collapse('hide');
        }, 10000);
    }
    // TOP NOTIFICATION: Include data-top-notification="true" on html markup


    $('.bs-notifications-mobile .dropdown-toggle').click(function(event) {
        if ($('.bs-navbar-mobile.in').length > 0) {
            $('#bs-navbar-mobile').collapse('hide');
        }
    });


    //$('#sticky-map').stick_in_parent();

    if ($('#sticky-map-wrap').length < 1) {
        $('#sticky-map').parent().css({
            'height': $('#sticky-map-parent').height() - 79
        });

        if ($('#seach_result_boats').length) {
            if ($('#seach_result_boats .search-result').length < 2) {
                $('#sticky-map').height(700);
            };
        };

        if ($('#seach_result_captains').length) {
            if ($('#seach_result_captains .search-result').length < 2) {
                $('#sticky-map').height(700);
            };
        };



        $(window).scroll(function(event) {
            if ($('.paginationContainer').length > 0) {
                pagOffset = $('.paginationContainer').offset().top - 20 - $('#sticky-map').outerHeight();
            } else {
                pagOffset = $('.bs-site-footer').offset().top - 20 - $('#sticky-map').outerHeight();
            }
            $winScrollTop = $(window).scrollTop();
            mapInitPos = $('header').height() + 40 + $('.panel.bg-blue').outerHeight();

            if ($winScrollTop > mapInitPos) {
                $('#sticky-map').addClass('map-stuck');
            } else if ($winScrollTop < mapInitPos) {
                $('#sticky-map').removeClass('map-stuck');
            }
            if ($winScrollTop > pagOffset) {
                $('#sticky-map').addClass('notmap-stuck');
            } else if ($winScrollTop < pagOffset) {
                $('#sticky-map').removeClass('notmap-stuck');
            }
        });
    };


    if ($('#sticky-map-wrap').length > 0) {


        if ($('#seach_result_boats').length) {
            if ($('.paginationContainer').length) {
                typeOff = 79
            } else {
                typeOff = 30
            }
            if ($('#seach_result_boats .search-result').length < 3) {
                $('#sticky-map').height(620);
                $('#sticky-map-wrap').parent().css({
                    'height': 780,
                    'margin-bottom': 20
                });
                $('#sticky-map-wrap').css({
                    'height': 620
                });
            } else {
                $('#sticky-map').height(700);
                $('#sticky-map-wrap').parent().css({
                    'height': $('#sticky-map-parent').outerHeight() - typeOff
                });
                $('#sticky-map-wrap').height(700);
            }




        };

        if ($('#seach_result_captains').length) {
            if ($('.paginationContainer').length) {
                typeOff = 79
            } else {
                typeOff = 30
            }
            if ($('#seach_result_captains .search-result').length < 3) {
                $('#sticky-map').height(620);
                $('#sticky-map-wrap').parent().css({
                    'height': 780,
                    'margin-bottom': 20
                });
                $('#sticky-map-wrap').css({
                    'height': 620
                });
            } else {
                $('#sticky-map').height(700);
                $('#sticky-map-wrap').height(700);
                $('#sticky-map-wrap').parent().css({
                    'height': $('#sticky-map-parent').outerHeight() - typeOff
                });
            }
        };

        $(window).scroll(function(event) {
            if ($('.paginationContainer').length > 0) {
                pagOffset = $('.paginationContainer').offset().top - 20 - $('#sticky-map').outerHeight();
            } else {
                pagOffset = $('.bs-site-footer').offset().top - 20 - $('#sticky-map').outerHeight();
            }
            $winScrollTop = $(window).scrollTop();
            mapInitPos = $('header').height() + 40 + $('.panel.bg-blue').outerHeight();

            if ($winScrollTop > mapInitPos) {
                $('#sticky-map-wrap').addClass('map-stuck');
            } else if ($winScrollTop < mapInitPos) {
                $('#sticky-map-wrap').removeClass('map-stuck');
            }
            if ($winScrollTop > pagOffset) {
                $('#sticky-map-wrap').addClass('notmap-stuck');
            } else if ($winScrollTop < pagOffset) {
                $('#sticky-map-wrap').removeClass('notmap-stuck');
            }
        });
    };

    $('#bs-insurance-submit').click(function(event) {
        event.preventDefault();
        verifyInsurance();
    });

    $('a.only-toggle').click(function(e) {
        e.preventDefault();
    });




    // FIX NOTIFICATION BELL Z-INDEX
    var boatOverview = $('#boat-overview'),
        bell = $('.bs-notifications-mobile');

    boatOverview.on('show.bs.modal', function() {
        bell.css('z-index', 2);
    });

    boatOverview.on('hidden.bs.modal', function() {
        bell.css('z-index', 4);
    });

    // FIX NOTIFICATION BELL Z-INDEX // END
    // FORCE RESIZE TO FIX SLIDER ISSUES ON IE
    $(window).resize();
    // FORCE RESIZE TO FIX SLIDER ISSUES ON IE // END
});




$(window).resize(function() {



    var width = $(window).width(),
        height = $(window).height();

    $('#how-it-works').height($('.how-it-works').outerHeight());


    if ($('.flexslider').length > 0) {
        resizeHomeSlideshow();
    }

    if ($screen_sm > width) {
        $('#filter').trigger('sticky_kit:detach');
        $('.lyb-list').removeClass('in');


    } else {
        //$('#filter').stick_in_parent();
        $('#filter').stick_in_parent({
            recalc_every: 1
        });
        $('.lyb-list').addClass('in');
    } // endif


});

function resizeHomeSlideshow() {

    var wWidth = $(window).width(),
        wHeight = $(window).height(),
        limit = 900;


    // default-alt slider
    // ------------------------------------------
    if (wWidth >= $screen_sm) { // DESKTOP

        var tHeight = $('.slideshow-overlay').outerHeight(true),
            dHeight = wHeight - $("#what-how-overview").outerHeight(true) - 45;
        dHeight = (dHeight > limit) ? limit : dHeight;

        $('.flexslider-fullwidth .slides > li').css({
            position: 'absolute'
                // ,property2: 'value2'
        });

        if (dHeight > 400) {
            $('.flexslider-fullwidth, .flexslider-fullwidth .slides > li, .slider-row').css('height', dHeight);

        }



        homeSlideshowActive = activateHomeSlideshowWithVideo();
    } else { // MOBILE

        var landscapeDiff = 0;
        if (wHeight < wWidth) {
            landscapeDiff = 0;
        }

        var mHeight = wHeight - $('#what-how-overview .lead').outerHeight() - $('header').outerHeight() - 16;

        $('.flexslider-fullwidth, .flexslider-fullwidth .slides > li').css('height', mHeight - standardHeight);

        homeSlideshowActive = activateHomeSlideshowWithVideo();
        CompleteSecHeight = $('#complete-home-search').outerHeight();
        $('.slideshow-overlay').height(CompleteSecHeight);
    }

    // default-alt slider
    // ------------------------------------------


    if (wWidth > $screen_md) {
        $('#search-main').stick_in_parent();
    } else {
        $('.hero-video video *').hide();
    }

}



function activateHomeSlideshowWithVideo() {

    var wWidth = $(window).width(),
        wHeight = $(window).height(),
        imgUrl,
        flex = $('.flexslider'),
        videoWrapper = $('.video-wrapper'),
        videoInner = $('.video-wrapper video');






    if (!video || wWidth <= $screen_sm) { // SLIDER

        $('.flexslider .slide-image').each(function(index, el) {

            if (wWidth > $screen_sm)
                imgUrl = 'url(' + $(this).data('img-sm') + ')';
            else
                imgUrl = 'url(' + $(this).data('img-xs') + ')';

            $(this).css('background-image', imgUrl);

        });





        videoWrapper.css('display', 'none');

        jQuery('.flexslider').flexslider({
            controlNav: true,

            pauseText: '<i class="fa fa-pause fa-3x"></i>',
            playText: '<i class="fa fa-play fa-3x"></i>',
            prevText: '<i class="fa fa-angle-left fa-3x"></i>',
            nextText: '<i class="fa fa-angle-right fa-3x"></i>',
            directionNav: true,

            slideshow: true,
            animationSpeed: 600,
            start: function(slider) {
                // alert( 'test' );
                jQuery('.flex-active-slide').find('.caption').fadeIn(300);
            },
            before: function(slider) {
                jQuery('.flex-active-slide').find('.caption').fadeOut(200);
            },
            after: function(slider) {
                jQuery('.flex-active-slide').find('.caption').delay(200).fadeIn(300);
            }
        });

        $(".slides").css('opacity', '1');
        $(".toggle-form.bg-dark-blue").css('background-color', 'rgba(0, 104, 176, 1)');
        $(".flex-control-nav").css('display', 'block');
        $(".flex-control-paging").css('display', 'block');
        $(".flex-direction-nav").css('display', 'block');
    } else { // VIDEO

        videoWrapper.css('display', 'block');
        setTimeout(function() {
            videoWrapper.css('opacity', '1')
        }, 300);

        if ($('video #mp4').attr('src') == '') {
            $('video #mp4').attr('src', '/videos/boatsettervideo.mp4');
            $('video #webm').attr('src', '/videos/boatsettervideo.webm');
            $('video #ogg').attr('src', '/videos/boatsettervideo.ogv');
            setTimeout(function() {
                videoEl.load();
                videoEl.play();
            }, 300);
        }




        if (flex.width() / flex.height() > 2.147651007) {
            videoInner.css('width', "100%");
            videoInner.css('height', "auto");
        } else {
            videoInner.css('height', "100%");
            videoInner.css('width', "auto");
        }

        $(".slides").css('opacity', '0');
        $(".toggle-form.bg-dark-blue").css('background-color', 'rgba(0, 104, 176, 0.7)');


        $(".flex-control-nav").css('display', 'none');
        $(".flex-control-paging").css('display', 'none');
        $(".flex-direction-nav").css('display', 'none');
    }



    return true;
}



function activateHomeSlideshow() {

    var wWidth = $(window).width(),
        wHeight = $(window).height(),
        imgUrl;

    $('.flexslider .slide-image').each(function(index, el) {

        if (wWidth > $screen_sm)
            imgUrl = 'url(' + $(this).data('img-sm') + ')';
        else
            imgUrl = 'url(' + $(this).data('img-xs') + ')';

        $(this).css('background-image', imgUrl);

    });


    jQuery('.flexslider').flexslider({
        controlNav: true,

        pauseText: '<i class="fa fa-pause fa-3x"></i>',
        playText: '<i class="fa fa-play fa-3x"></i>',
        prevText: '<i class="fa fa-angle-left fa-3x"></i>',
        nextText: '<i class="fa fa-angle-right fa-3x"></i>',
        directionNav: true,

        slideshow: true,
        animationSpeed: 600,
        start: function(slider) {
            // alert( 'test' );
            jQuery('.flex-active-slide').find('.caption').fadeIn(300);
        },
        before: function(slider) {
            jQuery('.flex-active-slide').find('.caption').fadeOut(200);
        },
        after: function(slider) {
            jQuery('.flex-active-slide').find('.caption').delay(200).fadeIn(300);
        }
    });




    return true;
}

function verifyInsurance() {

    var questions = $('#bs-insurance-form input:radio');
    var need_captain = false;

    questions.each(function(index, el) {
        if (el.checked && el.value == 'yes') {

            need_captain = true;
        }
    });

    if (need_captain) {
        $('#bs-modal-need-captain').modal('toggle');
        $('#captain-required-alert').addClass('in');
    }
}
