# OWNER DASHBOARD

## GENERAL
--- ~~inputs~~
- ~~checkboxes~~
- ~~radiobuttons~~
- ~~sliders (noUi)~~
- ~~responsive~~
- ~~tooltips~~
- ~~modals~~
- ~~mobile sidebar functionality~~
- progress bar
- ~~nano scroller~~
- Vamos a agregar un botón de  [ Save ] debajo de cada sección
- All CTA (Accept) Orange en todos lados
- Snooze deberia preguntar de cuando a cuando (Calendario)

## SIDEBAR
- ~~finish status selector~~
- ~~badge with number~~
- ~~mobile functionality~~
- ~~check responsive~~

## AVAILABILTY
- ~~navigation~~
- ~~activate tooltips~~
- ~~adjust tooltips trips layout?~~
- ~~calendar:~~
- ~~click on mobile~~
- ~~trips!~~
- ~~En el left hand nav, poner el dot de alerta al lado de Availability cuando haya algo pending esperando que acepte~~
- ~~Modal q sale del calendario, hacer una version sin messages~~

## PRICING
- ~~fix checkboxes~~
- ~~add sliders~~
- ~~add 'very likely to get rented' thingy~~
- ~~mod calendar for 'smart pricing'~~ RESPONSIVE BLEH
- ~~check responsive~~
- ~~Weekly discount (missing text) falta lo q explica q es un descuento.~~
- ~~Pone el slider de 0% a 15%~~
- ~~Agregar el length al lado de half-day (4 hours) / full-day (8 hrs)~~
- ~~Explication que el % es descuento (Weekly discount) y texto abajo pequeño~~

## CAPTAINS
- ~~add tooltips~~
- ~~add search-bar~~
- ~~add popups (modals)~~
- ~~check responsive~~
- ~~Suggest my own captain » el cursor no muestra estilo/mano~~
- ~~Debajo de captain request, pongamos un Lorem Ipsom para explicar q es eso~~
- ~~Las listas de capitanes se mezclan (Select / Requests) Vamos a poner una linea mas oscura o algo para separar~~
- ~~Cuando el dueño le da a “I’ll Captain..” y luego sale el modal, al aceptar ese modal sale el de las preguntas de seguro~~
- Hay que agregar un link para ver el profile del capitan en un modal
- ~~Suggest captain debe abrir el modal para poner name, email etc del capitán no este~~

## BOAT MANAGERS
- ~~add checkboxes~~
- ~~activate 'add more'~~
- ~~add form for 'add more'~~
- ~~radiobuttons~~
- ~~check responsive~~
- ~~Get approved debe abrir el modal de las preguntas del seguro~~
- ~~Send request debe mostrar un modal con el wrapper de los terms para que le de a agree (esta en los wireframes)~~

## TRIPS
- ~~fill all tabs~~
- ~~check responsive~~
- ~~polish animations~~
- ~~Quitar el carrot ( > ) ya q los paneles no son expandibles~~
- ~~En otras secciones del dashboard usamos un acordeón en el left nav. Hay q hacer lo mismo aqui. La parte de sub nav arriba se pierde (Requests, Upcoming, etc)~~
- ~~Poner accept en orange~~
- ~~en los tiles de los trips, hagamos que el carrot despliega la info, como cuando haces click en more details. Pero por instinto le hice click al > a ver si se abria~~

## PICS & VIDEO
- mod uploader (when upload)
- ~~check responsive~~
- ~~Vamos a poner un container q vaya de lado a lado para upload o drop multiple files de una vez~~

## BOAT DETAILS
- ~~everything!~~
- ~~activate tooltips~~
- ~~forms~~
- ~~boat buttons~~
- ~~sliders~~
- ~~selects~~
- Add HIN in the General Info tab

## INSURANCE
- ~~check responsive~~
- ~~check functionality?~~
- ~~Pon el botón de Selected gris, ya que ya fue selected~~
- ~~Pongamos abajo un check box tipo: I’d like to use my own insurance. on click q salga un mensaje que debe llamar al YXZ~~

## MY EARNINGS
- everything!
- graphic
- date picker
- selects
- checkboxes

## PROMOTE YOUR BOAT
- NPI

## ALERT BAR
- ~~Alert bar on the bottom orange~~
- Z-index de la barra esta off (el texto del sidebar se pone encima)

**IMPORTANTE** 
Para owners con mas de un bote falta la pagina de My Fleet