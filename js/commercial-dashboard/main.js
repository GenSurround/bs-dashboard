// PROGRESS CIRCLE ON SIDEBAR
$('#progress-circle').circleProgress({
    value: 0.33,
    size: 200,
    fill: "orange",
    thickness: 2.7777777777777777,
    startAngle: -Math.PI / 2,
});

// STATUS SELECT ON SIDEBAR
$('.boat-status-select').on('hidden.bs.select', function(e) {
    $('.boat-status-select .filter-option').removeClass().addClass('filter-option pull-left ' + $(this).find(":selected").data('state'));
});

// SIDEBAR TOGGLE
$('.sidebar-toggle').click(function(event) {
    $('.dashboard-sidebar').toggleClass('active');
    $('body.dashboard .bs-site-wrapper').toggleClass('sidebar-active');
});

// DRAG AND DROP UPLOADERS ON PICS & VIDEOS 
$(document).ready(function($) {
    for (i = 1; i <= $('.dropzone-trigger').length; i++) {

        wat = $("#dzmedia" + i).dropzone({
            url: "",
            dictDefaultMessage: ''
        });
    }
});

// TRIPS MORE DETAILS 
$('.more-detail, .bs-trip .trip-state').click(function(event) {
    $(this).closest('.bs-trip').toggleClass('collapsed expanded');
});

// CAPTAINS INFO TOOLTIPS 
$(document).ready(function() {
    var tp1 = $('.tooltip-assign-cap').tooltipster({
        // trigger: 'click',
        // interactive: true
        side: 'bottom'
    });

    setTimeout(function() { tp1.tooltipster('open'); }, 1000);
    setTimeout(function() { tp1.tooltipster('close'); }, 5000);
});

// INSURANCE SELECTION 
$(document).ready(function() {
    $('.insurance-plans .selection .btn').click(function(event) {
        if (!$(this).closest('.insurance-plans').hasClass('selected')) {
            $('.insurance-plans.selected').removeClass('selected').find('.btn').html('Select').removeClass('selected disabled');
            $(this).addClass('disabled').html('Selected').closest('.insurance-plans').addClass('selected');
            $('.insurance-plans.selected').find('.btn')
        }
    });

    $('.own-insurance.tooltipster').tooltipster({
        trigger: 'click',
        interactive: true
    });
});

// ACTIVATE SELECTS
(function() {
    [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {    
        new SelectFx(el);
    } );
})();

$('.collapsible .toggle').click(function(event) {
    $(this).toggleClass('in');
    $(this).next('.collapse').collapse('toggle');
});