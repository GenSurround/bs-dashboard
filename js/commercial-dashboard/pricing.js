$(document).ready(function($) {

    // TRIP LENGTH & PRICING
    var full_day_custom_price = document.getElementById('full-day-custom-price');
    full_day_custom_price.initialized = false;

    $('.pricing-selector input.bs-checkbox').click(function(event) {
        $(this).closest('.pricing-selector').toggleClass('unset');
    });

    $('.pricing-selector.full-day .custom-price').hide();

    $('.pricing-selector.full-day .set-own-price').click(function(event) {
        $('.fixed-price').hide(0, function() {
            $('.custom-price').show(0, function() {
                if (!full_day_custom_price.initialized) {

                    var smart = Number(full_day_custom_price.getAttribute('data-smart'));

                    noUiSlider.create(full_day_custom_price, {
                        start: [smart],
                        connect: [true, false],
                        range: {
                            'min': 100,
                            'max': 5000
                        },
                        tooltips: true,
                        format: wNumb({
                            decimals: 0,
                            thousand: ',',
                            prefix: '$'
                        })
                    });

                    full_day_custom_price.noUiSlider.on('update', function(values, handle, unencoded, tap, positions) {
                        var low = Number(full_day_custom_price.getAttribute('data-low'));
                        var high = Number(full_day_custom_price.getAttribute('data-high'));
                        var value = Number(values[0].slice(1, 10).replace(',', ''));
                        var metrorenter = document.getElementById('metrorenter');
                        var status = '';

                        if (value < low) {
                            if (status != 'verylikely') {
                                status = 'verylikely';
                                // console.log(value + ' - ' + status + ' - low: ' + low + ' - high: ' + high);
                                metrorenter.className = 'metrorenter row ' + status;

                            }
                        } else if (value <= high && value >= low) {
                            if (status != 'likely') {
                                status = 'likely';
                                // console.log(value + ' - ' + status + ' - low: ' + low + ' - high: ' + high);
                                metrorenter.className = 'metrorenter row ' + status;

                            }
                        } else if (value > high) {
                            if (status != 'unlikely') {
                                status = 'unlikely';
                                // console.log(value + ' - ' + status + ' - low: ' + low + ' - high: ' + high);
                                metrorenter.className = 'metrorenter row ' + status;

                            }
                        }
                    });

                    full_day_custom_price.initialized = true;
                } else {
                    full_day_custom_price.noUiSlider.set(window.full_day_custom_price_value);
                }
            });
        });
    });

    $('.pricing-selector.full-day .save-own-price').click(function(event) {
        $('.pricing-selector.full-day .custom-price').hide(0, function() {
            $('.pricing-selector.full-day .fixed-price').show(0, function() {
                window._value = full_day_custom_price.noUiSlider.get();
                $('.pricing-selector.full-day .fixed-price .amount').html(window._value);
                $('#metrorenter').removeClass('verylikely likely unlikely');
            });
        });
    });

    var full_day_price = document.getElementById('full-day-price');

    if (full_day_price) {
        noUiSlider.create(full_day_price, {
            start: [7],
            connect: [true, false],
            step: 1,
            range: {
                'min': 0,
                'max': 15
            },
            tooltips: true,
            format: wNumb({
                decimals: 0,
                thousand: ',',
                postfix: '%'
            })
        });
    }
    // END TRIP LENGTH & PRICING


    var custom_trips = [];

    $(".add-custom-trip").click(function(event) {

        var index = custom_trips.length;
        custom_trips.push({
            ID: "ct" + index,
            container: $(".custom-trip-slot")[index],
            index: index
        });

        custom_trip(custom_trips[index]);
        $(this).detach().appendTo($(".custom-trip-slot")[index + 1]);

    });


    function custom_trip(trip) {

        var source = $("#custom-trip-template").html();
        var template = Handlebars.compile(source);

        var context = {
            trip_ID: trip.ID,
            index: trip.index + 1
        };
        var html = template(context);

        $(trip.container).append(html);

        trip.hours = document.getElementById(context.trip_ID + "_hours");
        if (trip.hours) {
            noUiSlider.create(trip.hours, {
                start: [15],
                connect: [true, false],
                step: 1,
                range: {
                    'min': 6,
                    'max': 24
                },
                tooltips: true,
                format: wNumb({
                    decimals: 0
                }),
                pips: {
                    mode: 'range',
                    values: [6, 24],
                    density: 100
                }
            });
        }

        trip.days = document.getElementById(context.trip_ID + "_days");
        if (trip.days) {
            noUiSlider.create(trip.days, {
                start: [3],
                connect: [true, false],
                step: 1,
                range: {
                    'min': 1,
                    'max': 7
                },
                tooltips: true,
                format: wNumb({
                    decimals: 0
                }),
                pips: {
                    mode: 'range',
                    values: [6, 24],
                    density: 100
                }
            });
        }

        trip.weeks = document.getElementById(context.trip_ID + "_weeks");
        if (trip.weeks) {
            noUiSlider.create(trip.weeks, {
                start: [2],
                connect: [true, false],
                step: 1,
                range: {
                    'min': 1,
                    'max': 4
                },
                tooltips: true,
                format: wNumb({
                    decimals: 0
                }),
                pips: {
                    mode: 'range',
                    values: [6, 24],
                    density: 100
                }
            });
        }

        trip.price = document.getElementById(context.trip_ID + "_price");
        if (trip.price) {
            noUiSlider.create(trip.price, {
                start: [2500],
                connect: [true, false],
                step: 1,
                range: {
                    'min': 100,
                    'max': 5000
                },
                tooltips: true,
                format: wNumb({
                    decimals: 0,
                    thousand: ',',
                    prefix: '$'
                })
            });
        }

        $(trip.days).hide();
        $(trip.weeks).hide();


        $(trip.container).find('.radio-checkbox .checkbox').click(function() {
            var checkedState = $(this).prop('checked');
            $(this).closest('.radio-checkbox').find('.checkbox:checked').each(function() {
                $(this).prop('checked', false);
                $($(this).data("slider")).hide();
            });
            $($(this).data("slider")).show();
            $(this).prop('checked', checkedState);
        });

        $(trip.container).find(".values").hide();

        $(trip.container).find('#' + trip.ID + '_save').click(function(event) {
            $(trip.container).find(".set").hide();
            $(trip.container).find(".values").show();



            var sliderKind = $(trip.container).find('.radio-checkbox .checkbox:checked').data("kind");


            $("#" + trip.ID + "_duration-value").html(eval("trip." + sliderKind).noUiSlider.get() + " " + sliderKind);
            $("#" + trip.ID + "_price-value").html(trip.price.noUiSlider.get());
        });

        $(trip.container).find('#' + trip.ID + '_edit').click(function(event) {
            $(trip.container).find(".set").show();
            $(trip.container).find(".values").hide();
        });
    }

    var security_deposit_custom_price = document.getElementById('security-deposit-custom-price');
    security_deposit_custom_price.initialized = false;


    // SECURITY DEPOSIT

    $('.security-deposit .custom-price').hide();

    $('.security-deposit .set-own-price').click(function(event) {
        $('.security-deposit .fixed-price').hide(0, function() {
            $('.security-deposit .custom-price').show(0, function() {
                if (!security_deposit_custom_price.initialized) {

                    noUiSlider.create(security_deposit_custom_price, {
                        start: [1000],
                        connect: [true, false],
                        range: {
                            'min': 100,
                            'max': 5000
                        },
                        tooltips: true,
                        format: wNumb({
                            decimals: 0,
                            thousand: ',',
                            prefix: '$'
                        })
                    });

                    security_deposit_custom_price.initialized = true;
                } else {
                    security_deposit_custom_price.noUiSlider.set(window.security_deposit_custom_price_value);
                }
            });
        });
    });

    $('.security-deposit .save-own-price').click(function(event) {
        $('.security-deposit .custom-price').hide(0, function() {
            $('.security-deposit .fixed-price').show(0, function() {
                window.security_deposit_custom_price_value = security_deposit_custom_price.noUiSlider.get();
                $('.security-deposit .fixed-price .amount').html(window.security_deposit_custom_price_value);
            });
        });
    });
});
