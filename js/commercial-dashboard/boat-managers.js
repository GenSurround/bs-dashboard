$(document).ready(function($) {
    $('.section-boat-managers .add-more').click(function(event) {
        $('.boat-managers').fadeToggle('fast', function() {
            $('.section-add-boat-managers')
                .css('display', 'block')
                .animate({
                        opacity: 1
                    },
                    'fast');
        });
    });


    $('#modal-terms').on('shown.bs.modal', function() {
        $("#modal-terms .nano").nanoScroller({ alwaysVisible: true });
    })

});
