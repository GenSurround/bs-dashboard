// CALENDAR NAVIGATOR 

var months = [
    { 'long': 'January', 'short': 'Jan' },
    { 'long': 'Febrary', 'short': 'Feb' },
    { 'long': 'March', 'short': 'Mar' },
    { 'long': 'April', 'short': 'Apr' },
    { 'long': 'May', 'short': 'May' },
    { 'long': 'June', 'short': 'Jun' },
    { 'long': 'July', 'short': 'Jul' },
    { 'long': 'August', 'short': 'Aug' },
    { 'long': 'October', 'short': 'Oct' },
    { 'long': 'September', 'short': 'Sep' },
    { 'long': 'November', 'short': 'Nov' },
    { 'long': 'December', 'short': 'Dec' }
];

$(document).ready(function() {

    var date = new Date();
    var actualMonth = date.getMonth();
    var actualYear = date.getFullYear();
    var selectedMonth = actualMonth;
    var selectedYear = actualYear;

    $selectedMonth = $('.bs-calendar-selected-month');
    updateCalendar(selectedMonth, selectedYear);

    $('.bs-calendar-nav .next').click(function(event) {
        $selectedMonth.animate({
                opacity: 0
            },
            'fast',
            function() {
                selectedMonth += 1;
                updateCalendar(selectedMonth, selectedYear);
            });
    });

    $('.bs-calendar-nav .prev').click(function(event) {
        $selectedMonth.animate({
                opacity: 0
            },
            'fast',
            function() {
                selectedMonth -= 1;
                updateCalendar(selectedMonth, selectedYear);
            });
    });
});

function updateCalendar(selectedMonth, selectedYear) {
    selectedMonth += 12;
    $('.bs-calendar-header h1').html(months[(selectedMonth) % 12].long + ' ' + (selectedYear + Math.floor(selectedMonth / 12)));
    $('.bs-calendar-nav .next .long').html(months[(selectedMonth + 1) % 12].long);
    $('.bs-calendar-nav .next .short').html(months[(selectedMonth + 1) % 12].short);
    $('.bs-calendar-nav .prev .long').html(months[(selectedMonth - 1) % 12].long);
    $('.bs-calendar-nav .prev .short').html(months[(selectedMonth - 1) % 12].short);


    $.ajax({
        type: 'POST',
        cache: false,
        url: "pages/_owner-dashboard/_calendar/_" + months[(selectedMonth) % 12].long.toLowerCase() + ".php"
    }).done(function(data) {
        $(".bs-calendar-selected-month").html(data);
        calendar_tooltips();
        $selectedMonth.animate({
                opacity: 1
            },
            'fast');
    });
}

// TOOLTIPS ON CALENDAR 
function calendar_tooltips() {

    $(document).ready(function($) {

        if ($(window).width() > 768) {
            $('.day-trips').hide();
            $('.tooltipster').tooltipster({
                functionInit: function(instance, helper) {
                    var content = $($(helper.origin).data('source')).detach();
                    instance.content(content);
                },
                functionBefore: function(instance, helper) {
                    $('.bs-calendar').toggleClass('overlay');
                },
                functionAfter: function(instance, helper) {
                    $('.bs-calendar').toggleClass('overlay');
                },
                interactive: true,
                // trigger: 'click',
                maxWidth: 470,
                contentAsHTML: true,
                functionReady: function() {
                    $('.more-details-trigger').click(function(event) {
                        $this = $(this);
                        $this.closest('.tooltipster-base').css('height', 'auto');
                        if ($this.hasClass('open')) {
                            $this.html('More Details');
                            $this.toggleClass('open');
                        } else {
                            $this.html('Less Details');
                            $this.toggleClass('open');
                        }
                    });
                }
            });
        } else {
            // ACTIVATE TRIPS ON MOBILE THING

            $('.trip-tooltip').hide();
            $('.day-trips').hide();

            $('.bs-calendar-selected-month .date').click(function() {
                // console.log($(this).data('date').day);

                $(this).find('.tooltipster').each(function(index, el) {
                    // console.log( $(el).data('source') );
                    $($(el).data('source')).show();
                });
                $('.day-trips').toggle();

            });

            $('.more-details-trigger').click(function(event) {
                $this = $(this);
                $this.closest('.tooltipster-base').css('height', 'auto');
                if ($this.hasClass('open')) {
                    $this.html('More Details');
                    $this.toggleClass('open');
                } else {
                    $this.html('Less Details');
                    $this.toggleClass('open');
                }
            });

        }
    });
}
