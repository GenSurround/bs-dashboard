if ($("#cvv").length) { $("#cvv").datepicker(); }

$('.saved-credit-card-item-header').click(function(event) {
    $(this).toggleClass('active')
        .closest('.saved-credit-card-item').find('.saved-credit-card-item-body')
        .collapse('toggle');
});


$(".edit-inputs").click(function(event) {
    if ($(this).closest('.editable-section.no-editable').length) {

        $(this).closest('.editable-section')
            .removeClass('no-editable').addClass('editable')
            .find('.no-editable')
            .removeClass('no-editable').addClass('editable').attr('readonly', false);

    } else {

        $(this).closest('.editable-section')
            .removeClass('editable').addClass('no-editable')
            .find('.editable')
            .removeClass('editable').addClass('no-editable').attr('readonly', 'readonly');
    }

    $(this).hide();
});


$('#save-card').click(function(event) {
	$('#add-card').collapse('toggle'); 
	$('#no-card').addClass('hidden');
	$('#yes-card').removeClass('hidden');
});