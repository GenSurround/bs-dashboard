$(document).ready(function($) {

//  Messenger Nav Action ------------------//

	$('.all-messages h2, .sender-profile .message-actions span').addClass('close-menu');

	$('.all-messages h2').on('click', function(){
		if( $(this).hasClass('close-menu')){
			$('.all-messages-dropdown').show();
		    $(this).removeClass('close-menu').addClass('open-menu');
		}
		else if( $(this).hasClass('open-menu')){
			$('.all-messages-dropdown').fadeOut('fast');
		    $(this).removeClass('open-menu').addClass('close-menu');
		    $('.all-messages-dropdown .search').find('input').hide();

		}
	});

    $('.all-messages-dropdown .search').on('click', function(){
    	$(this).find('input').show();
    });

     $('.all-messages-dropdown .read, .all-messages-dropdown .unread').on('click', function(){
    	$('.all-messages-dropdown').fadeOut('fast');
		$('.all-messages h2').removeClass('open-menu').addClass('close-menu');
		$('.all-messages-dropdown .search').find('input').hide();
    });


     $('.all-messages-dropdown input').keypress(function (e) {
  		if (e.which == 13) {
  			$('.all-messages-dropdown').fadeOut('fast');
  			$(this).val('').hide();

			$('.all-messages h2').removeClass('open-menu').addClass('close-menu');
    		return false;  
  		}
	});


	$('.sender-profile .message-actions span').on('click', function(){
		if( $(this).hasClass('close-menu')){
			$('.message-actions-drop-down').fadeIn('fast');
		    $(this).removeClass('close-menu').addClass('open-menu');
		}
		else if( $(this).hasClass('open-menu')){
			$('.message-actions-drop-down').fadeOut('fast');
		    $(this).removeClass('open-menu').addClass('close-menu');
		}
	})


	 $('.message-actions-drop-down li').on('click', function(){
    	$('.message-actions-drop-down').fadeOut('fast');
		    $(this).removeClass('open-menu').removeClass('open-menu').addClass('close-menu');
    });





// Conversation has sidebar or not   ---------------------------- //	

	$('.list-message.has-sidebar').on('click', function(){
		$('.welcome-message').fadeOut('slow');
		$('.messenger-main-board').removeClass('wide-board');
		$('.messenger-right-sidebar').show();
		$('.messenger-right-top-section, .messenger-right-bottom-section').fadeIn('fast', function(){
			$(".conversation-board").nanoScroller();

		});

		if ($(window).width() < 995){
			$('.list-messages').fadeOut('slow');
	    }

	});

	$('.list-message.no-sidebar').on('click', function(){
		$('.welcome-message').fadeOut('slow');
		$('.messenger-main-board').addClass('wide-board');
		$('.messenger-right-sidebar').hide();
		$('.messenger-right-top-section, .messenger-right-bottom-section').fadeIn('fast', function(){
				$(".conversation-board").nanoScroller();
		});

		if ($(window).width() < 995){
			$('.list-messages').fadeOut('slow');
	    }
	});



	$('.sender-profile .back').on('click', function(){
		$('.messenger-right-top-section, .messenger-right-bottom-section').fadeOut('fast');
		$('.list-messages').fadeIn('slow', function(){
			$(".list-messages").nanoScroller();
		});

    });


	if ( $(window).width() < 995){
		var mobileHeight = $(window).height();
		$('.conversation-board').css({'height': mobileHeight});
		$('.list-messages').css({'height': mobileHeight});
	}

	$(".list-messages").nanoScroller();

	$(".right-sidebar-trip-items").nanoScroller();

	if ( $(window).width() >= 995){
		$('#input-default').emojiPicker({
			height: '250px',
			iconBackgroundColor: 'white'
		});
	}

	$('.delete-message').on('click', function(){

		$(this).parent().parent().css({'background':'#ffb9b9', 'border-left':'3px solid red'}).delay(300).fadeOut('fast');
	});

	var winHeightMessenger = $(window).height();

	if ( $(window).width() < 995){
		$('.messenger-right-sidebar').css({'top': winHeightMessenger});
	}
	

	$('.cta-sidebar h4').on('click', function(){
		$('.messenger-right-sidebar').animate({'top':0}, 500);

	});

	$('.back-to-chat span').on('click', function(){
		$('.messenger-right-sidebar').animate({'top':winHeightMessenger}, 500);

	});

	$("textarea").keydown(function(e){
// Enter was pressed without shift key
if (e.keyCode == 13 && !e.shiftKey)
{
    // prevent default behavior
    e.preventDefault();
}
});


/*$(window).resize(function(){
	if ( $(window).width() >=995){
		$('.messenger-right-top-section, .messenger-right-bottom-section').fadeIn('fast', function(){
				$(".conversation-board").nanoScroller();

		});
	}
	else if ( $(window).width() < 995){
		$('.messenger-right-top-section, .messenger-right-bottom-section').hide();
		$('.welcome-message').hide();
		$('.list-messages').fadeIn('fast');
	}


});*/





	



});
