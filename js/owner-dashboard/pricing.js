$(document).ready(function($) {
    var full_day_price = document.getElementById('full-day-price');
    var custom_price = document.getElementById('custom-price');
    custom_price.initialized = false;

    $('.pricing-selector input.bs-checkbox').click(function(event) {
        $(this).closest('.pricing-selector').toggleClass('unset');
    });

    $('.custom-price').hide();
    $('.set-own-price').click(function(event) {
        $('.fixed-price').hide(0, function() {
            $('.custom-price').show(0, function() {
                if (!custom_price.initialized) {

                    var smart = Number(custom_price.getAttribute('data-smart'));

                    noUiSlider.create(custom_price, {
                        start: [smart],
                        connect: [true, false],
                        range: {
                            'min': 100,
                            'max': 5000
                        },
                        tooltips: true,
                        format: wNumb({
                            decimals: 0,
                            thousand: ',',
                            prefix: '$'
                        })
                    });

                    custom_price.noUiSlider.on('update', function(values, handle, unencoded, tap, positions) {
                        var low = Number(custom_price.getAttribute('data-low'));
                        var high = Number(custom_price.getAttribute('data-high'));
                        var value = Number(values[0].slice(1, 10).replace(',', ''));
                        var metrorenter = document.getElementById('metrorenter');
                        var status = '';

                        if (value < low) {
                            if (status != 'verylikely') {
                                status = 'verylikely';
                                // console.log(value + ' - ' + status + ' - low: ' + low + ' - high: ' + high);
                                metrorenter.className = 'metrorenter row ' + status;

                            }
                        } else if (value <= high && value >= low) {
                            if (status != 'likely') {
                                status = 'likely';
                                // console.log(value + ' - ' + status + ' - low: ' + low + ' - high: ' + high);
                                metrorenter.className = 'metrorenter row ' + status;

                            }
                        } else if (value > high) {
                            if (status != 'unlikely') {
                                status = 'unlikely';
                                // console.log(value + ' - ' + status + ' - low: ' + low + ' - high: ' + high);
                                metrorenter.className = 'metrorenter row ' + status;

                            }
                        }
                    });

                    custom_price.initialized = true;
                } else {
                    custom_price.noUiSlider.set(window.custom_price_value);
                }
            });
        });
    });

    $('.save-own-price').click(function(event) {
        console.log('wata');
        $('.custom-price').hide(0, function() {
            $('.fixed-price').show(0, function() {
                window.custom_price_value = custom_price.noUiSlider.get();
                $('.fixed-price .amount').html(window.custom_price_value);
                $('#metrorenter').removeClass('verylikely likely unlikely');
            });
        });
    });

    if (full_day_price) {
        noUiSlider.create(full_day_price, {
            start: [7],
            connect: [true, false],
            range: {
                'min': 0,
                'max': 15
            },
            tooltips: true,
            format: wNumb({
                decimals: 0,
                thousand: ',',
                postfix: '%'
            })
        });
    }

});
