// CALENDAR NAVIGATOR 

var months = [
    { 'long': 'January', 'short': 'Jan' },
    { 'long': 'Febrary', 'short': 'Feb' },
    { 'long': 'March', 'short': 'Mar' },
    { 'long': 'April', 'short': 'Apr' },
    { 'long': 'May', 'short': 'May' },
    { 'long': 'June', 'short': 'Jun' },
    { 'long': 'July', 'short': 'Jul' },
    { 'long': 'August', 'short': 'Aug' },
    { 'long': 'October', 'short': 'Oct' },
    { 'long': 'September', 'short': 'Sep' },
    { 'long': 'November', 'short': 'Nov' },
    { 'long': 'December', 'short': 'Dec' }
];

$(document).ready(function() {

    var date = new Date();
    var actualMonth = date.getMonth();
    var actualYear = date.getFullYear();
    var selectedMonth = actualMonth;
    var selectedYear = actualYear;
    var monthStatus = [];

    var $selectedMonth = $('.bs-calendar-selected-month');

    updateCalendar(selectedMonth, selectedYear);

    $('.bs-calendar-nav .next').click(function(event) {
        selectedMonth += 1;
        changeMonth();
    });

    $('.bs-calendar-nav .prev').click(function(event) {
        selectedMonth -= 1;
        changeMonth();
    });

    function changeMonth() {
        $selectedMonth.animate({
                opacity: 0
            },
            'fast',
            function() {
                updateCalendar(selectedMonth, selectedYear);
            });
    }

    function updateCalendar(selectedMonth, selectedYear) {
        selectedMonth += 12;
        $('.bs-calendar-header h1').html(months[(selectedMonth) % 12].long + ' ' + (selectedYear + Math.floor(selectedMonth / 12)));
        $('.bs-calendar-nav .next .long').html(months[(selectedMonth + 1) % 12].long);
        $('.bs-calendar-nav .next .short').html(months[(selectedMonth + 1) % 12].short);
        $('.bs-calendar-nav .prev .long').html(months[(selectedMonth - 1) % 12].long);
        $('.bs-calendar-nav .prev .short').html(months[(selectedMonth - 1) % 12].short);

        $.ajax({
            type: 'POST',
            cache: false,
            url: "pages/_owner-dashboard/_calendar/_" + months[(selectedMonth) % 12].long.toLowerCase() + ".php"
        }).done(function(data) {
            $(".bs-calendar-selected-month").html(data);
            calendar_tooltips();
            $selectedMonth.animate({
                    opacity: 1
                },
                'fast');
        });
    }

    // TOOLTIPS ON CALENDAR 
    function calendar_tooltips() {

        $(document).ready(function($) {

            if ($(window).width() > 768) {
                $('.day-trips').hide();
                $('.tooltipster').tooltipster({
                    functionInit: function(instance, helper) {
                        var content = $($(helper.origin).data('source')).detach();
                        instance.content(content);
                    },
                    functionBefore: function(instance, helper) {
                        $('.bs-calendar').toggleClass('overlay');
                    },
                    functionAfter: function(instance, helper) {
                        $('.bs-calendar').toggleClass('overlay');
                    },
                    interactive: true,
                    maxWidth: 470,
                    contentAsHTML: true,
                    functionReady: function(instance, helper) {
                        $('.more-details-trigger').click(function(event) {
                            $this = $(this);
                            $this.closest('.tooltipster-base').css('height', 'auto');
                            if ($this.hasClass('open')) {
                                $this.html('More Details');
                                $this.toggleClass('open');
                            } else {
                                $this.html('Less Details');
                                $this.toggleClass('open');
                            }
                        });
                    }
                });
            } else {

                // ACTIVATE TRIPS ON MOBILE THING

                $('.trip-tooltip').hide();
                $('.day-trips').hide();

                $('.bs-calendar-selected-month .date').click(function() {
                    console.log($(this).data('date').day);

                    $(this).find('.tooltipster').each(function(index, el) {
                        $($(el).data('source')).show();
                    });
                    $('.day-trips').toggle();
                });

                $('.more-details-trigger').click(function(event) {
                    $this = $(this);
                    $this.closest('.tooltipster-base').css('height', 'auto');
                    if ($this.hasClass('open')) {
                        $this.html('More Details');
                        $this.toggleClass('open');
                    } else {
                        $this.html('Less Details');
                        $this.toggleClass('open');
                    }
                });

            }

            // Status Tooltip
            $('.bs-calendar-selected-month .date').each(function(index, element) {

                // template
                var availability_tooltip = {};
                availability_tooltip.source = $("#day-tooltipster").html();
                availability_tooltip.template = Handlebars.compile(availability_tooltip.source);

                availability_tooltip.context = {
                    day: $(this).data('date').day,
                    month: $(this).data('date').month,
                    year: $(this).data('date').year,
                    status: $(this).data('status')
                };

                if ($(this).attr("data-status") != "booked") {
                    availability_tooltip.html = availability_tooltip.template(availability_tooltip.context);
                    var clicky = false;

                } else {
                    availability_tooltip.html = "<div class=\"text-center\"><h2 class=\"red\"><i class=\"bs-icon-bs_warning bs-icon-lg\"></i> Action Denied</h2> <p>This day is already booked,<br> you cannot modify the status</p></div>";
                    clicky = true;
                }


                var tooltip = $(this).tooltipster({
                    functionInit: function(instance, helper) {
                        var content = availability_tooltip.html;
                        instance.content(content);
                    },
                    functionBefore: function(instance, helper) {
                        $('.bs-calendar').toggleClass('overlay');
                    },
                    functionAfter: function(instance, helper) {
                        $('.bs-calendar').toggleClass('overlay');
                    },
                    interactive: true,
                    trigger: 'custom',
                    triggerOpen: {
                        click: true,
                        tap: true
                    },
                    triggerClose: {
                        click: clicky,
                        tap: clicky
                    },
                    maxWidth: 470,
                    contentAsHTML: true,
                    functionReady: function(instance, helper) {

                        var fromDate = "#d-" + availability_tooltip.context.day + "-filter-from";
                        var toDate = "#d-" + availability_tooltip.context.day + "-filter-to";
                        var multi = false;

                        $('#d-' + availability_tooltip.context.day + '-' + tooltip.attr('data-status')).trigger('click');

                        // console.log('#d-' + availability_tooltip.context.day + '-' + availability_tooltip.context.status)

                        $("#d-" + availability_tooltip.context.day + "-multi").change(function(e) {
                            if ($(this).is(':checked')) {
                                $(fromDate + ", " + toDate).prop("disabled", false);
                                multi = true;
                                $("#d-" + availability_tooltip.context.day + "-save").addClass("disabled");
                            }
                        });

                        $("#d-" + availability_tooltip.context.day + "-single").change(function(e) {
                            if ($(this).is(':checked')) {
                                $(fromDate + ", " + toDate).prop("disabled", true);
                                multi = false;
                                $("#d-" + availability_tooltip.context.day + "-save").removeClass("disabled");
                            }
                        });

                        function activateToDatePicker(dateText, inst) {

                            if ($(toDate).length) {

                                if (window.toDateExist != availability_tooltip.context.day) {
                                    $("#d-" + availability_tooltip.context.day + "-save").addClass("disabled");
                                    $(toDate).datepicker("destroy");
                                    $(toDate)[0].value = dateText;
                                }

                                var dateBooked = detectBookedDayNext(dateTextToDate($(fromDate)[0].value));

                                // console.log(dateBooked.date);

                                $(toDate).datepicker({
                                    dateFormat: "dd/mm/yy",
                                    minDate: $(fromDate)[0].value,
                                    maxDate: dateBooked.date,
                                    onSelect: function(dateText, inst) {
                                        $(this).data('datepicker').inline = true;
                                        var booked = detectBookedDayNext(dateTextToDate($(fromDate)[0].value), dateTextToDate($(toDate)[0].value));
                                        if (booked.booked) {
                                            console.log(booked.date);
                                            console.log("You can’t perform this action because you have confirmed trips within the selected dates.");
                                        } else {
                                            $(this).data('datepicker').inline = false;
                                            $("#d-" + availability_tooltip.context.day + "-save").removeClass("disabled");
                                        }
                                    },
                                    beforeShowDay: function(date) {
                                        var day = date.getDate();
                                        if (day < 10) { day = "0" + day; }
                                        var status = $("#d" + day).attr("data-status");

                                        if (status == "unavailable") {
                                            return [true, 'ui-not-available', ''];
                                        } else if (status == "booked") {
                                            setTimeout(function() {

                                                var tooltip = $(".ui-booked").tooltipster({
                                                    functionInit: function(instance, helper) {
                                                        var content = "<div class=\"text-center\"><h2 class=\"red\"><i class=\"bs-icon-bs_warning bs-icon-lg\"></i> Action Denied</h2> <p>This day is already booked,<br> you cannot modify the status</p></div>";
                                                        instance.content(content);
                                                    },
                                                    zIndex: 99999999,
                                                    contentAsHTML: true
                                                });


                                            }, 10);
                                            return [false, 'ui-booked', 'This date is booked'];
                                        } else {
                                            return [true, '', ''];
                                        }
                                    }
                                });
                            }
                        }

                        if ($(fromDate).length) {
                            $(fromDate).datepicker({
                                dateFormat: "dd/mm/yy",
                                minDate: detectBookedDayPrev(dateTextToDate($(fromDate)[0].value)).date,
                                maxDate: $(fromDate)[0].value,
                                beforeShowDay: function(date) {
                                        var day = date.getDate();
                                        if (day < 10) { day = "0" + day; }
                                        var status = $("#d" + day).attr("data-status");

                                        if (status == "unavailable") {
                                            return [true, 'ui-not-available', ''];
                                        } else if (status == "booked") {
                                            setTimeout(function() {

                                                var tooltip = $(".ui-booked").tooltipster({
                                                    functionInit: function(instance, helper) {
                                                        var content = "<div class=\"text-center\"><h2 class=\"red\"><i class=\"bs-icon-bs_warning bs-icon-lg\"></i> Action Denied</h2> <p>This day is already booked,<br> you cannot modify the status</p></div>";
                                                        instance.content(content);
                                                    },
                                                    zIndex: 99999999,
                                                    contentAsHTML: true
                                                });


                                            }, 10);
                                            return [false, 'ui-booked', 'This date is booked'];
                                        } else {
                                            return [true, '', ''];
                                        }
                                    },
                                onSelect: activateToDatePicker
                            });
                        }

                        $(toDate).hover(function(e) {
                            console.log(window.toDateExist);
                            if (window.toDateExist != availability_tooltip.context.day) {
                                e.preventDefault();
                                window.toDateExist = availability_tooltip.context.day;
                                activateToDatePicker($(fromDate)[0].value, $(this));
                            }
                        });

                        // ON SAVE
                        $("#d-" + availability_tooltip.context.day + "-save").click(function(e) {

                            e.preventDefault();
                            var status = $("input[name=availability]:checked").attr("value");
                            var currentStatus = tooltip.attr('data-status');

                            var div = "";

                            for (var d = dateTextToDate($(fromDate)[0].value); d <= dateTextToDate($(toDate)[0].value); d.setDate(d.getDate() + 1)) {

                                div = "#d" + dateToDateObj(d).day;

                                if (status == 'available') {
                                    $(div).attr('data-status', status);
                                    $(div).find('.trips .status').html('');
                                } else if (status == 'unavailable') {
                                    $(div).attr('data-status', status);
                                    $(div).find('.trips .status').html('<img width="20" height="20" src="images/bs-calendar/bs_unavailable.svg" alt="unavailable"> N/A');
                                }
                            }
                            window.toDateExist = "";
                            tooltip.tooltipster('close');
                        });
                    }
                });
            });
        });
    }

    function detectBookedDayNext(dateFrom, dateTo) {
        var div = "";
        var booked = false;

        if (dateTo == undefined) {
            dateTo = new Date(dateFrom.getFullYear(), dateFrom.getMonth() + 1, 0);
        }

        for (var d = dateFrom; d <= dateTo; d.setDate(d.getDate() + 1)) {

            div = "#d" + dateToDateObj(d).day;

            if ($(div).attr("data-status") == "booked") {
                // console.log("You can’t perform this action because you have confirmed trips within the selected dates.");
                if (!booked) {
                    booked = true;
                    return { booked: booked, date: d };
                }
            }
        }
        return booked;
    }

    function detectBookedDayPrev(dateFrom, dateTo) {
        var div = "";
        var booked = false;

        if (dateTo == undefined) {
            dateTo = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), 1);
        }

        for (var d = dateFrom; d >= dateTo; d.setDate(d.getDate() - 1)) {

            div = "#d" + dateToDateObj(d).day;

            if ($(div).attr("data-status") == "booked") {
                // console.log("You can’t perform this action because you have confirmed trips within the selected dates.");
                if (!booked) {
                    booked = true;
                    return { booked: booked, date: d };
                }
            }
        }
        return booked;
    }

    // HELPERS
    function dateTextToDate(dateText) {
        var parts = dateText.split('/');
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }

    function dateToDateText(date) {
        var dateText = {};
        dateText.day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        dateText.month = date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
        dateText.year = date.getFullYear();
        return dateText.day + "/" + dateText.month + "/" + dateText.year;
    }

    function dateToDateTextPrevDay(date) {
        var dateText = {};
        dateText.day = (date.getDate() - 1) < 10 ? "0" + (date.getDate() - 1) : (date.getDate() - 1);
        dateText.month = date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
        dateText.year = date.getFullYear();
        return dateText.day + "/" + dateText.month + "/" + dateText.year;
    }

    function dateToDateObj(date) {
        var dateText = {};
        dateText.day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        dateText.month = date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
        dateText.year = date.getFullYear();
        return dateText;
    }

});
