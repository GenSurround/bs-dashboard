// PROGRESS CIRCLE ON SIDEBAR
$('#progress-circle').circleProgress({
    value: 0.33,
    size: 200,
    fill: "orange",
    thickness: 2.7777777777777777,
    startAngle: -Math.PI / 2,
});

// STATUS SELECT ON SIDEBAR
$('.boat-status-select').on('hidden.bs.select', function(e) {
    $('.boat-status-select .filter-option').removeClass().addClass('filter-option pull-left ' + $(this).find(":selected").data('state'));
});


// DRAG AND DROP UPLOADERS ON PICS & VIDEOS 
$(document).ready(function($) {
    for (i = 1; i <= $('.dropzone-trigger').length; i++) {

        wat = $("#dzmedia" + i).dropzone({
            url: "",
            dictDefaultMessage: ''
        });
    }
});

// TRIPS MORE DETAILS 
$('.more-detail, .bs-trip .trip-state').click(function(event) {
    $(this).closest('.bs-trip').toggleClass('collapsed expanded');
});

// CAPTAINS INFO TOOLTIPS 
$(document).ready(function() {
    var tp1 = $('.tooltip-assign-cap').tooltipster({
        // trigger: 'click',
        // interactive: true
        side: 'bottom'
    });

    setTimeout(function() { tp1.tooltipster('open'); }, 1000);
    setTimeout(function() { tp1.tooltipster('close'); }, 5000);
});

// INSURANCE SELECTION 
$(document).ready(function() {
    $('.insurance-plans .selection .btn').click(function(event) {
        if (!$(this).closest('.insurance-plans').hasClass('selected')) {
            $('.insurance-plans.selected').removeClass('selected').find('.btn').html('Select').removeClass('selected disabled');
            $(this).addClass('disabled').html('Selected').closest('.insurance-plans').addClass('selected');
            $('.insurance-plans.selected').find('.btn')
        }
    });

    $('.own-insurance.tooltipster').tooltipster({
        trigger: 'click',
        interactive: true
    });
});

// ACTIVATE SELECTS
(function() {
    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function(el) {
        new SelectFx(el);
    });
})();

$('.collapsible .toggle').click(function(event) {
    $(this).toggleClass('in');
    $(this).next('.collapse').collapse('toggle');
});


$(function() {
    var focusedElement;
    $(document).on('focus', '#copy-clipboard', function() {
        if (focusedElement == this) return; //already focused, return so user can now place cursor at specific point in input.
        focusedElement = this;
        setTimeout(function() { focusedElement.select(); }, 50); //select all text in any field on focus for easy re-entry. Delay sightly to allow focus to "stick" before selecting.
    });
});


document.getElementById("copy-button").addEventListener("click", function() {
    copyToClipboard(document.getElementById("copy-clipboard"));
});

function copyToClipboard(elem) {
      // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
          succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}
