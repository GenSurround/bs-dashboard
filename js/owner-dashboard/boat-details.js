// SLIDERS 
$(document).ready(function() {

    var year_built = document.getElementById('year-built-slider');
    var market_value = document.getElementById('market-value-slider');
    var dimension_slider = document.getElementById('dimension_slider');
    var passengers_slider = document.getElementById('passengers_slider');
    var sleeps_slider = document.getElementById('sleeps_slider');
    var bath_slider = document.getElementById('bath_slider');
    var cabin_slider = document.getElementById('cabin_slider');
    var engines_slider = document.getElementById('engines_slider');
    var horsepower_slider = document.getElementById('horsepower_slider');
    var max_speed_slider = document.getElementById('max-speed_slider');
    var tank_capacity_slider = document.getElementById('tank-capacity_slider');


    if (year_built) {
        noUiSlider.create(year_built, {
            start: [2006],
            connect: [true, false],
            step: 1,
            range: {
                'min': 2000,
                'max': 2016
            },
            tooltips: true,
            format: wNumb({
                decimals: 0
            })
        });
    }

    if (market_value) {
        noUiSlider.create(market_value, {
            start: [20000],
            connect: [true, false],
            range: {
                'min': 10000,
                'max': 100000
            },
            tooltips: true,
            format: wNumb({
                decimals: 0,
                thousand: ',',
                prefix: '$'
            })
        });
    }

    // SPECIFICATION SLIDERS

    if (dimension_slider) {
        noUiSlider.create(dimension_slider, {
            start: [47],
            connect: [true, false],
            range: {
                'min': 10,
                'max': 100
            },
            tooltips: true,
            format: wNumb({
                decimals: 0,
                thousand: ',',
                postfix: '’' 
            })
        });
    }

    if (passengers_slider) {
        noUiSlider.create(passengers_slider, {
            start: [5],
            connect: [true, false],
            step: 1,
            range: {
                'min': 0,
                'max': 10
            },
            tooltips: true,
            format: wNumb({
                decimals: 0
            })
        });
    }

    if (sleeps_slider) {
        noUiSlider.create(sleeps_slider, {
            start: [2],
            connect: [true, false],
            step: 1,
            range: {
                'min': 0,
                'max': 5
            },
            tooltips: true,
            format: wNumb({
                decimals: 0
            })
        });
    }

    if (bath_slider) {
        noUiSlider.create(bath_slider, {
            start: [2],
            connect: [true, false],
            step: 1,
            range: {
                'min': 0,
                'max': 5
            },
            tooltips: true,
            format: wNumb({
                decimals: 0
            })
        });
    }

    if (cabin_slider) {
        noUiSlider.create(cabin_slider, {
            start: [2],
            connect: [true, false],
            step: 1,
            range: {
                'min': 0,
                'max': 5
            },
            tooltips: true,
            format: wNumb({
                decimals: 0
            })
        });
    }

    if (horsepower_slider) {
        noUiSlider.create(horsepower_slider, {
            start: [260],
            connect: [true, false],
            range: {
                'min': 100,
                'max': 5000
            },
            tooltips: true,
            format: wNumb({
                decimals: 0,
                thousand: ',',
                postfix: ' hp'
            })
        });
    }

    if (engines_slider) {
        noUiSlider.create(engines_slider, {
            start: [4],
            connect: [true, false],
            step: 1,
            range: {
                'min': 1,
                'max': 10
            },
            tooltips: true,
            format: wNumb({
                decimals: 0
            })
        });
    }

    if (tank_capacity_slider) {
        noUiSlider.create(tank_capacity_slider, {
            start: [160],
            connect: [true, false],
            range: {
                'min': 50,
                'max': 500
            },
            tooltips: true,
            format: wNumb({
                decimals: 0,
                thousand: ',',
                postfix: ' gallons'
            })
        });
    }

    if (max_speed_slider) {
        noUiSlider.create(max_speed_slider, {
            start: [20],
            connect: [true, false],
            range: {
                'min': 5,
                'max': 100
            },
            tooltips: true,
            format: wNumb({
                decimals: 0,
                thousand: ',',
                postfix: ' mph'
            })
        });
    }
    
    // BOAT DETAILS INFO TOOLTIPS 
    $(document).ready(function() {
        var tp2 = $('.tooltip-registration').tooltipster({
            // trigger: 'click',
            // interactive: true,
            maxWidth: 350
        });

        setTimeout(function() { tp2.tooltipster('open'); }, 1000);
        setTimeout(function() { tp2.tooltipster('close'); }, 8000);
    });

});