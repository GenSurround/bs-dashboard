var $screen_xs = 480,
    $screen_sm = 768,
    $screen_md = 992,
    $screen_lg = 1200,
    $flagSearch = 0,
    $flagHelp = 0,
    $flagAccount = 0,
    $flagMenu = 0,
    $flagBodyVeil =0;


function mouseFcnHeader(){


	$('.triggerSearch').click(function() {
		
	 	if ($flagSearch==0) {
	 		$('.wrapper-header-search').addClass('tooltipShow');
	 		$('body').addClass('menuHeaderOn');
	 		$flagSearch=1;

	  		$('.wrapper-header-help').removeClass('tooltipShow');
	  		$('.wrapper-header-account').removeClass('tooltipShow');
	  		
	  		$flagAccount=0;
	 		$flagHelp=0;

	 	} else {
	 		$('.wrapper-header-search').removeClass('tooltipShow');
	 		$('body').removeClass('menuHeaderOn');
	 		$flagSearch=0;
	 	}

	 	// hideMenu();
	});

	$('.triggerHelp').click(function() {
		
	 	if ($flagHelp==0) {
	 		$('.wrapper-header-help').addClass('tooltipShow');
	 		$('body').addClass('menuHeaderOn');
	 		$flagHelp=1;

	 		$('.wrapper-header-search').removeClass('tooltipShow');
	 		$('.wrapper-header-account').removeClass('tooltipShow');
	 		
	 		$flagAccount=0;
	 		$flagSearch=0; 		
	 	} else {
	 		$('.wrapper-header-help').removeClass('tooltipShow');
	 		$('body').removeClass('menuHeaderOn');
	 		$flagHelp=0;
	 	}
	 	hideMenu();
	});

	$('.triggerAccount').click(function() {
		
	 	if ($flagHelp==0) {
	 		$('.wrapper-header-account').addClass('tooltipShow');
	 		$('body').addClass('menuHeaderOn');
	 		$flagAccount=1;

	 		$('.wrapper-header-search').removeClass('tooltipShow');
	 		$('.wrapper-header-help').removeClass('tooltipShow');
	 		
	 		$flagHelp=0;
	 		$flagSearch=0; 		
	 	} else {
	 		$('.wrapper-header-account').removeClass('tooltipShow');
	 		$('body').removeClass('menuHeaderOn');
	 		$flagAccount=0;
	 	}
	 	hideMenu();
	});


	$('.wrapper-hamburger').click(function() {
		if ($flagMenu==0) {
			$('body').addClass('menuOn');
			
			$flagMenu=1;
		} else if ($flagMenu==1) {
			$('body').removeClass('menuOn');
			$flagMenu=0;
		}
		$flagBodyVeil=1;
		hideElements();
	});

	$('.body-veil').click(function() {
		$flagBodyVeil=1;
		hideElements();
		hideMenu();
		
	});




}

function hideElements(){
	if ($(window).width() >= 768){
		$('.wrapper-header-search').removeClass('tooltipShow');
		$('.wrapper-header-help').removeClass('tooltipShow');
		$('.wrapper-header-account').removeClass('tooltipShow');
		$('body').removeClass('menuHeaderOn');
		$flagSearch=0;		
	}
	if ($flagBodyVeil==1) {
		$('.wrapper-header-search').removeClass('tooltipShow');
		$('.wrapper-header-help').removeClass('tooltipShow');
		$('.wrapper-header-account').removeClass('tooltipShow');
		$('body').removeClass('menuHeaderOn');
		$flagSearch=0;	
	}

	$flagBodyVeil=0;


	
	$( ".wrapper-tile.exp" ).removeClass('clickOn');
 	
}


function hideMenu(){
	$flagMenu=0;
	$('body').removeClass('menuOn');
}


function selectPickerHeader(){
	
	$('.selectpicker').selectpicker({
	  style: 'btn-info',
	  size: 4
	});

}



function searchHeight(){
	if ($(window).width() >= 768){
		var searchBoxHeight=$('.sectionSearch').offset().top;
		searchBoxHeight=searchBoxHeight;
		if ($(window).scrollTop() > searchBoxHeight) {
			
			$('.sectionSearch .wrapper-search').addClass('searchShow');	
		}else{
			$('.sectionSearch .wrapper-search').removeClass('searchShow');	
		}
	}else{
		var searchBoxHeight=$('.sectionSearch').offset().top;
		searchBoxHeight=searchBoxHeight+150;
		if ($(window).scrollTop() > searchBoxHeight) {	
			$('.sectionSearch .wrapper-search').addClass('searchShow');	
		}else{
			$('.sectionSearch .wrapper-search').removeClass('searchShow');	
		}
	}
}