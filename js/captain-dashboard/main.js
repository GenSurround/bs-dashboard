// PROGRESS CIRCLE ON SIDEBAR
$('#progress-circle').circleProgress({
    value: 0.33,
    size: 200,
    fill: "orange",
    thickness: 2.7777777777777777,
    startAngle: -Math.PI / 2,
});

// STATUS SELECT ON SIDEBAR
$('.boat-status-select').on('hidden.bs.select', function(e) {
    $('.boat-status-select .filter-option').removeClass().addClass('filter-option pull-left ' + $(this).find(":selected").data('state'));
});


// DRAG AND DROP UPLOADERS ON PICS & VIDEOS 
$(document).ready(function($) {
    for (i = 1; i <= $('.dropzone-trigger').length; i++) {

        wat = $("#dzmedia" + i).dropzone({
            url: "",
            dictDefaultMessage: ''
        });
    }
});

// TRIPS MORE DETAILS 
$('.more-detail, .bs-trip .trip-state').click(function(event) {
    $(this).closest('.bs-trip').toggleClass('collapsed expanded');
});

// CAPTAINS INFO TOOLTIPS 
$(document).ready(function() {
    var tp1 = $('.tooltip-assign-cap').tooltipster({
        // trigger: 'click',
        // interactive: true
        side: 'bottom'
    });

    setTimeout(function() { tp1.tooltipster('open'); }, 1000);
    setTimeout(function() { tp1.tooltipster('close'); }, 5000);
});

// INSURANCE SELECTION 
$(document).ready(function() {
    $('.insurance-plans .selection .btn').click(function(event) {
        if (!$(this).closest('.insurance-plans').hasClass('selected')) {
            $('.insurance-plans.selected').removeClass('selected').find('.btn').html('Select').removeClass('selected disabled');
            $(this).addClass('disabled').html('Selected').closest('.insurance-plans').addClass('selected');
            $('.insurance-plans.selected').find('.btn')
        }
    });

    $('.own-insurance.tooltipster').tooltipster({
        trigger: 'click',
        interactive: true
    });
});

// ACTIVATE SELECTS
(function() {
    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function(el) {
        new SelectFx(el);
    });
})();

$('.collapsible .toggle').click(function(event) {
    $(this).toggleClass('in');
    $(this).next('.collapse').collapse('toggle');
});

$(document).ready(function() {
    var trips = document.getElementById('trips-slider');
    if (trips) {
        noUiSlider.create(trips, {
            start: [1],
            connect: [true, false],
            step: 1,
            range: {
                'min': 1,
                'max': 10
            },
            tooltips: true,
            format: wNumb({
                decimals: 0
            })
        });
    }

    var length = document.getElementById('length-slider');
    if (length) {
        noUiSlider.create(length, {
            start: [30],
            connect: [true, false],
            step: 1,
            range: {
                'min': 1,
                'max': 100
            },
            tooltips: true,
            format: wNumb({
                postfix: "’",
                decimals: 0
            })
        });
    }

    var passengers = document.getElementById('passengers-slider');
    if (passengers) {
        noUiSlider.create(passengers, {
            start: [1],
            connect: [true, false],
            step: 1,
            range: {
                'min': 1,
                'max': 20
            },
            tooltips: true,
            format: wNumb({
                decimals: 0
            })
        });
    }
});

// ACTIVATE EDITABLE INPUTS
$(".edit-inputs").click(function(event) {
    if ($(this).closest('.editable-section.no-editable').length) {

        $(this).closest('.editable-section')
            .removeClass('no-editable').addClass('editable')
            .find('.no-editable')
            .removeClass('no-editable').addClass('editable').attr('readonly', false);

    } else {

        $(this).closest('.editable-section')
            .removeClass('editable').addClass('no-editable')
            .find('.editable')
            .removeClass('editable').addClass('no-editable').attr('readonly', 'readonly');
    }

    $(this).hide();
});

$("#payment-account-edit-trigger").click(function(event) {
    console.log('wat')
    $("#payment-account-info-values").toggleClass("hidden");
    $("#payment-account-info-edit").toggleClass("hidden");
});

$("#documentation-edit-trigger").click(function(event) {
    console.log('wat')
    $("#documentation-values").toggleClass("hidden");
    $("#documentation-edit").toggleClass("hidden");
});