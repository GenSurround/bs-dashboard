var touchstartX = 0;
var touchstartY = 0;
var touchendX = 0;
var touchendY = 0;
var touchmoveX = 0;
var touchmoveY = 0;
var threshold = 100;
var translationX = 0;
var translationY = 0;
var lastTranslationX = 0;
var lastTranslationY = 0;

var limits = {};
var motion = {};


var gesturedZone = document;
var movableZone = document.getElementById('dashboard-sidebar');

limits.open = movableZone.offsetWidth;

gesturedZone.addEventListener('touchstart', function(event) {
    touchstartX = event.changedTouches[0].screenX;
    touchstartY = event.changedTouches[0].screenY;
}, false);

gesturedZone.addEventListener('touchend', function(event) {
    touchendX = event.changedTouches[0].screenX;
    touchendY = event.changedTouches[0].screenY;
    // lastTranslationX = translationX > limits.open ? limits.open : translationX;
    // lastTranslationY = translationY > limits.open ? limits.open : translationY;

    console.log('mova: ' + limits.open + ' translationX: ' + translationX + ' translationY: ' + translationY);

    // if (lastTranslationX < threshold) { closeMovable(); }
}, false);

gesturedZone.addEventListener('touchmove', function(event) {
    touchmoveX = event.changedTouches[0].screenX;
    touchmoveY = event.changedTouches[0].screenY;
    handleGesture('drag');
}, false);

function handleGesture(gesture) {

    translationX = lastTranslationX + touchmoveX - touchstartX;
    translationY = lastTranslationY + touchmoveY - touchstartY;

    if (gesture == 'swipe' ) {
        if (touchendX < touchstartX) {
            console.log(swiped + 'left!');
        }
        if (touchendX > touchstartX) {
            console.log(swiped + 'right!');
        }
        if (touchendY < touchstartY) {
            console.log(swiped + 'down!');
        }
        if (touchendY > touchstartY) {
            console.log(swiped + 'left!');
        }
    } else if (gesture == 'drag' && window.outerWidth < 768) {

        console.log('mova: ' + limits.open + ' translationX: ' + translationX + ' translationY: ' + translationY);

        if (Math.abs(translationX) > Math.abs(translationY)) {
            // console.log(translationX)
            if (translationX > 0) {
                if (translationX < limits.open) {
                    movableZone.style.transform = "translateX(" + Number(translationX - limits.open) + "px )";
                }
            } else {
                movableZone.style.transform = "translateX(" + translationX + "px )";
                // if (translationX > -limits.open) {
                // }
            }
        }
    }
}

// function closeMovable() {
//     var closeTransition = Number(translationX - limits.open);

//     console.log('ct: ' + closeTransition);

//     function closing(closeTransition) {
//         closeTransition -= 1;
//         if (closeTransition < limits.open) {
//             movableZone.style.transform = "translateX(" + closeTransition + "px )";
//             console.log(closeTransition);
//         } else return;
//     }

//     setTimeout(function() { closing(closeTransition) }, 200);

//     touchstartX =
//         touchstartY =
//         touchendX =
//         touchendY =
//         touchmoveX =
//         touchmoveY =
//         translationX =
//         translationY =
//         lastTranslationX =
//         lastTranslationY = 0;
// }
