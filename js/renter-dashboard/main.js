// PROGRESS CIRCLE ON SIDEBAR
$('#progress-circle').circleProgress({
    value: 0.33,
    size: 200,
    fill: "orange",
    thickness: 2.7777777777777777,
    startAngle: -Math.PI / 2,
});

// STATUS SELECT ON SIDEBAR
$('.boat-status-select').on('hidden.bs.select', function(e) {
    $('.boat-status-select .filter-option').removeClass().addClass('filter-option pull-left ' + $(this).find(":selected").data('state'));
});

// SIDEBAR TOGGLE
$('.sidebar-toggle').click(function(event) {
    $('.dashboard-sidebar').toggleClass('active');
    $('body.dashboard .bs-site-wrapper').toggleClass('sidebar-active');
});

// DRAG AND DROP UPLOADERS ON PICS & VIDEOS 
$(document).ready(function($) {
    for (i = 1; i <= $('.dropzone-trigger').length; i++) {

        wat = $("#dzmedia" + i).dropzone({
            url: "",
            dictDefaultMessage: ''
        });
    }
});

// TRIPS MORE DETAILS 
// $('.more-detail, .bs-trip .trip-state').click(function(event) {
//     $(this).closest('.bs-trip').toggleClass('collapsed expanded');
// });

// CAPTAINS INFO TOOLTIPS 
$(document).ready(function() {
    var tp1 = $('.tooltip-assign-cap').tooltipster({
        // trigger: 'click',
        // interactive: true
        side: 'bottom'
    });

    setTimeout(function() { tp1.tooltipster('open'); }, 1000);
    setTimeout(function() { tp1.tooltipster('close'); }, 5000);
});

// INSURANCE SELECTION 
$(document).ready(function() {
    $('.insurance-plans .selection .btn').click(function(event) {
        if (!$(this).closest('.insurance-plans').hasClass('selected')) {
            $('.insurance-plans.selected').removeClass('selected').find('.btn').html('Select').removeClass('selected disabled');
            $(this).addClass('disabled').html('Selected').closest('.insurance-plans').addClass('selected');
            $('.insurance-plans.selected').find('.btn')
        }
    });

    $('.own-insurance.tooltipster').tooltipster({
        trigger: 'click',
        interactive: true
    });
});

// ACTIVATE SELECTS
(function() {
    [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {    
        new SelectFx(el);
    } );
})();

$('.collapsible .toggle').click(function(event) {
    $(this).toggleClass('in');
    $(this).next('.collapse').collapse('toggle');
});


// DEPLOY TRIPS
$(document).ready(function() {
    $('.trip-header h2').click(function(event) {
        var  element = $(this).parent().parent();
        element.toggleClass( "active" );
        element.siblings(".wrapper-body").toggleClass('active');

    });
});

// FAVORITE BOATS / FAVORITE CAPTAINS  

$(document).ready(function() {
    function init_shapeshift(){
        $(".fav-container").shapeshift({
            align:"left",
            minHeight: 800,
            gutterX: 25,
            gutterY: 25
        });
    }
    function fav_cta(){
        $('.fav-more').on('click', function(){
            $(this).parent().parent().parent().addClass('flip'); 
        });

        $('.flip-back').on('click', function(){
            $(this).parent().parent().parent().removeClass('flip'); 
        });

        $('.flip-container .heart').on('click', function(){
            $(this).parent().append('<div class="tooltip-heart"><h3>Are you sure you want to unlike this boat?</h3><span class="fav-yes">Yes</span><span class="fav-no">No</span></div>');
        });

       


    }

    $('.fav-container.fav-boats').load('pages/_renter-dashboard/_fav1.php', function() {
           init_shapeshift();
           fav_cta();
    });

    $('.fav-container.fav-captains').load('pages/_renter-dashboard/_fav_cap_1.php', function() {
           init_shapeshift();
           fav_cta();
    });



    $('.fav-nav li a').on('click', function(event){
        event.preventDefault();
        $('.fav-container').load(this.href, function() {
           init_shapeshift();
           fav_cta();
        });
    });

    $(document).on('click', '.flip-container .fav-yes', function(){
       $(this).parent().parent().parent().parent().remove();
        $(".fav-container").shapeshift({
            animateOnInit: true,
            align:"left",
            minHeight: 800,
            gutterX: 25,
            gutterY: 25
        });
    });

     $(document).on('click', '.flip-container .fav-no', function(){
        $(this).parent().remove();
    });


  

    $('.fav-nav li a').each(function(){
        $(this).on('click', function(){
            $('.fav-nav li').removeClass('active');
            $(this).parent().addClass('active');

        });

    });

});


