$(document).ready(function($) {
	$.extend($.easing,
		{
			def: 'easeOutQuad',
			easeInOutQuart: function (x, t, b, c, d) {
			if ((t/=d/2) < 1) {
			    return c/2*t*t*t*t + b;
			}
			return -c/2 * ((t-=2)*t*t*t - 2) + b;
	    }
	});

	var bottomPosition;
	var bodyTop;
	var bodyNow;
	bottomPosition = ($(window).height());
	
    if( $(window).width() < 993 ) {
    	$('.trip-details-sidebar').hide();
      	$('.trip-details-sidebar').css({'top':bottomPosition, 'left':0, 'right':'auto'});
      	$('.show-sidebar span').html('Your Boatsetter Experience').removeClass('sidebar-on').addClass('sidebar-off');
	}

	$(window).resize(function(){
		bottomPosition = ($(window).height());
		if( $(window).width() < 993 ) {
			$('.trip-details-sidebar').hide();
	      	$('.trip-details-sidebar').css({'top':bottomPosition, 'left':0, 'right':'auto'});
	      	$('.show-sidebar span').html('Your Boatsetter Experience').removeClass('sidebar-on').addClass('sidebar-off');
	      	$('body').css({'position':'relative'});
	      	$('.trip-details-sidebar').css({'position':'absolute'});
		}
		if( $(window).width() >= 993 ) {
			$('.show-sidebar span').delay(800).html('Your Boatsetter Experience').removeClass('sidebar-on').addClass('sidebar-off');
			$('.trip-details-sidebar').css({'position':'relative'});
			$('body').css({'position':'relative'});
			$('.trip-details-sidebar').css({'left':'auto', 'right':0, 'top':0});
			$('.trip-details-sidebar').show();
		}
	})

   	$('.show-sidebar span').addClass('sidebar-off');
	$('.show-sidebar span').on('click', function(){
		if( $('.show-sidebar span').hasClass('sidebar-off') ){
			bodyTop = $(window).scrollTop();
		    bodyNow = bodyTop;
			$('.trip-details-sidebar').css({'position':'fixed'});
			$('.trip-details-sidebar').show();
			$('.trip-details-sidebar').scrollTop(0);
			$('.trip-details-sidebar').animate({'top':0}, 800, 'easeInOutQuart', function(){
				$('.show-sidebar span').html('close').removeClass('sidebar-off').addClass('sidebar-on');
				$('.trip-details-sidebar').css({'overflow':'scroll'});
				$('body').delay(800).css({'position':'fixed'});
			});
		}
		else if( $('.show-sidebar span').hasClass('sidebar-on') ){
			$('body').css({'position':'relative'});
			$(window).scrollTop( bodyNow );
			$('.trip-details-sidebar').animate({'top':bottomPosition}, 800, 'easeInOutQuart',function(){
				$('.trip-details-sidebar').css({'position':'absolute'});
				$('.trip-details-sidebar').hide();
				$('.show-sidebar span').delay(800).html('Your Boatsetter Experience').removeClass('sidebar-on').addClass('sidebar-off');
			});
		}
	});
});
