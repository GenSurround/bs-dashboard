var toolbarCount = 0;
var documentHeight = 0;


$(document).ready(function($) {
	$( ".toolbar-item" ).each(function( index ) {
		toolbarCount = toolbarCount + $( this ).width(); 
	});
	toolbarCount = toolbarCount + 320; 
	documentHeight = $( document ).height();

	checkToolbar();
	getHeight();
	// setInterval(function(){ checkToolbar(); getHeight(); }, 10);
	toggleToolbar();
	// swipe();

	
	$('.menu-item > a').each(function( index ) {
		if ($(this).height() < 35) {
			$(this).addClass('expand');
		}
	});
	$('.submenu-item > a').each(function( index ) {
		if ($(this).height() < 35) {
			$(this).addClass('expand');
		}
	});
});


$( window ).resize(function() {
	checkToolbar();
	getHeight();

  	$('ul.toolbar').removeClass('open');
  	$('.menu-item').removeClass('active');
	$(".toolbar-item").removeClass('active');
	$(".menu").removeClass('active');
	$(".submenu").removeClass('active');

	$('.submenu-item > a').each(function( index ) {
		if ($(this).height() < 35) {
			$(this).addClass('expand');
		}
	});
	
	documentHeight = $( document ).height();
});

function swipe(){
		$(".swipe-area").swipe({
              swipeStatus:function(event, phase, direction, distance, duration, fingers)
                  {
                      if (phase=="move" && direction =="right") {
                           // $(".container").addClass("open-sidebar");
							if ($(window).width() < 769) {
								$('ul.toolbar').removeClass('open');
								$('li.toolbar-item').removeClass('active');
								$('li.menu-item').removeClass('active');

								$('#site-wrapper').addClass('sidebar-active');
								$('#dashboard-sidebar').addClass('active');
                           }
                           return false;
                      }
                      if (phase=="move" && direction =="left") {
                           // $(".container").removeClass("open-sidebar");
                           if ($(window).width() < 769) {
	                           	$('#site-wrapper').removeClass('sidebar-active');
								$('#dashboard-sidebar').removeClass('active');
                           }
                           return false;
                      }
                  }
          });
}

function checkToolbar(){
	// console.log(toolbarCount);
	var containerWidth = $('.container').width();
	if (toolbarCount >= containerWidth) {
		$('.dashboard-toolbar').removeClass('desktop').addClass('mobile');
	} else {
		$('.dashboard-toolbar').removeClass('mobile').addClass('desktop');
	}	
}
function getHeight(){
	console.log(documentHeight);
 	if ($('.dashboard-toolbar').hasClass('mobile')) {
 		$('ul.toolbar').height(documentHeight);
 	} else {
 		$('ul.toolbar').height(40);
 	}
}

function toggleToolbar(){

	$( ".toolbar-item > a" ).click(function() {

		if ($(this).parent().index() == 0) {  

			if ($('.dashboard-toolbar').hasClass('mobile')) {
		 		if ($('ul.toolbar').not('.open')) {
		 			$('#dashboard-sidebar').removeClass('active');
					$('#site-wrapper').removeClass('sidebar-active');
					$('ul.toolbar').addClass('open');
					
				}

		 		if ($(this).siblings().hasClass('active')) {
		 			$('ul.toolbar').removeClass('open');
		 			$(this).parent().removeClass('active');
		 			$('.menu').removeClass('active');
		 			$('.menu-item').removeClass('active');
		 			$('.submenu').removeClass('active');
		 		} else {
		 			$(".toolbar-item").removeClass('active');
		 			$('.menu-item').removeClass('active');
		 			$('.menu').removeClass('active');
		 			$(this).parent().addClass('active');
		 			$(this).siblings().addClass('active');
		 		}
		 	} else{
		 		if ($(this).siblings().hasClass('active')) {
		 			$(this).parent().removeClass('active');
		 			$(this).siblings().removeClass('active');
		 			$('.menu-item').removeClass('active');
		 			$('.menu').removeClass('active');

		 		} else {
		 			$(".toolbar-item").removeClass('active');
		 			$(".menu").removeClass('active');
		 			$('.menu-item').removeClass('active');
		 			$(this).parent().addClass('active');
		 			$(this).siblings().addClass('active');
		 		}
		 	}

		} else {

				if ($(this).siblings().hasClass('active')) {
		 			$(this).siblings().removeClass('active');
		 			$(this).parent().removeClass('active');
		 			$('.menu-item').removeClass('active');
		 			$('.menu').removeClass('active');
		 			$('.submenu').removeClass('active');
		 		} else {
		 			$(".toolbar-item").removeClass('active');
		 			$('.menu-item').removeClass('active');
		 			$('.menu').removeClass('active');
		 			$('.submenu').removeClass('active');
		 			$(this).parent().addClass('active');
		 			$(this).siblings().addClass('active');
		 		}
		
		}
	 	
	});


	$( ".menu-item > a" ).click(function() {
		if (!$(this).siblings().hasClass('active')) {
			$( ".menu-item" ).removeClass('active');
			$( ".submenu" ).removeClass('active');
			$(this).parent().addClass('active');
			$(this).siblings().addClass('active');
		} else {
			$('.menu-item').removeClass('active');
			$('.submenu').removeClass('active');
		}
	});

	$( ".submenu-item.bscaret > a" ).click(function() {
		if (!$(this).siblings().hasClass('active')) {
			$( ".submenu .submenu" ).removeClass('active');
			$( ".submenu .submenu-item.bscaret" ).removeClass('active');
			// $( ".submenu" ).removeClass('active');
			$(this).parent().addClass('active');
			$(this).siblings().addClass('active');
		} else {
			$('.submenu-item').removeClass('active');
			$(this).parent().removeClass('active');
			$(this).siblings().removeClass('active');
		}
	});


}

