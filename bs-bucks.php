<?php include 'includes/_head.php'; ?>
<?php 
    if($_GET["p"] != "") {
        $page = $_GET["p"]; 
    } else {
        $page = 'personal' ;
    }
?>

<body class="dashboard swipe-area">
    <?php include 'includes/_header.php'; ?>
    <?php include 'includes/_top-bar.php'; ?>

    <div id="site-wrapper" class="bs-site-wrapper">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <?php include 'includes/_bs-bucks/_sidebar.php'; ?>

                    <div class="main-panel col-xs-12 col-sm-9">

                        <?php 
                        include 'pages/_bs-bucks/_bs-bucks.php';
                        ?>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php include 'includes/_bs-bucks/_footer.php'; ?>