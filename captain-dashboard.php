<?php include 'includes/_head.php'; ?>
<?php 
    if($_GET["p"] != "") {
        $page = $_GET["p"]; 
        if ($page == "trips") {
            if($_GET["tab"] != "") {
                $tab = $_GET["tab"]; 
            } else {
                $tab = 'requests' ;
            }
        }
    } else {
        $page = 'availability' ;
    }
?>

<body class="dashboard swipe-area">
    <?php include 'includes/_header.php'; ?>
    <?php include 'includes/_top-bar.php'; ?>

    <div id="site-wrapper" class="bs-site-wrapper">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <?php include 'includes/_captain-dashboard/_sidebar.php'; ?>

                    <div class="main-panel col-xs-12 col-sm-9">

                        <?php 
                        include 'pages/_captain-dashboard/_'.$page.'.php';
                        ?>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php include 'includes/_captain-dashboard/_footer.php'; ?>