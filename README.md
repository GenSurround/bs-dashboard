# OWNER DASHBOARD

The main page is 'owner-dashboard.php'. We are using a bit of php for includes and ease of navigation.  Of course it should be discarded.

## INCLUDES

Basic and repeated elements in all pages

- _head.php (links and meta ...)
- _header.php (mockup, not the fully functional from the site)
- _footer.php (mostly scripts, not including the global footer from the site)
- _alert-bar.php (the bottom element)
- _top-bar (top menu)
- _sidebar.php (navigation)

## SIDEBAR
- It contains a bit of php code for the navigation.

## SCRIPTS
in _footer.php we make calls to the page specific scripts in "js/owner-dashboard/"

## PAGES

- _availability.php
    + the calendar uses ajax to include the months. Only December and January are included at "_calendar/" for example purposes.
    + the month files include the  "_trips-tooltips" files.

- _boat-details.php
- _boat-manages.php
- _captains.php
- _insurance.php
- _pics-videos.php
- _pricing.php
- _renter-profile.php
- _trips.php
    + include the "_trips/" files

## FONTS

We are using a custom webfont for icons.. it's located at: "/assets/fonts/boatsetter", we will be updating this font with each delivery.


This is a reduced version of the repository. You can download it here: 
https://GenSurround@bitbucket.org/GenSurround/bs-dashboard.git

and you can see it live here:
http://bs.mwxdigital.com/bs-dashboard