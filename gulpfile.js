var gulp = require('gulp');
var less = require('gulp-less');

gulp.task('compile-less', function() {
	return gulp.src('assets/less/style.less')
	.pipe(less())
	.pipe(gulp.dest(''));
});

gulp.task('watch-less', function() {
	gulp.watch('assets/less/**/*.less', ['compile-less']);
});

/* Task then running 'gulp' from terminal */
gulp.task('default', ['watch-less']);