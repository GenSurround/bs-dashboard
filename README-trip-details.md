# TRIP DETAILS

The main page is 'trip-details.php'. We are using a bit of php for includes and ease of navigation.  Of course it should be discarded.

## INCLUDES

- "includes/_trip-details/"

## SIDEBAR
- It contains a bit of php code for the navigation.

## SCRIPTS
in "_footer.php" we make calls to the page specific scripts in "js/trip-details/"

## PAGES
- "pages/_trip-details-pages/"

## FONTS

We are using a custom webfont for icons.. it's located at: "/assets/fonts/boatsetter", we will be updating this font with each delivery.

This is a reduced version of the repository. You can download it here: 
https://GenSurround@bitbucket.org/GenSurround/bs-dashboard.git

and you can see it live here:
http://bs.mwxdigital.com/bs-dashboard/trip-details.phpk