<?php include 'includes/_head.php'; ?>
<?php 
    if($_GET["p"] != "") {
        $page = $_GET["p"]; 
    } else {
       $page = 'notifications_main_file' ;
    }
?>
<body  class="dashboard notifications">
    <?php include 'includes/_header.php'; ?>
    <div  class="bs-site-wrapper">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                     <div class="notifications-container">
                          <?php include 'pages/_notifications/_'.$page.'.php'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'includes/_footer.php'; ?>