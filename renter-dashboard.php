<?php include 'includes/_head.php'; ?>
<?php 
    if($_GET["p"] != "") {
        $page = $_GET["p"]; 
        if ($page == "trips") {
            if($_GET["tab"] != "") {
                $tab = $_GET["tab"]; 
            } else {
                $tab = 'requests' ;
            }
        }
    } else {
        $page = 'availability' ;
    }
?>

<body class="dashboard renter-dashboard">
    <?php include 'includes/_header.php'; ?>
    <?php include 'includes/_top-bar.php'; ?>

    <div id="site-wrapper" class="bs-site-wrapper">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <?php include 'includes/_renter-dashboard/_sidebar.php'; ?>

                    <div class="main-panel wrapper-renter-dashboard col-xs-12 col-sm-9">

                        <?php 
                        include 'pages/_renter-dashboard/_'.$page.'.php';
                        ?>

                    </div>

                </div>
            </div>
        </div>
        <?php /*include 'includes/_renter-dashboard/_alert-bar.php';*/ ?>
    </div>

<?php include 'includes/_renter-dashboard/_footer.php'; ?>