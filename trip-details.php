<?php include 'includes/_head.php'; ?>
<?php 
    if($_GET["p"] != "") {
        $page = $_GET["p"]; 

    } else {
       $page = 'renter-profile' ;
    }
?>
<body class="dashboard swipe-area">
  <div class="modal-background"></div>
    <div class="modal-cancel-trip">
     <span class="close-modal"></span>
        <h2>Trip Cancellations</h2>
        <p>If an unexpected issue arises and you need to cancel this trip, please contact BoatSetter Customer Service by calling <span>(844) 232-738</span> or by email <span>contact@boatsetter.com</span> as soon as you possibly can.</p>
    </div>

    <div class="modal-captain-profile">
    <span class="close-modal"></span>

        <div class="modal-captain-profile-header row">
            <div class="profile-picture col-xs-12 col-sm-6">
                <figure>
                    <img src="images/placeholders/dude1.jpg">
                </figure>
                <div class="profile-items">
                    <h3>Pedro Manuel</h3>
                    <span class="user-rating">
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <a class="reviews" href="">(11)</a>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 align-right">
                <ul class="profile-ul">
                    <li><i class="bs-icon-bs_master blue"></i>Status<span>Admiral</span></li>
                    <li><i class="bs-icon-bs_experience blue"></i>Experience<span>11 trips</span></li>
                    <li><i class="bs-icon-bs_response blue"></i>Avg Resp<span>23 min</span></li>
                    <li><i class="bs-icon-bs_location blue"></i>Location<span>Miami</span></li>
                </ul>
            </div>
        </div>
        <div class="modal-captain-profile-about row">
              <p>There is nothing I love more than spending a day out on the water with family or friends. I've been boating most of my life, primarily in Venezuela- Morrocoy for over 30 . I own a 2006 23'' Champion Explorer in Aventura FL. Have had my boating license . MATER 25-100 Gr Ton & FIRST AID US COAST GUARD APPROVED.</p>
        </div>

        <div class="modal-captain-profile-skills row">
            <div class="col-xs-12 col-sm-8 align-left">
                <ul>
                    <li><i class="bs-icon-bs_fishing "></i>Fishing</li>
                    <li><i class="bs-icon-bs_fishing "></i>Snorkeling</li>
                    <li><i class="bs-icon-bs_fishing "></i>Sandbars</li>
                    <li><i class="bs-icon-bs_fishing "></i>Sightseeing</li>
                    <li><i class="bs-icon-bs_fishing "></i>Waterskiing</li>
                </ul>
         
            </div>
            <div class="col-xs-12 col-sm-4 align-right">
               <span class="contact">Send Message</span> <span class="select">Select</span>
               
            </div>
        </div>
      
    </div>
    <?php include 'includes/_header.php'; ?>
    <?php include 'includes/_trip-details/_top-bar-trip-details.php'; ?>

  

    <div class="bs-site-wrapper">
        <div class="container-fluid background-white">
            <div class="container">
                <div class="row">
              
                    <div class="trip-details-main-panel col-sm-12 col-md-9">
                        <?php 
                        include 'pages/_trip-details-pages/_'.$page.'.php';
                        ?>
                    </div>
                    <?php 
                    if ( ($page == 'renter-profile') || ($page == 'renter-profile-owner-confirmed') || ($page == 'renter-profile-all-confirmed') || ($page == 'renter-profile-captain-cancelled') || ($page == 'renter-profile-owner-cancelled') ){
                        include 'includes/_trip-details/_sidebar-renter-profile.php';
                    }
                  
                    else if (($page == 'owner-dashboard') || ($page == 'owner-dashboard-owner-confirmed') || ($page == 'owner-dashboard-all-confirmed')  ) {
                        include 'includes/_trip-details/_sidebar-owner-dashboard.php';
                    }
                    else if (($page == 'captain-dashboard') || ($page == 'captain-dashboard-captain-confirmed') | ($page == 'captain-dashboard-all-confirmed') ){
                        include 'includes/_trip-details/_sidebar-captain-dashboard.php';
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
    <div class="show-sidebar">
        <span class="show-boatsetter-experience">Your Boatsetter Experience</span>
    </div>
   
<?php include 'includes/_footer.php'; ?>