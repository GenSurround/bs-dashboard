<!DOCTYPE html>
<!--[if IEMobile 7 ]> 
<html lang="en-US" class="no-js iem7"> 
<![endif]-->
<!--[if lt IE 7 ]>    <html lang="en-US" class="no-js ie6"> 
    <![endif]-->
<!--[if IE 7 ]>       <html lang="en-US" class="no-js ie7"> 
    <![endif]-->
<!--[if IE 8 ]>       <html lang="en-US" class="no-js ie8"> 
    <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!-->
<html lang="en-US" class="no-js">
<!--<![endif]-->

<head>
    <!-- META TAGS -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- TITLE -->
    <title itemprop="name">Yacht Charters | Boatsetter.com</title>
    <!--[if IE]><link rel="shortcut icon" href="images/favicon.ico"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-cap#able" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="BoatSetter">
    <!-- MICROSOFT -->
    <meta name="msapplication-TileColor" content="#0066cc">
    <meta name="msapplication-TileImage" content="images/mstile-144x144.png">
    <meta name="msapplication-config" content="images/browserconfig.xml">
    <meta name="application-name" content="BoatSetter">
    <!-- MISC -->
    <link type="text/css" rel="stylesheet" id="reset-css" href="assets/css/reset.css" media="all">
    <!-- <link type="text/css" rel="stylesheet" id="google-font-css" href="http://fonts.googleapis.com/css?family=Open+Sans:700,300,800,400|Montserrat:400,700&#038;subset=latin" media="all"> -->
    <link type="text/css" rel="stylesheet" id="google-font-css" href="
    http://fonts.googleapis.com/css?family=Open+Sans:700,300,800,400,500,600" media="all">
    <link rel="stylesheet" type="text/css" href="assets/css/_third-party/nouislider.min.css">
     <link rel="stylesheet" type="text/css" href="assets/css/_third-party/nanoscroller.css">
      <link rel="stylesheet" type="text/css" href="assets/css/_third-party/nanoscroller.css">

    <link rel="stylesheet" type="text/css" href="assets/emojipicker/css/jquery.emojipicker.css">

    <link rel="stylesheet" type="text/css" href="assets/notifications/css/ns-style-other.css">
    
      <script src="assets/notifications/js/modernizr.custom.js"></script>



    <link type="text/css" rel="stylesheet" id="style-css" href="style.css" media="all">
    <link type="text/css" rel="stylesheet" id="slick-css" href="css/slick.css" media="all">
    <link rel="stylesheet" href="css/bootstrap-select.css">
    <link type="text/css" rel="stylesheet" id="slick-theme-css" href="css/slick-theme.css" media="all">
    <link rel="stylesheet" href="css/icons.css">

    <!-- <link rel="stylesheet" href="css/font-awesome.css"> -->
    <!-- favicon -->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--ICONMOON-->
    <!-- <link rel="stylesheet" href="icomoon/style.css"> -->
    <link rel="stylesheet" href="assets/fonts/boatsetter/style.css">

    <link rel="stylesheet" type="text/css" href="assets/js/fancybox/jquery.fancybox.css">

</head>
