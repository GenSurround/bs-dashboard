    <!-- .bs-site-wrapper -->

    <script type="text/javascript" src="assets/js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-ui-min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.slick.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.sticky-kit.min.js"></script>
    <script type="text/javascript" src="assets/js/circle-progress.min.js"></script>
    <script type="text/javascript" src="assets/js/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-slider.js"></script>
    <script type="text/javascript" src="assets/js/nouislider.min.js"></script>
    <script type="text/javascript" src="assets/js/wNumb.js"></script>
    <script type="text/javascript" src="assets/js/handlebars-v4.0.5.js"></script>
    <script type="text/javascript" src="js/jquery.waypoints.js"></script>
    <script type="text/javascript" src="js/header.js"></script>
    <script type="text/javascript" src="js/jquery.nanoscroller.min.js"></script>
    
    <script src="js/classie.js"></script>
    <script src="js/selectFx.js"></script>
    
    <script type="text/javascript" src="assets/js/tooltipster/dist/js/tooltipster.bundle.min.js"></script>
    <script src="js/bootstrap-select.js"></script>
    <script src="assets/js/dropzone.js"></script>

    <!-- TOOLBAR SCRIPTS-->
     <!-- <script src="assets/js/jquery.touchSwipe.min.js"></script> -->
     <script src="js/toolbar/main.js"></script>

     
    <!-- OWNER DASHBOARD SCRIPTS -->
    <script src="js/captain-dashboard/main.js"></script>

    <?php if ($page == 'trips_availability') { ?>
    <script src="js/captain-dashboard/availability.js"></script>
    <?php } ?>

    <script>
        $(".bs-site-wrapper .modal").appendTo("body");
        $(".bs-site-wrapper .modal").detach();
    </script>

</body>
</html>