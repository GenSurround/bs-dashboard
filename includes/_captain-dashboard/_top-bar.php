    <div class="container-fluid dashboard-toolbar white">
        <div class="container">
            <div class="row">
                <ul class="toolbar">
                    <li class="toolbar-item">
                        <a>Renter Profile</a>
                            <ul class="menu">
                                <li class="menu-item icon-trip">
                                    <a>Trips1</a>
                                </li>
                                <li class="menu-item icon-favorite">
                                    <a>Favorites</a>
                                    <ul class="submenu">
                                        <li class="submenu-item"><a href="#">Trips</a></li>
                                        <li class="submenu-item active"><a href="#">Favorites</a></li>
                                    </ul>
                                </li>
                            </ul> 
                    </li>
                    <li class="toolbar-item">
                        <a>Commercial Dashboard</a>
                        <ul class="menu">
                                <li class="menu-item icon-trip">
                                    <a>Trips1</a>
                                </li>
                                <li class="menu-item icon-favorite">
                                    <a>Favorites</a>
                                    <ul class="submenu">
                                        <li class="submenu-item"><a href="#">Trips</a></li>
                                        <li class="submenu-item active"><a href="#">Favorites</a></li>
                                    </ul>
                                </li>
                        </ul>
                    </li>
                    <li class="toolbar-item">
                        <a>Captain's Dashboard</a>
                        <ul class="menu">
                                <li class="menu-item icon-trip">
                                    <a>Trips1</a>
                                    <ul class="submenu">
                                        <li class="submenu-item"><a href="#">Trips</a></li>
                                        <li class="submenu-item active"><a href="#">Favorites</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item icon-favorite">
                                    <a>Favorites</a>
                                    
                                </li>
                        </ul>
                    </li>
                    <li class="toolbar-item">
                        <a>Personal Info</a>
                        <ul class="menu">
                                <li class="menu-item icon-trip">
                                    <a>Trips1</a>
                                </li>
                                <li class="menu-item icon-favorite">
                                    <a>Favorites</a>
                                    <ul class="submenu">
                                        <li class="submenu-item"><a href="#">Trips</a></li>
                                        <li class="submenu-item active"><a href="#">Favorites</a></li>
                                    </ul>
                                </li>
                        </ul>
                    </li>
                    <li class="toolbar-item">
                        <a>B$ Bucks</a>
                        <ul class="menu">
                                <li class="menu-item icon-trip">
                                    <a>Trips1</a>
                                </li>
                                <li class="menu-item icon-favorite">
                                    <a>Favorites</a>
                                    <ul class="submenu">
                                        <li class="submenu-item"><a href="#">Trips</a></li>
                                        <li class="submenu-item active"><a href="#">Favorites</a></li>
                                    </ul>
                                </li>
                        </ul>
                    </li>
                </ul>
                <div class='pull-right wrapper-messages'>
                    <a class="sidebar-toggle"><i class="bs-icon-bs_engines"></i></a>
                            &nbsp;
                    <a href="">
                    <i class="bs-icon-bs_messages"></i>
                    <span class="">Messages</span></a>
                    <div class="red-badge">2</div>
                </div>
            </div>
        </div>
    </div>