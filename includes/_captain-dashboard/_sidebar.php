<div id="dashboard-sidebar" class="dashboard-sidebar col-xs-12 col-sm-3">
    <div class="toggle"></div>
    <div class="user-info">
        <div class="progress">
            <div class="progress-text">                                     
            </div>
            <div id="progress-circle" class="progress-circle">
                <img class="img-circle" src="images/placeholders/cap1.jpg">
            </div>
        </div>

        <div class="user-name">Al Morin</div>
        <div class="small"><a href="">Edit Profile</a></div>

    </div>
    <br>
    <div class="dashboard-navigator">
        <ul> 
            <li class="collapsible <?php is_active( strstr($page,'trips'), 'trips' ); ?>" >
                <a href="captain-dashboard.php?p=trips-active_upcoming"><i class='bs-icon-bs_trips'></i> Trips</a>
                <div class="toggle"></div>
                <ul class="collapse in">
                    <li class="<?php is_active( $page ,'trips-active_upcoming'); ?>">
                        <a href="captain-dashboard.php?p=trips-active_upcoming">Active/Upcoming</a>
                    </li>
                    <li class="<?php is_active( $page ,'trips-pending'); ?>">
                        <a href="captain-dashboard.php?p=trips-pending">Pending</a>
                    </li>
                    <li class="<?php is_active( $page ,'trips-completed_cancelled'); ?>">
                        <a href="captain-dashboard.php?p=trips-completed_cancelled">Completed/Cancelled</a>
                    </li>
                    <li class="<?php is_active( $page ,'trips_availability'); ?>">
                        <a href="captain-dashboard.php?p=trips_availability">Availability</a>
                    </li>
                </ul>
            </li>
            <li class="<?php is_active( $page ,'boats_i_run'); ?>" >
                <a href="captain-dashboard.php?p=boats_i_run"><i class='bs-icon-bs_boat-details'></i> Boats I Run</a> 
            </li>
            <li class="<?php is_active( $page ,'find_boats_to_run'); ?>" >
                <a href="captain-dashboard.php?p=find_boats_to_run"><i class='bs-icon-bs_search'></i> Find Boats to Run</a> 
            </li>
            <li class="<?php is_active( $page ,'my_price'); ?>" >
                <a href="captain-dashboard.php?p=my_price"><i class='bs-icon-captain_dashboard_price'></i> My Price</a> 
            </li>
            <li class="<?php is_active( $page ,'my_earnings'); ?>" >
                <a href="captain-dashboard.php?p=my_earnings"><i class='bs-icon-bs_earning'></i> My Earnings</a> 
            </li>
            <li class="<?php is_active( $page ,'payment_settings'); ?>" >
                <a href="captain-dashboard.php?p=payment_settings"><i class='bs-icon-captain_dashboard_direct-pay'></i> Payment Settings (ACH)</a> 
            </li>
            <li class="<?php is_active( $page ,'promote'); ?>" >
                <a href="captain-dashboard.php?p=promote"><i class='bs-icon-captain_dashboard_promote'></i> 
                Promote</a> 
            </li>
            <li class="<?php is_active( $page ,'get_the_bst_app'); ?>" >
                <a href="captain-dashboard.php?p=get_the_bst_app"><i class='bs-icon-captain_dashboard_cel'></i> 
                Get the BST App</a> 
            </li>
        </li>
        </ul>
    </div>
</div>

<?php 
function is_active($page, $item) {
    if ($page == $item) {
        echo " active ";
    } else {
        return;
    }
}
?>