<div id="dashboard-sidebar" class="dashboard-sidebar col-xs-12 col-sm-3">
    <div class="toggle"></div>

    <?php if ( $page  == 'billing' ) { ?>

    <div class="user-info">
        <div class="progress">
            <div class="progress-text">

            </div>
            <div id="progress-circle" class="progress-circle">
                <img class="img-circle" src="images/placeholders/cap1.jpg">
            </div>
        </div>

        <div class="user-name">Al Morin</div>
        <div class="small"><a href="">Edit Profile</a></div>

    </div>

    <?php } ?>



    <br>

    <div class="dashboard-navigator">
        <ul> 
            <li class="<?php is_active( $page ,'personal'); ?>" >
                <a href="account-info.php?p=personal"><i class='bs-icon-bs_personal'></i> Personal Info</a> 
            </li>
            <li class="<?php is_active( $page ,'billing'); ?>" >
                <a href="account-info.php?p=billing"><i class='bs-icon-bs_billing'></i> Billing Info</a> 
            </li>
        </li>
        </ul>
    </div>
</div>

<?php 
function is_active($page, $item) {
    if ($page == $item) {
        echo " active ";
    } else {
        return;
    }
}
?>