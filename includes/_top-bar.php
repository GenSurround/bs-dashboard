    <div class="container-fluid dashboard-toolbar white swipe-area">
        <div class="container">
            <div class="row">
                <ul class="toolbar">
                    <li class="toolbar-item">
                        <a>Renter Profile</a>
                            <ul class="menu">
                                <li class="menu-item">
                                    <a><i class="bs-icon-bs_trips"></i>Trips</a>
                                    <ul class="submenu">
                                        <li class="submenu-item"><a href="renter-dashboard.php?p=trips-upcoming">Active/Upcoming</a></li>
                                        <li class="submenu-item"><a href="renter-dashboard.php?p=trips-pending">Pending</a></li>
                                        <li class="submenu-item"><a href="renter-dashboard.php?p=trips-completed">Completed/Cancelled</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item">
                                    <a><i class="bs-icon-renter_dashboard_heart-2"></i>Favorites</a>
                                    <ul class="submenu">
                                        <li class="submenu-item"><a href="renter-dashboard.php?p=favorites-boats">Boats</a></li>
                                        <li class="submenu-item"><a href="renter-dashboard.php?p=favorites-captains">Captains</a></li>
                                    </ul>
                                </li>
                            </ul> 
                    </li>
                    <li class="toolbar-item">
                        <a>Owner’s Dashboard</a>
                        <ul class="menu">
                                <li class="menu-item">
                                    <a><i class="bs-icon-my_fleet"></i>My Fleet</a>
                                    <ul class="submenu">
                                        <li class="submenu-item nocaret">
                                            <a href="owner-fleet-dashboard.php?p=my_fleet">
                                            All my Boats
                                            </a>
                                        </li>
                                        <li class="submenu-item bscaret">
                                            <a href="#">My Boat</a>
                                            <ul class="submenu">
                                                <li class="submenu-item"><a href="owner-dashboard.php?p=availability">Availability</a></li>
                                                <li class="submenu-item"><a href="owner-dashboard.php?p=pricing">Pricing</a></li>
                                                <li class="submenu-item"><a href="owner-dashboard.php?p=captains">Captains</a></li>
                                                <li class="submenu-item"><a href="owner-dashboard.php?p=boat-managers">Managers</a></li>
                                                <li class="submenu-item"><a href="owner-dashboard.php?p=trips">Trips</a></li>
                                                <li class="submenu-item"><a href="owner-dashboard.php?p=pics-videos">Pics</a></li>
                                                <li class="submenu-item"><a href="owner-dashboard.php?p=boat-details">Details</a></li>
                                                <li class="submenu-item"><a href="owner-dashboard.php?p=insurance">Insurance</a></li>
                                                <li class="submenu-item"><a href="owner-dashboard.php?p=promote">Promote</a></li>
                                            </ul>
                                        </li>
                                        <li class="submenu-item bscaret">
                                            <a href="#">My Boat2</a>
                                            <ul class="submenu">
                                                <li class="submenu-item"><a href="#">Availability</a></li>
                                                <li class="submenu-item"><a href="#">Pricing</a></li>
                                                <li class="submenu-item"><a href="#">Captains</a></li>
                                                <li class="submenu-item"><a href="#">Managers</a></li>
                                                <li class="submenu-item"><a href="#">Trips</a></li>
                                                <li class="submenu-item"><a href="#">Pics</a></li>
                                                <li class="submenu-item"><a href="#">Details</a></li>
                                                <li class="submenu-item"><a href="#">Insurance</a></li>
                                                <li class="submenu-item"><a href="#">Promote</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item nocaret">
                                    <a><i class="bs-icon-bs_earning"></i>My Earnings</a>
                                </li>
                                <li class="menu-item nocaret">
                                    <a><i class="bs-icon-captain_dashboard_direct-pay"></i>Payment settings (ACH)</a>
                                </li>
                                <li class="menu-item nocaret">
                                    <a><i class="bs-icon-bs_mechanical-repair"></i>Owner tools (including BST App)</a>
                                </li>
                                <li class="menu-item nocaret">
                                    <a><i class="bs-icon-bs_boat-profile"></i>List a boat</a>
                                </li>
                        </ul>
                    </li>
                    <li class="toolbar-item">
                        <a>Captain’s Dashboard</a>
                        <ul class="menu">
                                <li class="menu-item">
                                    <a><i class="bs-icon-bs_trips"></i>Trips</a>
                                    <ul class="submenu">
                                        <li class="submenu-item"><a href="captain-dashboard.php?p=trips-active_upcoming">Active/Upcoming</a></li>
                                        <li class="submenu-item"><a href="captain-dashboard.php?p=trips-pending">Pending</a></li>
                                        <li class="submenu-item"><a href="captain-dashboard.php?p=trips-completed_cancelled">Completed/Cancelled</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item nocaret">
                                    <a href="captain-dashboard.php?p=boats_i_run"><i class="bs-icon-bs_boat-details"></i>Boats I run</a>
                                </li>
                                <li class="menu-item nocaret">
                                    <a href="captain-dashboard.php?p=find_boats_to_run"><i class="bs-icon-bs_search"></i>Find boats to run</a>
                                </li>
                                <li class="menu-item nocaret">
                                    <a href="captain-dashboard.php?p=my_price"><i class="bs-icon-captain_dashboard_price"></i>My Price</a>
                                </li>
                                <li class="menu-item nocaret">
                                    <a href="captain-dashboard.php?p=my_earnings"><i class="bs-icon-bs_earning"></i>My earnings</a>
                                </li>
                                <li class="menu-item nocaret">
                                    <a href="captain-dashboard.php?p=payment_settings"><i class="bs-icon-captain_dashboard_direct-pay"></i>Payment settings (ACH)</a>
                                </li>
                                <li class="menu-item nocaret">
                                    <a href="captain-dashboard.php?p=promote"><i class="bs-icon-captain_dashboard_promote"></i>Promote</a>
                                </li>
                                <li class="menu-item nocaret">
                                    <a href="captain-dashboard.php?p=get_the_bst_app"><i class="bs-icon-captain_dashboard_cel"></i>Get the BST App - form</a>
                                </li>
                        </ul>
                    </li>
                    <li class="toolbar-item nocaret">
                        <a href="account-info.php?p=personal">Personal Info</a>
                    </li>
                    <li class="toolbar-item nocaret">
                        <a href="bs-bucks.php">B$ Bucks</a>
                    </li>
                </ul>
                <div class='pull-right wrapper-messages'>
                    <a class="sidebar-toggle"><i class="bs-icon-bs_engines"></i></a>
                            &nbsp;
                    <a href="messenger.php">
                    <i class="bs-icon-bs_messages"></i>
                    <span class="">Messages</span></a>
                    <div class="red-badge">2</div>
                </div>
            </div>
        </div>
    </div>