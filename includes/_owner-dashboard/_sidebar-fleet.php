<div id="dashboard-sidebar" class="dashboard-sidebar col-xs-12 col-sm-3">
    <div class="toggle"></div>
    <div class="user-info">
        <div class="progress">
            <div class="progress-text">                                     
            </div>
            <div id="progress-circle" class="progress-circle">
                <img class="img-circle" src="images/placeholders/cap1.jpg">
            </div>
        </div>

        <div class="user-name">Al Morin</div>
        <div class="small"><a href="">Edit Profile</a></div>

    </div>
    <br>
    <div class="dashboard-navigator">
        <ul>
            <li class="<?php is_active( $page ,'my_fleet'); ?>" >
                <a href="owner-fleet-dashboard.php?p=my_fleet"><i class='bs-icon-boats_boat-14'></i> My Fleet</a> 
            </li>
            <li class="<?php is_active( $page ,'my_earnings'); ?>" >
                <a href="owner-fleet-dashboard.php?p=my_earnings"><i class="bs-icon-bs_earning"></i> My Earnings</a> 
            </li>
            <li class="<?php is_active( $page ,'payments_settings'); ?> two-lines" >
                <a href="owner-fleet-dashboard.php?p=payments_settings"><i class="bs-icon-captain_dashboard_direct-pay"></i>Payment settings (ACH)</a> 
            </li>
            <li class="<?php is_active( $page ,'owner_tools'); ?>" >
                <a href="owner-fleet-dashboard.php?p=owner_tools"><i class="bs-icon-bs_mechanical-repair"></i> Owner tools</a> 
            </li>
            <li class="<?php is_active( $page ,'list_a_boat'); ?>" >
                <a href="owner-fleet-dashboard.php?p=list_a_boat"><i class="bs-icon-bs_boat-profile"></i> List a boat</a> 
            </li>
        </ul>
    </div>
</div>

<?php 
function is_active($page, $item) {
    if ($page == $item) {
        echo " active ";
    } else {
        return;
    }
}
?>