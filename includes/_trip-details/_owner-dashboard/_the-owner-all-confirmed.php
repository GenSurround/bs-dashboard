    <div class="trip-details-section trip-details-active-section">
<div class="trip-details-section-header row">
     <div class="col-xs-12">
        <h1><i class="bs-icon-bs_next gray"></i>Next Steps</h1>
    </div>

     <div class="col-xs-12">
        <ul class="info-ul">
             <li> Click <a href="#">here</a> to download Boatsetter Today</li>
            <li>Download and print the <a href="#">Check-In/Check-Out form.</a></li>
           <li>Have the boat ready at the designated location 30 minutes prior to check-out time.</li>
           <li>Save the renter’s contact information.</li>
           <li><a href="#">Use this form</a> to authorize someone else to handle check-in/check-out process for you.</li>
           <li>Make sure you fill up the tank.</li>
        </ul>
    </div>
</div>
</div>

    <div class="trip-details-section">
       <div class="trip-details-section-header row">
                <div class="col-xs-12 col-sm-6">
                    <h1><i class="bs-icon-bs_boat-details gray"></i>The Boat (You)</h1>
                </div>
                <div class="col-xs-12 col-sm-6 align-right">
                     <span class="status-message confirmed"><i class="bs-icon-bs_completed-47 green-confirmed"></i>Confirmed</span>
                </div>
        </div>
         <div class="trip-details-section-info row">
            <div class="profile-picture col-xs-12 col-sm-6">
                <figure>
                    <img src="images/placeholders/dude1.jpg">
                </figure>
                  <div class="profile-items">
                    <h3>Friend Zone Demolisher</h3>
                    <p>Miami, FL</p>
                   <div class="user-rating">
                         <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <span class="reviews">(7 reviews)</span>
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-sm-6 align-right">
               <ul class="actions-ul align-right">
                   <li>Add to Calendar <i class="bs-icon-bs_calendar blue"></i></li>
                   <li>Trip Cancellation<i class="bs-icon-bs_cancelled-trips blue"></i></li>
            
               </ul>
            </div>
        </div>
    </div>