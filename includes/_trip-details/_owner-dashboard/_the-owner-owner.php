    <div class="trip-details-section trip-details-pending-section">
       <div class="trip-details-section-header row">
                <div class="col-xs-12 col-sm-6">
                    <h1><i class="bs-icon-bs_boat-details gray"></i>The Boat (You)</h1>
                </div>
                <div class="col-xs-12 col-sm-6 align-right">
                    <span class="status-message pending"><i class="bs-icon-bs_pending orange"></i>Pending</span>
                </div>
        </div>
         <div class="trip-details-section-info row">
            <div class="profile-picture col-xs-12 col-sm-6">
                <figure>
                    <img src="images/placeholders/dude1.jpg">
                </figure>
                  <div class="profile-items">
                    <h3>Friend Zone Demolisher</h3>
                    <p>Miami, FL</p>
                    <div class="user-rating">
                         <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <span class="reviews">(7 reviews)</span>
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-sm-6 align-right">
                <span class="decline action-needed">Decline</span>
                <span class="accept action-needed">Accept</span>
            </div>
        </div>
    </div>