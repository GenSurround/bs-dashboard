    <div class="container-fluid dashboard-toolbar bg-blue white">
        <div class="container">
            <div class="row">
                <ul>
                    <li class="<?php is_active( $page ,'renter-profile'); ?>"><a href="trip-details.php?p=renter-profile"><span>Renter </span>Profile</a></li>
                     <li class="<?php is_active( $page ,'owner-dashboard'); ?>"><a href="trip-details.php?p=owner-dashboard"><span>Owner's </span>Dashboard</a></li>
                     <li class="<?php is_active( $page ,'captain-dashboard'); ?>"><a href="trip-details.php?p=captain-dashboard"><span>Captain's </span>Dashboard</a></li>
                    <li><a href=""> <span>Account </span>Info</a></li>
                     <li><a href=""> <span>B$ </span>Bucks</a></li>
                    <li class='pull-right'>
                        <a class="sidebar-toggle"><i class="bs-icon-bs_engines"></i></a>
                        &nbsp;
                        <a href="">
                            <i class="bs-icon-bs_messages"></i>
                            <span class="hidden-xs">Messages</span></a>
                            <div class="red-badge">2</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <?php 
function is_active($page, $item) {
    if ($page == $item) {
        echo " active ";
    } else {
        return;
    }
}
?>
