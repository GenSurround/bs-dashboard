<div class="sidebar-itinerary">
	<h3><i class="bs-icon-bs_week"></i>Itinerary <a href="">(change)</a></h3>
	<div class="itinerary-details divTable">
        <div class="divRow">
              <div class="divCell">Trip ID #</div>
            <div class="divCell">1825</div>
        </div>
        <div class="divRow">
            <div class="divCell">Start Date/Time</div>
            <div class="divCell">Nov 25, 2016 @ 8:00 am</div>
       </div>
        <div class="divRow">
            <div class="divCell">End Date/Time</div>
            <div class="divCell">Nov 25, 2016 @ 12:00 pm</div>
       </div>
        <div class="divRow">
            <div class="divCell">Passengers</div>
            <div class="divCell">8</div>
       </div>
  	</div>
  	<footer class="row">
  		<div class="col-xs-6">
  		<img src="images/dashboard/trip-details/bs-free-cancellation.svg" alt="">
  		</div>
  		<div class="col-xs-6">
  			<p>You are free to cancel up to 24 hours pre-trip!</p>
  		</div>
  	</footer>
</div>
