<div class="sidebar-fees">
	<h3><i class="bs-icon-bs_dollar-sign"></i>Rental Fees</a></h3>
	<div class="fees-details divTable">
        <div class="divRow">
            <div class="divCell divCellTh">Boat Rental</div>
            <div class="divCell divCellTh">$780.00</div>
        </div>
        <div class="divRow">
            <div class="divCell">Booking Fee</div>
            <div class="divCell">$58.50</div>
       </div>
        <div class="divRow">
            <div class="table-divider"></div>
              <div class="table-divider"></div>
       
       </div>
   
        <div class="divRow">
            <div class="divCell ">Subtotal</div>
            <div class="divCell ">$838.50</div>
        </div>
        <div class="divRow">
            <div class="divCell">Sales Tax (7%)</div>
            <div class="divCell">$99.00</div>
       </div>
         <div class="divRow">
            <div class="divCell">Insurance & Towing</div>
            <div class="divCell">$58.50</div>
       </div>
       <div class="divRow">
            <div class="table-divider"></div>
             <div class="table-divider"></div>
     
       </div>
          <div class="divRow">
            <div class="divCell divCellTf">Rental Total</div>
            <div class="divCell divCellTf">$996.20</div>
       </div>
  </div>
  <h4>Refundable Deposit<i class="bs-icon-bs_interrogation"></i><span>$1.000,00</span></h4>
 <span class="print"><i class="bs-icon-bs_print blue"></i>Print Charter Contract</span>
</div>
