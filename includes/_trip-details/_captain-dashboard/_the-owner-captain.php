    <div class="trip-details-section">
       <div class="trip-details-section-header row">
                <div class="col-xs-12 col-sm-6">
                    <h1><i class="bs-icon-bs_boat-details gray"></i>The Boat</h1>
                </div>
                <div class="col-xs-12 col-sm-6 align-right">
                    <span class="status-message pending"><i class="bs-icon-bs_pending orange"></i>Pending</span>
                </div>
        </div>
         <div class="trip-details-section-info row">
            <div class="profile-picture col-xs-12 col-sm-6">
                <figure>
                    <img src="images/placeholders/dude1.jpg">
                </figure>
                  <div class="profile-items">
                    <h3>Friend Zone Demolisher</h3>
                    <p>Miami, FL</p>
                    <div class="user-rating">
                         <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <span class="reviews">(7 reviews)</span>
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-sm-6">
                 <ul class="actions-ul align-right">
                   <li>View Boat Profile <i class="bs-icon-bs_boat-profile"></i></li>
               </ul>
            </div>
        </div>
         <div class="trip-details-section-info row">
            <div class="profile-picture col-xs-12 col-sm-6">

                <figure>
                    <img src="images/placeholders/dude1.jpg">
                </figure>
                <div class="profile-items">
                    <h3>José Antonio (Owner)</h3>
                    <p>Miami, FL</p>
                    <ul class="actions-ul left-icons">
                        <li><i class="bs-icon-bs_messages"></i><span>Message Owner</span></li>
                    </ul>
                </div>
            </div>
             <div class="col-xs-12 col-sm-6">
                  <ul class="actions-ul align-right">
                   <li>Call: (000) 000 0000 <i class="bs-icon-bs_phone blue"></i></li>
                   <li>Email: frienzone@email.com<i class="bs-icon-bs_mail blue"></i></li>
                
               </ul>
            </div>
        </div>
    </div>