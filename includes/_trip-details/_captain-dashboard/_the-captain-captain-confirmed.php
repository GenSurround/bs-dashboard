
    <div class="trip-details-section">
       <div class="trip-details-section-header row">
                <div class="col-xs-12 col-sm-6">
                    <h1><i class="bs-icon-bs_captain-required gray"></i>The Captain (You)</h1>
                </div>
                <div class="col-xs-12 col-sm-6 align-right">
                    <span class="status-message confirmed"><i class="bs-icon-bs_completed-47 green-confirmed"></i>Confirmed</span>
                </div>
        </div>
         <div class="trip-details-section-info row">
            <div class="profile-picture col-xs-12 col-sm-6">
                <figure>
                    <img src="images/placeholders/dude1.jpg">
                </figure>
                  <div class="profile-items">
                    <h3>Alexander</h3>
                    <p>Miami, FL</p>
                    <div class="user-rating">
                         <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <span class="reviews">(7 reviews)</span>
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-sm-6 align-right">
                <ul class="actions-ul align-right">
                   <li>Add to Calendar <i class="bs-icon-bs_calendar blue"></i></li>
                   <li>Trip Cancellation<i class="bs-icon-bs_cancelled-trips blue"></i></li>
            
               </ul>
            </div>
        </div>
    </div>