<div class="trip-details-section">
    <div class="trip-details-section-header row">
        <div class="col-xs-12 col-sm-6">
            <h1><i class="bs-icon-bs_renter gray"></i>The Renter</h1>
        </div>
        <div class="col-xs-12 col-sm-6 align-right">
            <span class="status-message confirmed"><i class="bs-icon-bs_completed-47 green-confirmed"></i>Confirmed</span>
        </div>
    </div>

    <div class="trip-details-section-info row">
        <div class="profile-picture col-xs-12 col-sm-6">
            <figure>
                <img src="images/placeholders/dude1.jpg">
            </figure>
              <div class="profile-items">
                <h3>Vanessa</h3>
                <p>Miami, FL</p>
             </div>
        </div>
        <div class="col-xs-12 col-sm-6 align-right">
            
        </div>
           <div class="row-divider col-xs-12"></div>

        <div class="col-xs-12 col-sm-6 align-left">
              <ul class="actions-ul left-icons">
                   <li><i class="bs-icon-bs_messages blue"></i><span>Message Renter</span></li>
                   <li><i class="bs-icon-bs_captain-required blue"></i><span>View Renter Profile</span></li>
               </ul>
            </div>
            <div class="col-xs-12 col-sm-6 align-right">
                <ul class="actions-ul">
                   <li>Call: (000) 000 0000 <i class="bs-icon-bs_phone blue"></i></li>
                   <li>Email: frienzone@email.com<i class="bs-icon-bs_mail blue"></i></li>
               </ul>
            </div>

    </div>


</div>