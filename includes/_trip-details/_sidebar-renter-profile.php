<div class="trip-details-sidebar col-sm-12 col-md-3">
	<div class="sidebar-content">
		<header>
			<img src="images/bs-icons/bs-logo.svg" alt="Boatsetter Logo">
			<h2>Your Boatsetter Experience</h2>
		</header>
		<?php include 'includes/_trip-details/_sidebar-trip-details/_location-trip-details.php'; ?>
		<?php include 'includes/_trip-details/_sidebar-trip-details/_itinerary-trip-details.php'; ?>
		<?php include 'includes/_trip-details/_sidebar-trip-details/_fees-trip-details.php'; ?>
	</div>
</div>
