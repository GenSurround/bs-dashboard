<div class="row">
	<div class="trip-details-progress-bar col-xs-12">
		<ul class="status-bar">
			<li><i class="bs-icon-bs_renter confirmed"></i></li>
			<li><i class="bs-icon-bs_boat-details not-confirmed"></i></li>
			<li><i class="bs-icon-bs_captain-required not-confirmed"></i></li>
		</ul>
		<ul class="status-messages">
			<li class="status-waiting"><i class="bs-icon-bs_warning red"></i>The Captain has declined the trip. Please select another captain for this trip:</li>
		</ul>
	</div>
</div>
