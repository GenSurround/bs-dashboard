<div class="row">
	<div class="trip-details-progress-bar col-xs-12">
		<ul class="status-bar">
			<li><i class="bs-icon-bs_renter confirmed"></i></li>
			<li><i class="bs-icon-bs_boat-details cancelled"></i></li>
			<li><i class="bs-icon-bs_captain-required not-confirmed"></i></li>
		</ul>
		<ul class="status-messages">
			<li><i class="bs-icon-bs_cancelled-trips red"></i>The Boat Owner has declined the trip. Here are some other options for you to choose from:</li>
		</ul>
	</div>
</div>
