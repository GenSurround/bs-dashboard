    <div class="owner-choices">
        <div class="owner-choices-items row">
            <div class="owner-choices-item col-xs-12 col-sm-4">
                <header>
                    <figure>
                        <img src="images/placeholders/boat.jpg" alt="">
                    </figure>
                    <h3>White Dolphin - 34'</h3>
                </header>
                <div class="owner-profile">
                    <span><i class="bs-icon-bs_location"></i>Miami, FL</span>
                    <span><i class="bs-icon-bs_half-day"></i>Half Day</span>
                    <span><i class="bs-icon-bs_captains"></i>With Captain</span>
                </div>
                   
                <div class="owner-pic">
                    <figure>
                       <img src="images/placeholders/cap1.jpg" alt="">
                    </figure>
                    <span class="price">$480</span>
                </div>
                <span class="view-boat-profile">View Boat Profile</span>
            </div>
             <div class="owner-choices-item col-xs-12 col-sm-4">
                 <header>
                    <figure>
                       <img src="images/placeholders/boat.jpg" alt="">
                    </figure>
                    <h3>My Great Boat - 34'</h3>
                </header>
                <div class="owner-profile">
                    <span><i class="bs-icon-bs_location"></i>Aventura, FL</span>
                    <span><i class="bs-icon-bs_full-day"></i>Full Day</span>
                    <span><i class="bs-icon-bs_captains"></i>With Captain</span>
                </div>
                 <div class="owner-pic">
                    <figure>
                        <img src="images/placeholders/cap1.jpg" alt="">
                    </figure>
                   <span class="price">$1,580</span>
                </div>
                 <span class="view-boat-profile">View Boat Profile</span>
            </div>
             <div class="owner-choices-item col-xs-12 col-sm-4">
                 <header>
                    <figure>
                       <img src="images/placeholders/boat.jpg" alt="">
                    </figure>
                    <h3>Envidia de todos - 34'</h3>
                </header>
                <div class="owner-profile">
                    <span><i class="bs-icon-bs_location"></i>Myrna Beach, FL</span>
                    <span><i class="bs-icon-bs_half-day"></i>Half Day</span>
                    <span><i class="bs-icon-bs_captains"></i>With Captain</span>
                </div>
                 <div class="owner-pic">
                    <figure>
                       <img src="images/placeholders/cap1.jpg" alt="">
                    </figure>
                   <span class="price">$2,000</span>
                </div>
                <span class="view-boat-profile">View Boat Profile</span>
            </div>
        </div>
    </div>