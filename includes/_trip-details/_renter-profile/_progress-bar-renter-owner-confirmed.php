<div class="row">
	<div class="trip-details-progress-bar col-xs-12">
		<ul class="status-bar">
			<li><i class="bs-icon-bs_renter confirmed"></i></li>
			<li><i class="bs-icon-bs_boat-details confirmed"></i></li>
			<li><i class="bs-icon-bs_captain-required not-confirmed"></i></li>
		</ul>
		<ul class="status-messages">
			<li class="status-confirmed"><i class="bs-icon-bs_boat-details green-confirmed"></i>Boat Confirmed</li>
			<li class="status-waiting"><i class="bs-icon-bs_time-86"></i>Waiting for Captain Confirmation</li>
		</ul>
	</div>
</div>
