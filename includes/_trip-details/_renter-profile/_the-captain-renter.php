    <div class="trip-details-section">
       <div class="trip-details-section-header row">
            <div class="col-xs-12 col-sm-6">
                <h1><i class="bs-icon-bs_captain-required gray"></i>The Captain</h1>
            </div>
            <div class="col-xs-12 col-sm-6 align-right">
                <span class="status-message pending"><i class="bs-icon-bs_pending orange"></i>Pending</span>
            </div>
        </div>
        <div class="trip-details-section-info row">
            <div class="profile-picture col-xs-12 col-sm-6">
                <figure>
                    <img src="images/placeholders/dude1.jpg">
                </figure>
                <div class="profile-items">
                     <h3>Alexander</h3>
                     <p>Miami, FL</p>
                      <div class="user-rating">
                         <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <i class="fa fa-star yellow"></i>
                        <span class="reviews">(7 reviews)</span>
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-sm-6 align-right">
               <ul class="profile-ul">
                    <li><i class="bs-icon-bs_master gray"></i>Status<span>Admiral</span></li>
                    <li><i class="bs-icon-bs_experience gray"></i>Experience<span>11 trips</span></li>
                    <li><i class="bs-icon-bs_response gray"></i>Avg Resp<span>23 min</span></li>
                    <li><i class="bs-icon-bs_boat gray"></i>Assigned<span>10 Boats</span></li>
               </ul>
            </div>
            <div class="row-divider col-xs-12"></div>
            <div class="col-xs-12 col-sm-6 align-left">
               <ul class="actions-ul left-icons">
                   <li><i class="bs-icon-bs_messages blue"></i><span>Message Captain </span></li>
                   <li><i class="bs-icon-bs_captain-required blue"></i><span class="view-captain-profile">View Captain Profile</span></li>
               </ul>
            </div>
            <div class="col-xs-12 col-sm-6 align-right">
                <ul class="actions-ul">
                   <li>Call: (000) 000 0000 <i class="bs-icon-bs_phone blue"></i></li>
                   <li>Email: frienzone@email.com<i class="bs-icon-bs_mail blue"></i></li>
               </ul>
            </div>
        </div>
    </div>