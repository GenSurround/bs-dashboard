<div class="row">
	<div class="trip-details-progress-bar col-xs-12">
		<ul class="status-bar">
			<li><i class="bs-icon-bs_renter confirmed"></i></li>
			<li><i class="bs-icon-bs_boat-details confirmed"></i></li>
			<li><i class="bs-icon-bs_captain-required confirmed"></i></li>
		</ul>
		<ul class="status-messages">
			<li class="status-confirmed"><i class="bs-icon-bs_success green-confirmed"></i>Success! All parties have confirmed the Trip Reservation.</li>
		
		</ul>
	</div>
</div>
