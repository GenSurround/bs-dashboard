        <div class="container-fluid alert-bar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 margin-bottom-full">
                        <p>You are one step closer to activating your boat!</p>
                        <p class="small ">Please complete the items listed <span class="hidden-xs">to the right.</span> <span class="visible-xs">below.</span></p>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-half">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="badge-icon">
                                    <i class="bs-icon-bs_pricing"></i>
                                    <div class="badge">
                                        <i class="bs-icon-bs_warning"></i>
                                    </div> 
                                    <p>Pricing</p>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="badge-icon">
                                    <i class="bs-icon-bs_captains"></i>
                                    <div class="badge">
                                        <i class="bs-icon-bs_warning"></i>
                                    </div> 
                                    <p>Captains</p>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="badge-icon">
                                    <i class="bs-icon-bs_boat-managers"></i>
                                    <div class="badge">
                                        <i class="bs-icon-bs_warning"></i>
                                    </div> 
                                    <p>Boat Managers</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>