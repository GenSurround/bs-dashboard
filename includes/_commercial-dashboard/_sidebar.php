<div id="dashboard-sidebar" class="dashboard-sidebar col-xs-12 col-sm-3">
    <div class="toggle"></div>
    <div class="boat-info">
        <div class="progress">
            <div class="progress-text">                                     
                Profile <span class="percent">33%</span> complete
            </div>
            <div id="progress-circle" class="progress-circle">
                <img class="img-circle" src="images/dashboard/boat-pic.png">
            </div>
        </div>

        <div class="boat-name">Friendzone Demolisher</div>
        <div class="boat-location"><i class="nc-icon-outline location_pin"></i> Aventura, FL</div>

        <div class="boat-status">
            <span>Status</span>
            <div class="wrapper-select">
                <select class="boat-status-select selectpicker form-control">
                    <option class="active" data-state="active" data-icon="circle-active">Active</option>
                    <option class="snooze" data-state="snooze" data-icon="circle-snooze">Snooze</option>
                    <option class="inactive" data-state="inactive" data-icon="circle-inactive">Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <div class="dashboard-navigator">
        <ul>
            <li class="<?php is_active( $page ,'availability'); ?>" >
                <a href="commercial-dashboard.php?p=availability"><i class='bs-icon-bs_availability'></i> Availability <span class="red-badge">3</span></a> 
            </li>
            <li class="<?php is_active( $page ,'pricing'); ?>" >
                <a href="commercial-dashboard.php?p=pricing"><i class='bs-icon-bs_pricing'></i> Pricing</a>
            </li>
            <li class="<?php is_active( $page ,'captains'); ?>" >
                <a href="commercial-dashboard.php?p=captains"><i class='bs-icon-bs_captains'></i>Captains <i class='bs-icon-bs_warning red'></i></a>
            </li>
            <li class="<?php is_active( $page ,'boat-managers'); ?>">
                <a href="commercial-dashboard.php?p=boat-managers"><i class='bs-icon-bs_boat-managers'></i> Boat Managers  <i class='bs-icon-bs_warning red'></i></a>
            </li>
            <li class="collapsible <?php is_active( $page ,'trips'); ?>" >
                <a href="commercial-dashboard.php?p=trips"><i class='bs-icon-bs_trips'></i> Trips</a>
                <div class="toggle"></div>
                <ul class="collapse in">
                    <li class="<?php is_active( $tab ,'requests'); ?>">
                        <a href="commercial-dashboard.php?p=trips&tab=requests">Trip Requests</a>
                    </li>
                    <li class="<?php is_active( $tab ,'upcoming'); ?>">
                        <a href="commercial-dashboard.php?p=trips&tab=upcoming">Upcoming Trips</a>
                    </li>
                    <li class="<?php is_active( $tab ,'completed'); ?>">
                        <a href="commercial-dashboard.php?p=trips&tab=completed">Completed Trips</a>
                    </li>
                    <li class="<?php is_active( $tab ,'cancelled'); ?>">
                        <a href="commercial-dashboard.php?p=trips&tab=cancelled">Cancelled Trips</a>
                    </li>
                </ul>
            </li>
            <li class="<?php is_active( $page ,'pics-videos');  ?>">
                <a href="commercial-dashboard.php?p=pics-videos"><i class='bs-icon-bs_pics-videos'></i>Pics & Videos</a>
            </li>
            <li class="<?php is_active( $page ,'boat-details');  ?>">
                <a href="commercial-dashboard.php?p=boat-details"><i class='bs-icon-bs_boat-details'></i>Boat Details</a>
            </li>
            <li class="<?php is_active( $page ,'insurance'); ?>" >
                <a href="commercial-dashboard.php?p=insurance"><i class='bs-icon-bs_insurance'></i>Insurance</a>
            </li>
            <li class="<?php is_active( $page ,'promote'); ?>promote"><a href="commercial-dashboard.php?p=promote"><i class='bs-icon-bs_promote'></i> Promote your Boat!</a>
        </li>
        </ul>
    </div>
</div>

<?php 
function is_active($page, $item) {
    if ($page == $item) {
        echo " active ";
    } else {
        return;
    }
}
?>