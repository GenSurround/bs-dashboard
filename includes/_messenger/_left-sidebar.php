<div class="messenger-left-sidebar">
	<div class="all-messages">
		<img src="images/dashboard/messenger/bs-msn.png" alt="">
		<h2><i class="bs-icon-bs_inbox"></i>All Messages<i class="bs-icon-bs_up-arrow"></i></h2>
		<div id="demoContainer">
			<div class="all-messages-dropdown">
				<ul>
					<li class="unread"><i class="bs-icon-bs_unread"></i>Unread</li>
					<li class="read"><i class="bs-icon-bs_read"></i>Read</li>
					<li class="search"><i class="bs-icon-bs_search"></i><input type="text" placeholder="Search"><span class="clear-search"><i class="bs-icon-bs_cancelled"></i></span></li>
				</ul>
			</div>
		</div>

	</div>
	<div class="list-messages nano">
		<ul class="nano-content">
			<li class="list-message has-messages has-sidebar">
				<div class="message-inner clearfix">
				<span class="message-comment-number">22</span>
				<span class="delete-message"><i class="bs-icon-bs_cancelled"></i></span>
					<figure>
						 <img src="images/placeholders/dude1.jpg">
					</figure>
					<div class="list-message-info">
						<h2 class="message-title">Vanessa</h2>
						<span>Nov 15, 2016 - 03:15pm</span>
						<p>Good News!The Boatsetter Crew lorem ipsum dolor sit amet</p>
					</div>
				</div>
			</li>
			<li class="list-message">
				<div class="message-inner clearfix">
				<span class="message-comment-number">22</span>
				<span class="delete-message"><i class="bs-icon-bs_cancelled"></i></span>
					<figure>
						 <img src="images/placeholders/dude1.jpg">
					</figure>
					<div class="list-message-info">
						<h2 class="message-title">John</h2>
						<span>Nov 15, 2016 - 03:15pm</span>
						<p>Good News!The Boatsetter Crew lorem ipsum dolor sit amet</p>
					</div>
				</div>
			</li>
			<li class="list-message has-messages no-sidebar">
				<div class="message-inner clearfix">
				<span class="message-comment-number">12</span>
					<span class="delete-message"><i class="bs-icon-bs_cancelled"></i></span>
					<figure>
						 <img src="images/placeholders/dude1.jpg">
					</figure>
					<div class="list-message-info">
						<h2 class="message-title">Vanessa</h2>
						<span>Nov 15, 2016 - 03:15pm</span>
						<p>Good News!The Boatsetter Crew lorem ipsum dolor sit amet</p>
					</div>
				</div>
			</li>
			<li class="list-message">
				<div class="message-inner clearfix">
				<span class="message-comment-number">22</span>
				<span class="delete-message"><i class="bs-icon-bs_cancelled"></i></span>
					<figure>
						 <img src="images/placeholders/dude1.jpg">
					</figure>
					<div class="list-message-info">
						<h2 class="message-title">Edward</h2>
						<span>Nov 15, 2016 - 03:15pm</span>
						<p>Good News!The Boatsetter Crew lorem ipsum dolor sit amet</p>
					</div>
				</div>
			</li>
			<li class="list-message">
				<div class="message-inner clearfix">
				<span class="message-comment-number">22</span>
				<span class="delete-message"><i class="bs-icon-bs_cancelled"></i></span>
					<figure>
						 <img src="images/placeholders/dude1.jpg">
					</figure>
					<div class="list-message-info">
						<h2 class="message-title">Brad</h2>
						<span>Nov 15, 2016 - 03:15pm</span>
						<p>Good News!The Boatsetter Crew lorem ipsum dolor sit amet</p>
					</div>
				</div>
			</li>
			<li class="list-message">
				<div class="message-inner clearfix">
				<span class="message-comment-number">22</span>
				<span class="delete-message"><i class="bs-icon-bs_cancelled"></i></span>
					<figure>
						 <img src="images/placeholders/dude1.jpg">
					</figure>
					<div class="list-message-info">
						<h2 class="message-title">Angelina</h2>
						<span>Nov 15, 2016 - 03:15pm</span>
						<p>Good News!The Boatsetter Crew lorem ipsum dolor sit amet</p>
					</div>
				</div>
			</li>
			<li class="list-message">
				<div class="message-inner clearfix">
				<span class="message-comment-number">22</span>
				<span class="delete-message"><i class="bs-icon-bs_cancelled"></i></span>
					<figure>
						 <img src="images/placeholders/dude1.jpg">
					</figure>
					<div class="list-message-info">
						<h2 class="message-title">Mark</h2>
						<span>Nov 15, 2016 - 03:15pm</span>
						<p>Good News!The Boatsetter Crew lorem ipsum dolor sit amet</p>
					</div>
				</div>
			</li>
			<li class="list-message">
				<div class="message-inner clearfix">
				<span class="message-comment-number">22</span>
				<span class="delete-message"><i class="bs-icon-bs_cancelled"></i></span>
					<figure>
						 <img src="images/placeholders/dude1.jpg">
					</figure>
					<div class="list-message-info">
						<h2 class="message-title">Harrison</h2>
						<span>Nov 15, 2016 - 03:15pm</span>
						<p>Good News!The Boatsetter Crew lorem ipsum dolor sit amet</p>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>