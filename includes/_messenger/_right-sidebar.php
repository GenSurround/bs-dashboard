<div class="messenger-right-sidebar">
	<div class="back-to-chat">
		<span><i class="bs-icon-bs_up-arrow blue"></i>Back to chat</span>
	</div>
	<div class="right-sidebar-header">
		<h2><a href="#">Trip #1296</a> - Nov 30th, 2016</h2>
	</div>
	<div class="right-sidebar-trip-items nano">
		<ul class="nano-content">
			<li class="clearfix">
				<i class="bs-icon-bs_boat-front"></i>
				<div class="item-info" class="clearfix">
					<h3>The Boat</h3>
					<p>Friendzone Demolisher</p>
				</div>
				<figure>
					 <img src="images/placeholders/dude1.jpg">
				</figure>
			</li>
			<li class="clearfix">
				<i class="bs-icon-bs_renter"></i>
				<div class="item-info" class="clearfix">
					<h3>The Renter</h3>
					<p>Vanessa</p>
				</div>
				<figure>
					 <img src="images/placeholders/dude1.jpg">
				</figure>
			</li>
			<li class="clearfix" class="clearfix">
				<i class="bs-icon-bs_captain-required"></i>
				<div class="item-info">
					<h3>The Captain</h3>
					<p>Alexander</p>
				</div>
				<figure>
					 <img src="images/placeholders/dude1.jpg">
				</figure>
			</li>
			<li class="clearfix">
				<i class="bs-icon-bs_week"></i>
				<div class="item-info" class="clearfix">
					<h3>Itinerary</h3>
					<p>Half Day</p>
					<p>8 passengers</p>
					<p>8:30am - 12:30am</p>
				</div>
			</li>
			<li class="clearfix">
				<i class="bs-icon-bs_earning"></i>
				<div class="item-info" class="clearfix">
					<h3>Boat Rental</h3>
				</div>
				<span class="price">$500.00</span>
				<div class="item-info-footer">	
				<a class="more-details" role="button">More Details</a>
				</div>
			</li>
		</ul>
	</div>
</div>