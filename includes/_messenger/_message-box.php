<div class="message-box">
	<div class="cta-sidebar">
		<h4><span>Vanessa is your </span><strong>Renter</strong> on <strong>Trip #1296</strong> - Nov 30th, 2016</h4>
	</div>
	<div class="message-area clearfix">
		<form action="">
			<textarea id="input-default" placeholder="Type your message here"></textarea>
			<div class="message-actions clearfix">
				<span class="attachment"><i class="bs-icon-bs_attachment"></i></span>
				<button>Send</button>
			</div>
		</form>
	</div>
</div>
