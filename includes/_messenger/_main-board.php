<div class="messenger-main-board">
	<div class="sender-profile clearfix">
		<span class="back"><i class="bs-icon-bs_up-arrow blue"></i></span>
		<figure>
			 <img src="images/placeholders/dude1.jpg">
		</figure>
		<h2>Vanessa</h2>
		<div class="message-actions">
			<span><img src="images/dashboard/messenger/menu-messenger.svg"></span> 
			<div class="message-actions-drop-down">
				<ul>
					<li class="unread"><i class="bs-icon-bs_unread"></i>Mark as unread</li>
					<li class="forward"><i class="bs-icon-bs_forward"></i>Forward</li>
					<li class="delete"><i class="bs-icon-bs_cancelled-trips"></i>Delete</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="conversation-board nano">
		<div class="message-dialog nano-content">
			<span class="message-day">Today</span>
		    <div class="sender-message clearfix">
				<div class="message-wrapper">
					<h3>Vanessa</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam at, dolorum voluptatem optio quisquam sunt nisi illo qui repellat, explicabo magnam, similique sit blanditiis officia corporis tempora, vel consequuntur cumque!</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam at, dolorum voluptatem optio quisquam sunt nisi illo qui repellat, explicabo magnam, similique sit blanditiis officia corporis tempora, vel consequuntur cumque!</p>
				</div>
				<span class="message-time">10:30 am</span>
			</div>
			<div class="user-message clearfix">
			    <span class="message-time">10:35 am</span>
				<div class="message-wrapper">
					<h3>You</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam at, dolorum voluptatem optio quisquam sunt nisi illo qui repellat, explicabo magnam, similique sit blanditiis officia corporis tempora, vel consequuntur cumque!</p>
				</div>
			</div>
			<div class="sender-message clearfix">
				<div class="message-wrapper">
					<h3>Vanessa</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam at, dolorum voluptatem optio quisquam sunt nisi illo qui repellat, explicabo magnam, similique sit blanditiis officia corporis tempora, vel consequuntur cumque!</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam at, dolorum voluptatem optio quisquam sunt nisi illo qui repellat, explicabo magnam, similique sit blanditiis officia corporis tempora, vel consequuntur cumque!</p>
				</div>
				<span class="message-time">10:30 am</span>
			</div>
			<div class="user-message clearfix">
			    <span class="message-time">10:35 am</span>
				<div class="message-wrapper">
					<h3>You</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam at, dolorum voluptatem optio quisquam sunt nisi illo qui repellat, explicabo magnam, similique sit blanditiis officia corporis tempora, vel consequuntur cumque!</p>
				</div>
			</div>
			<div class="sender-message clearfix">
				<div class="message-wrapper">
					<h3>Vanessa</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam at, dolorum voluptatem optio quisquam sunt nisi illo qui repellat, explicabo magnam, similique sit blanditiis officia corporis tempora, vel consequuntur cumque!</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam at, dolorum voluptatem optio quisquam sunt nisi illo qui repellat, explicabo magnam, similique sit blanditiis officia corporis tempora, vel consequuntur cumque!</p>
				</div>
				<span class="message-time">10:30 am</span>
			</div>
			<div class="user-message clearfix">
			    <span class="message-time">10:35 am</span>
				<div class="message-wrapper">
					<h3>You</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam at, dolorum voluptatem optio quisquam sunt nisi illo qui repellat, explicabo magnam, similique sit blanditiis officia corporis tempora, vel consequuntur cumque!</p>
				</div>
			</div>
		</div>
	</div>
</div>