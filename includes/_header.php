<!-- bs header -->
<header>
    <div class="container">
        <a href="http://www.boatsetter.com"><img class="logo" src="images/dashboard/logo-white.png"></a>
        <nav class="main pull-right white">
            <div class="contact-us-wrapper"><a class="triggerHelp">Contact Us </a> | <a href=""> English </a></div>
            <a class="triggerAccount white hidden-xs"><img width="auto" height="26" src="images/icon-account.svg"><span>Account</span></a>
            <a href="#" class="white  triggerSearch"><img width="auto" height="26" src="images/icon-search.svg"><span>Search</span></a>
            <a href="#" class="white hidden-xs"><img width="auto" height="26" src="images/icon-list-boat.svg"><span>List boat</span></a>
            <div class="icons-mobile">
                <div class="wrapper-hamburger  ">
                    <span class="icon-line line1"></span>
                    <span class="icon-line line2"></span>
                    <span class="icon-line line3"></span>
                </div>
            </div>
        </nav>
    </div>
</header>
<!-- end header -->