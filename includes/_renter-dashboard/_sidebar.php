<div id="dashboard-sidebar" class="dashboard-sidebar renter col-xs-12 col-sm-3">
    <div class="toggle"></div>
    <div class="boat-info">
        <div class="progress">
            <div id="progress-circle" class="progress-circle">
                <img class="img-circle" src="images/placeholders/dude1.jpg">
            </div>
        </div>

        <div class="boat-name">Alejandro</div>
        <div class="boat-location btn-edit"><a href="#">Edit Profile</a></div>

    </div>
    <div class="dashboard-navigator">
        <ul>
            
            <li class="collapsible <?php is_active( $page ,'availability'); ?>" >
                <a href="renter-dashboard.php?p=trips-upcoming"><i class='bs-icon-bs_trips'></i> Trips</a>
                <div class="toggle"></div>
                <ul class="collapse in">
                    <li class="<?php is_active( $page ,'trips-upcoming'); ?>">
                        <a href="renter-dashboard.php?p=trips-upcoming">Active/Upcoming</a>
                    </li>
                    <li class="<?php is_active( $page ,'trips-pending'); ?>">
                        <a href="renter-dashboard.php?p=trips-pending">Pending</a>
                    </li>
                    <li class="<?php is_active( $page ,'trips-completed'); ?>">
                        <a href="renter-dashboard.php?p=trips-completed">Completed/Cancelled</a>
                    </li>
                </ul>
            </li>
            <li class="collapsible <?php is_active( $page ,'favorites'); ?>" >
                <a href="renter-dashboard.php?p=favorites-boats"><i class='bs-icon-renter_dashboard_heart-2'></i> Favorites</a>
                <div class="toggle"></div>
                <ul class="collapse in">
                    <li class="<?php is_active( $page ,'favorites-boats'); ?>">
                        <a href="renter-dashboard.php?p=favorites-boats">Boats</a>
                    </li>
                    <li class="<?php is_active( $page ,'favorites-captains'); ?>">
                        <a href="renter-dashboard.php?p=favorites-captains">Captains</a>
                    </li>
                </ul>
            </li>
            <li class="collapsible <?php is_active( $page ,'wishlist-search'); ?>" >
                <a href="renter-dashboard.php?p=wishlist-search"><i class='bs-icon-bs_add-fav'></i> Trip Wishlist</a>
            </li>
        </ul>
    </div>
</div>

<?php 
function is_active($page, $item) {
    if ($page == $item) {
        echo " active ";
    } else {
        return;
    }
}
?>