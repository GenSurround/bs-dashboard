
<div style="overflow: hidden; opacity: 0; width: 0px; height:0px;"><img src="img/user1.jpg" /></div>
<div class="not-buttons">

	<button id="notification-trigger-1" class="progress-button pb-1">
		<span class="content">New Message</span>
	</button>

	<button id="notification-trigger-2" class="progress-button pb-2">
		<span class="content">New Trip Request</span>
	</button>

	<button id="notification-trigger-3" class="progress-button pb-3">
		<span class="content">New Boats</span>
	</button>

	<button id="notification-trigger-4" class="progress-button pb-4">
		<span class="content">New Captain Request</span>
	</button>

</div>

<script src="assets/notifications/js/classie.js"></script>
<script src="assets/notifications/js/notificationFx.js"></script>
	
	<script>
		(function() {
			var bttn1 = document.getElementById( 'notification-trigger-1' );
			bttn1.addEventListener( 'click', function() {
				setTimeout( function() {
					var notification = new NotificationFx({
						message : '<div class="ns-thumb notif1"><img src="images/placeholders/dude1.jpg"/></div> <a href="#"><div class="ns-content"><p><span class="ns-box-span">John Connor</span> just sent you a message</p></div></a>',
						ttl : 4000,
						effect : 'thumbslider'
					});
					notification.show();
				}, 1000 );
			} );

			var bttn2 = document.getElementById( 'notification-trigger-2' );
			bttn2.addEventListener( 'click', function() {
				setTimeout( function() {
					var notification = new NotificationFx({
						message : '<div class="ns-thumb notif2"><img src="images/placeholders/logo.jpg"/></div><a href="#"><div class="ns-content"><p>Your trip request has been accepted by <span class="ns-box-span">the owner</span></p></div></a>',
						ttl : 4000,
						effect : 'thumbslider'
					});
					notification.show();
				}, 1000 );
			} );

			var bttn3 = document.getElementById( 'notification-trigger-3' );
			bttn3.addEventListener( 'click', function() {
				setTimeout( function() {
					var notification = new NotificationFx({
						message : '<div class="ns-thumb notif3"><img src="images/placeholders/boatlogo.jpg"/></div><a href="#"><div class="ns-content"><p>New boats listed in your area! <span class="ns-box-span">Check it out!</span></p></div></a>',
						ttl : 4000,
						effect : 'thumbslider'
					});
					notification.show();
				}, 1000 );
			} );

			var bttn4 = document.getElementById( 'notification-trigger-4' );
			bttn4.addEventListener( 'click', function() {
				setTimeout( function() {
					var notification = new NotificationFx({
						message : '<div class="ns-thumb notif4"><img src="images/placeholders/cap1.jpg"/></div><a href="#"><div class="ns-content"><p><span class="ns-box-span">Captain James</span> has requested to operate your boat</p></div></a>',
						ttl : 4000,
						effect : 'thumbslider'
					});
					notification.show();
				}, 1000 );
			} );
		})();
	</script>

