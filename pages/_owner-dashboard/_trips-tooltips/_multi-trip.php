<!-- <div id="trip-multidate" class="tooltipster-trips"> -->
	<div id="trip-0010-tooltipster" class="bs-trip  multi-trip  trip-tooltip collapsed">
        <div class="trip-header row">
            <div class="col-xs-12">
                <h2><a href="" class="blue">Trip #0010</a> - January 20th, 2017</h2>
                <span class="trip-status pending">
                	<img width="20" height="20" src="images/bs-calendar/bs_pending.svg" alt="upcomimng">
                	<span class="hidden-xs">pending</span>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6">
                <div class="row user-profile">
                    <div class="col-xs-12">
                        <span class="img-wrapper">
                            <img src="images/placeholders/dude1.jpg" >
                        </span>
                        <span class="user-details">
                            <span class="user-kind margin-bottom-none">
                                <strong>The Renter:</strong>
                            </span>
                            <span class="user-name">
                                Pedro Manuel
                            </span>
                            <span class="user-send-message small">
                                <a href=""><i class="bs-icon-bs_messages"></i> Send Message</a>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 border-left-gray">
                <div class="row user-profile">
                    <div class="col-xs-12">
                        <span class="img-wrapper">
                            <img src="images/placeholders/cap1.jpg" >
                        </span>
                        <span class="user-details">
                            <span class="user-kind margin-bottom-none">
                                <strong>The Captain:</strong>
                            </span>
                            <span class="user-name">
                                Alfred Molina
                            </span>
                            <span class="user-send-message small">
                                <a href=""><i class="bs-icon-bs_messages"></i> Send Message</a>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12"><hr class="border-bottom-gray padding-bottom-full margin-top-none"></div>
        </div>
        <div id="trip-0010-tooltipster-moredetails" class="trip-body row collapse">
            <div class="properties col-xs-12 col-sm-4">
                <div class="row">
                    <div class="col-xs-12"><p><i class="bs-icon-bs_half-day"></i> Half Day</p>
                    <p><i class="bs-icon-bs_passengers"></i> 8 Passengers</p>
                    <p><i class="bs-icon-bs_time-65"></i> 8:30 am - 12:30 pm</p>
                    <p><i class="bs-icon-bs_captain-required"></i> With Captain</p>
                    <p class="price"><i class="bs-icon-bs_earning blue"></i> 
                    $480</p></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 border-left-gray">
                <div class="row">
                	<div class="col-xs-12">
	                    <span class="open-bs-messenger">
	                    	<span class="img-wrapper">
	                    		<img src="images/bs_messenger.svg" alt="">
		                    </span>
		                    <span class="red-badge inline">3</span>
		                </span>

		                <span class="small pull-right">
		                	Fri Sep 20,  2:34 pm
		                </span>
                	</div>
                </div>
                <div class="row margin-top-full">
                	<div class="col-xs-12">
                		<p class="small">
                			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit totam quae ex voluptates architecto eius, sed cumque culpa. Ut, et!
                		</p>
                	</div>
                </div>
            </div>
        </div>
        <div class="trip-footer row">
            <div class="col-xs-4">   
                <p class="margin-bottom-none"><a class="more-details-trigger btn btn-secondary btn-sm btn-knockout btn-block" data-toggle="collapse" data-target="#trip-0010-tooltipster-moredetails">More Details</a></p>
            </div>
            <div class="col-xs-4">   
                <p class="margin-bottom-none"><a href="" class="btn btn-secondary btn-sm btn-knockout btn-block">Decline</a></p>
            </div>
            <div class="col-xs-4">
                <p class="margin-bottom-none"><a href="" class="btn btn-primary btn-sm btn-block"><i class="bs-icon-bs_complete"></i>Accept Trip</a></p>
            </div>
        </div>
    </div>
    <hr>    

    <!-- SECOND TRIP -->
    <div id="trip-0011-tooltipster" class="bs-trip multi-trip trip-tooltip collapsed">
        <div class="trip-header row">
            <div class="col-xs-12">
                <h2><a href="" class="blue">Trip #0011</a> - January 20th, 2017</h2>
                <span class="trip-status pending">
                    <img width="20" height="20" src="images/bs-calendar/bs_pending.svg" alt="upcomimng">
                    <span class="hidden-xs">pending</span>   
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6">
                <div class="row user-profile">
                    <div class="col-xs-12">
                        <span class="img-wrapper">
                            <img src="images/placeholders/dude1.jpg" >
                        </span>
                        <span class="user-details">
                            <span class="user-kind margin-bottom-none">
                                <strong>The Renter:</strong>
                            </span>
                            <span class="user-name">
                                Pedro Manuel
                            </span>
                            <span class="user-send-message small">
                                <a href=""><i class="bs-icon-bs_messages"></i> Send Message</a>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 border-left-gray">
                <div class="row user-profile">
                    <div class="col-xs-12">
                        <span class="img-wrapper">
                            <img src="images/placeholders/cap1.jpg" >
                        </span>
                        <span class="user-details">
                            <span class="user-kind margin-bottom-none">
                                <strong>The Captain:</strong>
                            </span>
                            <span class="user-name">
                                Alfred Molina
                            </span>
                            <span class="user-send-message small">
                                <a href=""><i class="bs-icon-bs_messages"></i> Send Message</a>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12"><hr class="border-bottom-gray padding-bottom-full margin-top-none"></div>
        </div>
        <div id="trip-0011-tooltipster-moredetails" class="trip-body row collapse">
            <div class="properties col-xs-12 col-sm-4">
                <div class="row">
                    <div class="col-xs-12"><p><i class="bs-icon-bs_half-day"></i> Half Day</p>
                    <p><i class="bs-icon-bs_passengers"></i> 8 Passengers</p>
                    <p><i class="bs-icon-bs_time-65"></i> 8:30 am - 12:30 pm</p>
                    <p><i class="bs-icon-bs_captain-required"></i> With Captain</p>
                    <p class="price"><i class="bs-icon-bs_earning blue"></i> 
                    $480</p></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 border-left-gray">
                <div class="row">
                    <div class="col-xs-12">
                        <span class="open-bs-messenger">
                            <span class="img-wrapper">
                                <img src="images/bs_messenger.svg" alt="">
                            </span>
                            <span class="red-badge inline">3</span>
                        </span>

                        <span class="small pull-right">
                            Fri Sep 20,  2:34 pm
                        </span>
                    </div>
                </div>
                <div class="row margin-top-full">
                    <div class="col-xs-12">
                        <p class="small">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit totam quae ex voluptates architecto eius, sed cumque culpa. Ut, et!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="trip-footer row">
            <div class="col-xs-4">   
                <p class="margin-bottom-none"><a class="more-details-trigger btn btn-secondary btn-sm btn-knockout btn-block" data-toggle="collapse" data-target="#trip-0011-tooltipster-moredetails">More Details</a></p>
            </div>
            <div class="col-xs-4">   
                <p class="margin-bottom-none"><a href="" class="btn btn-secondary btn-sm btn-knockout btn-block">Decline</a></p>
            </div>
            <div class="col-xs-4">
                <p class="margin-bottom-none"><a href="" class="btn btn-primary btn-sm btn-block"><i class="bs-icon-bs_complete"></i>Accept Trip</a></p>
            </div>
        </div>
    </div>
<!-- </div> -->