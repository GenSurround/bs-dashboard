<div class="dashboard-section section-trips-length">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_pricing"></i>
        Trip Length & Pricing</h2>
        <p class="intro-text">First, set how long you would like your boat to be rented for (I.e. 4-hours, 8-hours, or extended trips). Rental prices tips are calculated based on location, similar boats and rental demand.</p>
    </div>

    <div class="pricing-calculator row">
        <div class="pricing-selector half-day unset col-xs-4 col-sm-4 text-center">
            <input id="half-day" class="bs-checkbox bs-checkbox-lg" type="checkbox">
            <label for="half-day">
                <i class="bs-icon-bs_half-day"></i><span> Half-day <span class="small inline">&nbsp; (4 hours)</span></span>
            </label>
            <hr class="border-bottom">
            <div class="settings">
              <p class="amount">
                  $480 
              </p>
              <span class="small bottom"> Lorem ipsum dolor.</span>
            </div>
        </div>
        <div class="pricing-selector full-day unset col-xs-4 col-sm-4 text-center border-left-gray">
            <input id="full-day" class="bs-checkbox bs-checkbox-lg" type="checkbox">
            <label for="full-day" >
                <i class="bs-icon-bs_full-day"></i><span> Full-day <span class="small inline">&nbsp; (8 hours)</span></span>
            </label>

            <hr class="border-bottom">
            <div class="settings">
              <div class="fixed-price">
                <p class="amount">
                    $1,450 
                </p>

                <p><a class="set-own-price btn btn-sm btn-knockout btn-secondary">
                    Set my own price
                </a></p>
              </div>

              <div class="custom-price">
                <p class="amount">&nbsp;</p>
                  <div id="custom-price" class="big-tooltip" data-low="1500" data-high="3000" data-smart="1450"></div>

                  <div id="metrorenter" class="metrorenter row">
                    <div class="col-xs-12 col-sm-6">
                      <?php include 'images/bs-icons/spare/bs_metrorenter.svg'; ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <span class="metrorenter-msg"><span></span></span>
                    </div>
                    
                  </div>  

                  <p><a class="save-own-price btn btn-sm btn-knockout btn-secondary">Save Price</a></p>
              </div>
            </div>

        </div>
        <div class="pricing-selector week unset col-xs-4 col-sm-4 text-center border-left-gray">
            
            <input id="week" class="bs-checkbox bs-checkbox-lg" type="checkbox">
            <label for="week">
                <i class="bs-icon-bs_week"></i><span> Week</span>
            </label>
            <hr class="border-bottom">
            <div class="settings">
              <p class="amount">&nbsp;</p>
              <div id="full-day-price" class="big-tooltip"></div>
            </div>
            <br>
            <p class="small">Discount for trips a week or longer</p>
            
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 text-center">
            <p class="small">Note: Your earnings are 72% of the rental price. The balance covers insurance, towing and Boatsetter fees.</p>
        </div>
    </div>
</div>

<div class="dashboard-section section-smart-pricing">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_smart-pricing"></i>
        Smart Pricing</h2>
        <p class="intro-text">Maximize your earnings and save time by allowing Boasetter to dynamically adjust your prices each day based upon seasonality, demand, day of the week, holidays, etc. </p>
    </div>

    <div class="row margin-top-double margin-bottom-double">
        <div class="col-xs-12 col-sm-6 text-center">
            <input id="smart-pricing" class="bs-checkbox bs-checkbox-lg" type="checkbox">
            <label for="smart-pricing" class="horizontal">
                <span>Turn on Smart Pricing</span>
            </label>
        </div>
        <div class="col-xs-12 col-sm-6 text-center">
            <a class="btn btn-sm btn-knockout btn-secondary" data-toggle="modal" data-target="#modal-smart-pricing">
                Preview Pricing
            </a>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-xs-12 text-right">
    <div class="save-progress">
        <a class="btn btn-secondary btn-save">Save</a>
    </div>
  </div>
</div>


<?php include "_calendar/december-pricing.php"; ?>