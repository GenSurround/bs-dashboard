<div class="dashboard-section section-my_fleet">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-my_fleet blue"></i>
        My Fleet</h2>
        <p class="intro-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus cumque perspiciatis dolor id asperiores. Error quis voluptate ratione tempore amet?</p>
    </div>
    <div class="row find-boat margin-top-half margin-bottom-full">
        <div class="col-xs-5 col-sm-5 pull-right wrapper-search">
            <p class="bs-inputs-icon">
                <input class="bs-inputs bs-inputs-lg" type="text" placeholder="Find a Boat by Name">
                <i class="bs-icon-bs_search"></i>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="boat col-xs-12 border-bottom">
            <div class="row">
                <div class="col-xs-7 col-sm-8 wrapper-boat">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="user-profile">
                                <span class="img-wrapper">
                                    <img src="images/placeholders/boat.jpg" >
                                </span>
                                <span class="user-details">
                                    <span class="user-name">
                                        White Dolphin - 34'
                                    </span>
                                    <span class="user-rating">
                                        <i class="fa fa-star yellow"></i>
                                        <i class="fa fa-star yellow"></i>
                                        <i class="fa fa-star yellow"></i>
                                        <i class="fa fa-star yellow"></i>
                                        <i class="fa fa-star yellow"></i>
                                        <small>(6 reviews)</small>
                                    </span>
                                    
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="row text-left stats">
                                <div class="boat-feature">
                                    <i class="bs-icon-bs_location"></i>
                                    <span class="boat-feature-name">Location</span>
                                    <span class="boat-feature-value">Miami, FL</span>
                                </div>
                                <div class="boat-feature">
                                    <i class="bs-icon-captain_dashboard_trips"></i>
                                    <span class="boat-feature-name">Trips</span>
                                    <span class="boat-feature-value">18 Trips</span>
                                </div>
                                <div class="boat-feature">
                                    <i class="bs-icon-members_captain-01"></i>
                                    <span class="boat-feature-name">Captains</span>
                                    <span class="boat-feature-value">2 Asigned</span>
                                </div>
                                <div class="boat-feature">
                                    <i class="bs-icon-bs_pending"></i>
                                    <span class="boat-feature-name">Pending</span>
                                    <span class="boat-feature-value">Requests</span>
                                    <span class="red-badge">1</span>
                                </div>
                            </div>
                        </div>
                    </div>
<!--                     <div class="row">
                        <div class="col-xs-12">
                            <div class="row text-center">
                                <div class="boat-feature">
                                    <i class="bs-icon-bs_location"></i>
                                    <span class="boat-feature-name">Location</span>
                                    <span class="boat-feature-value">Miami, FL</span>
                                </div>
                                <div class="boat-feature">
                                    <i class="bs-icon-captain_dashboard_trips"></i>
                                    <span class="boat-feature-name">Trips</span>
                                    <span class="boat-feature-value">18 Trips</span>
                                </div>
                                <br class="only-xxs"/>
                                <div class="boat-feature">
                                    <i class="bs-icon-members_captain-01"></i>
                                    <span class="boat-feature-name">Captains</span>
                                    <span class="boat-feature-value">2 Asigned</span>
                                </div>
                                <div class="boat-feature">
                                    <i class="bs-icon-bs_pending"></i>
                                    <span class="boat-feature-name">Pending</span>
                                    <span class="boat-feature-value">Requests</span>
                                    <span class="red-badge">1</span>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="col-xs-5 col-sm-4 margin-top-full wrapper-select">
                    <div class="row no-underline text-left">
                        <div class="col-xs-12">
                            <label class="bs-select-icon"><i class="bs-icon-bs_status blue"></i></label>
                            <select class="cs-select cs-skin-underline">
                                <option data-class="green" value="" disabled selected>Active</option>
                                <option data-class="yelloww" value="2" data-option="green">Snooze</option>
                                <option data-class="red" value="3">Inactive</option>
                            </select>
                        </div>
                        <div class="col-xs-12">
                            <label class="bs-select-icon"><i class="bs-icon-bs_boat blue"></i></label>
                            <select class="cs-select cs-skin-underline">
                                <option value="" disabled selected>Boat Profile</option>
                                <option value="1">Boat Profile 1</option>
                                <option value="1">Boat Profile 2</option>
                                <option value="1">Boat Profile 3</option>
                            </select>
                        </div>
                        <div class="col-xs-12">
                            <a data-toggle="collapse" href="#boat-1">
                                <label class="bs-select-icon"><i class="bs-icon-bs_availability blue"></i></label>
                                <select class="cs-select cs-skin-underline">
                                    <option value="" disabled selected>Availability</option>
                                </select>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
            <div id="boat-1" class="row collapse">
                <div class="availability-calendar">
                    <div class="bs-calendar">
                        <div class="bs-calendar-header">
                                <h1>December 2016</h1>
                            <div class="bs-calendar-nav">
                                <a class="prev">
                                    <i class="bs-icon-bs_left-arrow blue"></i> <span class="long">November</span>
                                    <span class="short">Nov</span>
                                </a>
                                <a class="next">
                                    <span class="long">January</span>
                                    <span class="short">Jan</span> <i class="bs-icon-bs_right-arrow blue"></i> 
                                </a>
                            </div>
                        </div>

                        <div class="bs-calendar-months">
                            <div class="bs-calendar-selected-month">
                                <?php 
                                // include "_calendar/_december.php";
                                ?>
                            </div>
                        </div>

                        <div class="legend">
                            <span><img width="20" height="20" src="images/bs-calendar/bs_pending.svg" alt="Pending Approval"> Pending Approval</span>
                            <span><img width="20" height="20" src="images/bs-calendar/bs_upcoming.svg" alt="Upcoming"> Upcoming</span>
                            <span><img width="20" height="20" src="images/bs-calendar/bs_completed.svg" alt="Completed"> Completed</span>
                            <span><img width="20" height="20" src="images/bs-calendar/bs_cancelled.svg" alt="Cancelled"> Cancelled</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script id="day-tooltipster"
type="text/x-handlebars-template">
<div class="tooltipster-trips">
    <div id="d-{{day}}-tooltipster" class="bs-trip trip-tooltip collapsed">
        <div class="trip-header row">
            <div class="col-xs-12">
                <h3>Set Your Status:</h3>
            </div>
            <div class="col-xs-12 col-sm-6 margin-bottom-full">
                <input name="availability" id="d-{{day}}-available" class="simple" type="radio" checked="checked" value="available">
                <label for="d-{{day}}-available" class="btn btn-block btn-available ">
                    <img width="20" height="20" src="images/bs-calendar/bs_booked.svg" alt="unavailable">
                    Available
                </label>
            </div>
            <div class="col-xs-12 col-sm-6 margin-bottom-full">
                <input name="availability" id="d-{{day}}-unavailable" class="simple" type="radio" value="unavailable">
                <label for="d-{{day}}-unavailable" class="btn btn-block btn-unavailable btn-knockout">
                    <img width="20" height="20" src="images/bs-calendar/bs_unavailable.svg" alt="unavailable">
                    Unavailable
                </label>
            </div>
            <div class="col-xs-12 col-sm-6 text-center">
                <span class="margin-bottom-full">
                    <input name="range" id="d-{{day}}-single" class="simple" type="radio" checked="checked">
                    <label for="d-{{day}}-single" class="horizontal">Just Today</label>
                </span>
            </div>
            <div class="col-xs-12 col-sm-6 text-center">
                <span class="margin-bottom-full">
                    <input name="range" id="d-{{day}}-multi" class="simple" type="radio">
                    <label for="d-{{day}}-multi" class="horizontal">Multiple Days</label>
                </span>
            </div>
        </div>
        <hr class="border-bottom-gray padding-bottom-full margin-top-none">
        <div class="row trip-body">
            <div class="col-xs-12 col-sm-4">
                <p>Starting today</p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <p>
                    <label class="bs-select small margin-bottom-none">Filter from</label>
                    <span class="bs-datepicker">
                        <input id="d-{{day}}-filter-from" class="bs-inputs short"  type="text" value="{{day}}/{{month}}/{{year}}" disabled="disabled">
                    </span>
                </p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <label class="bs-select small margin-bottom-none">To</label>
                <span class="bs-datepicker">
                    <input id="d-{{day}}-filter-to" class="bs-inputs short"  type="text" value="{{day}}/{{month}}/{{year}}" disabled="disabled">
                </span>
            </div>
        </div>
        <div class="row">
            <p class="text-center margin-bottom-none">
                <a id="d-{{day}}-save" href="#" class="btn btn-secondary">Save Status</a>
            </p>
        </div>
    </div>
</div>
</div>
</script>