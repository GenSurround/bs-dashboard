<div class="dashboard-section section-availability">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_availability"></i>
        Availability</h2>
        <p class="intro-text">Click on each date to update your boat's availability.</p>
    </div>

    <div class="availability-calendar">
    	<div class="bs-calendar">
    		<div class="bs-calendar-header">
    				<h1>December 2016</h1>
    			<div class="bs-calendar-nav">
    				<a class="prev">
    					<i class="bs-icon-bs_left-arrow blue"></i> <span class="long">November</span>
                        <span class="short">Nov</span>
    				</a>
    				<a class="next">
    					<span class="long">January</span>
                        <span class="short">Jan</span> <i class="bs-icon-bs_right-arrow blue"></i> 

    				</a>
    			</div>
    		</div>

            <div class="bs-calendar-months">
                <div class="bs-calendar-selected-month">
                    <?php 
                    // include "_calendar/_december.php";
                    ?>
                </div>
            </div>

			<div class="legend">
				<span><img width="20" height="20" src="images/bs-calendar/bs_pending.svg" alt="Pending Approval"> Pending Approval</span>
				<span><img width="20" height="20" src="images/bs-calendar/bs_upcoming.svg" alt="Upcoming"> Upcoming</span>
				<span><img width="20" height="20" src="images/bs-calendar/bs_completed.svg" alt="Completed"> Completed</span>
				<span><img width="20" height="20" src="images/bs-calendar/bs_cancelled.svg" alt="Cancelled"> Cancelled</span>
			</div>
    	</div>
    </div>
</div>

<div class="dashboard-section section-pending-requests border-bottom">
	<div class="dashboard-section-header row">
		<div class="col-xs-12">
		    <h2 class="intro-text">
		    <i class="bs-icon-bs_pending blue"></i>
		    <span>Pending Trip Requests<span class="red-badge">1</span></span> </h2>
		    <p class="intro-text">Please accept or decline the following trip requests:</p>
		</div>
	</div>

<div id="trip-1108" class="bs-trip collapsed">
        <div class="trip-header row">
            <div class="col-xs-12">
                <h2> <i class="trip-state nc-icon-outline arrows-1_minimal-right"></i> <a href="" class="blue">Trip #1108</a> - October 14th, 2016</h2>
                <span class="open-bs-messenger">
                    <span class="img-wrapper">
                        <img src="images/bs_messenger.svg" alt="">
                    </span>
                    <span class="red-badge">3</span>
                </span>
            </div>

        </div>
        <div class="trip-body row">
            <div class="properties col-xs-8">
                <div class="row margin-top-full">
                    <div class="col-xs-6"><p><i class="bs-icon-bs_half-day"></i> Half Day</p></div>
                    <div class="col-xs-6"><p><i class="bs-icon-bs_passengers"></i> 8 Passengers</p></div>
                    <div class="col-xs-6"><p><i class="bs-icon-bs_time-65"></i> 8:30 am - 12:30 pm</p></div>
                    <div class="col-xs-6"><p><i class="bs-icon-bs_captain-required"></i> With Captain</p></div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="row  margin-top-full">
                    <div class="col-xs-12 margin-top-full text-right">
                        <span class="price">
                            <i class="bs-icon-bs_earning blue"></i> $480
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="trip-footer trip-footer-collapsed row">
            <div class="col-xs-12">
                <p class="text-right">
                    <a class="more-detail btn btn-secondary btn-knockout">More Details</a>
                    <a href="" class="btn btn-secondary btn-knockout">Decline</a>
                    <a href="" class="btn btn-primary"><i class="bs-icon-bs_availability"></i> Accept Trip</a>
                </p>
            </div>
        </div>
        <div class="trip-more-info">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="row user-profile">
                        <div class="col-xs-12">
                            <span class="img-wrapper">
                                <img src="images/placeholders/dude1.jpg" >
                            </span>
                            <span class="user-details">
                                <span class="user-kind margin-bottom-none">
                                    <strong>The Renter:</strong>
                                </span>
                                <span class="user-name">
                                    Pedro Manuel
                                </span>
                                <span class="user-send-message small">
                                    <a href=""><i class="bs-icon-bs_messages"></i> Send Message</a>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="row user-more-info margin-top-full">
                        <div class="col-xs-6">             
                            <img height="28" src="images/bs_messenger.svg"> 
                        </div>
                        <div class="col-xs-6 text-right">
                            <span class="small">3 Messages</span>
                        </div>
                        <div class="col-xs-12 margin-top-half">
                            <p class="user-description margin-bottom-half small">
                            Lorem ipsum dolor sit amet, conseetur adipiscing elit. Etiam eu tristique lacus. Lorem ipsum dolor sit amet, conseetur adipiscing elit. Etiam eu tristique lacus.
                            </p>
                            <p class="margin-top-none small">
                                Fri Sep 20, 2:34 pm
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 border-left-gray">
                    <div class="row user-profile">
                        <div class="col-xs-12">
                            <span class="img-wrapper">
                                <img src="images/placeholders/cap1.jpg" >
                            </span>
                            <span class="user-details">
                                <span class="user-kind margin-bottom-none">
                                    <strong>The Captain:</strong>
                                </span>
                                <span class="user-name">
                                    Alfred Molina
                                </span>
                                <span class="user-send-message small">
                                    <a href=""><i class="bs-icon-bs_messages"></i> Send Message</a>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="row captain user-more-info margin-top-full">
                        <div class="col-xs-12 margin-top-half">
                            <div class="row captain-features text-center">
                                <div class="captain-feature">
                                    <i class="bs-icon-bs_location"></i>
                                    <span class="captain-feature-name">Location</span>
                                    <span class="captain-feature-value">Miami, FL</span>
                                </div>
                                <div class="captain-feature">
                                    <i class="bs-icon-bs_experience"></i>
                                    <span class="captain-feature-name">Experience</span>
                                    <span class="captain-feature-value">11 Trips</span>
                                </div>
                                <div class="captain-feature">
                                    <i class="bs-icon-bs_response"></i>
                                    <span class="captain-feature-name">Avg Resp</span>
                                    <span class="captain-feature-value">23 min</span>
                                </div>
                                <div class="captain-feature">
                                    <i class="bs-icon-bs_boat"></i>
                                    <span class="captain-feature-name">Assigned</span>
                                    <span class="captain-feature-value">10 Boats</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="text-right margin-top-full margin-bottom-none">
                        <span class="more-detail btn btn-secondary btn-knockout">
                            Less Details
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="dashboard-section section-recent-messages  border-bottom">
	<div class="dashboard-section-header row">
		<div class="col-xs-12">
		    <h2 class="intro-text">
			    <i class="bs-icon-bs_recent-messages blue"></i>
		    Recent Messages
		    </h2>
		</div>

		<span class="open-bs-messenger">
			<span class="img-wrapper">
				<img src="images/bs_messenger.svg" alt="">
			</span>
			<span class="red-badge">3</span>
		</span>
	</div>

    <div class="bs-message">
    	<div class="message-header row">
    		<div class="col-xs-12">
    			<div class="message-user">
    				<span class="img-wrapper">
    					<img src="images/placeholders/dude1.jpg" >
    				</span>
    				<span class="user-details">
    					<span class="user-name">
    						Christopher
    					</span>
    					<span class="date-time">
    						Fri Sep 20, 2:34 pm
    					</span>
    				</span>
    			</div>
    		</div>
    	</div>
    	<div class="message-body row">
    		<div class="message col-xs-12 col-sm-8">
				<p class="margin-top-full">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu tristique lacus. Phasellus congue, dolor at mollis imperdiet, erat orci sodales mauris, non tristique massalacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu tristique lacus.</p>
    		</div>
    		<div class="col-xs-12 col-sm-4 text-right">
				<p class="margin-top-full">
					<a href="" class="btn btn-secondary btn-knockout"><i class="bs-icon-bs_reply"></i> Reply</a>
					<a href="" class="btn btn-secondary btn-knockout"><i class="bs-icon-bs_messages"></i> Messenger</a>
				</p>
    		</div>
    	</div>
    </div>
</div>

<script id="day-tooltipster"
type="text/x-handlebars-template">
<div class="tooltipster-trips">
    <div id="d-{{day}}-tooltipster" class="bs-trip trip-tooltip collapsed">
        <div class="trip-header row">
            <div class="col-xs-12">
                <h3>Set Your Status:</h3>
            </div>
            <div class="col-xs-12 col-sm-6 margin-bottom-full">
                <input name="availability" id="d-{{day}}-available" class="simple" type="radio" checked="checked" value="available">
                <label for="d-{{day}}-available" class="btn btn-block btn-available ">
                    <img width="20" height="20" src="images/bs-calendar/bs_booked.svg" alt="unavailable">
                    Available
                </label>
            </div>
            <div class="col-xs-12 col-sm-6 margin-bottom-full">
                <input name="availability" id="d-{{day}}-unavailable" class="simple" type="radio" value="unavailable">
                <label for="d-{{day}}-unavailable" class="btn btn-block btn-unavailable btn-knockout">
                    <img width="20" height="20" src="images/bs-calendar/bs_unavailable.svg" alt="unavailable">
                    Unavailable
                </label>
            </div>
            <div class="col-xs-12 col-sm-6 text-center">
                <span class="margin-bottom-full">
                    <input name="range" id="d-{{day}}-single" class="simple" type="radio" checked="checked">
                    <label for="d-{{day}}-single" class="horizontal">Just Today</label>
                </span>
            </div>
            <div class="col-xs-12 col-sm-6 text-center">
                <span class="margin-bottom-full">
                    <input name="range" id="d-{{day}}-multi" class="simple" type="radio">
                    <label for="d-{{day}}-multi" class="horizontal">Multiple Days</label>
                </span>
            </div>
        </div>
        <hr class="border-bottom-gray padding-bottom-full margin-top-none">
        <div class="row trip-body">
            <div class="col-xs-12 col-sm-4">
                <p>Starting today</p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <p>
                    <label class="bs-select small margin-bottom-none">Filter from</label>
                    <span class="bs-datepicker">
                        <input id="d-{{day}}-filter-from" class="bs-inputs short"  type="text" value="{{day}}/{{month}}/{{year}}" disabled="disabled">
                    </span>
                </p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <label class="bs-select small margin-bottom-none">To</label>
                <span class="bs-datepicker">
                    <input id="d-{{day}}-filter-to" class="bs-inputs short"  type="text" value="{{day}}/{{month}}/{{year}}" disabled="disabled">
                </span>
            </div>
        </div>
        <div class="row">
            <p class="text-center margin-bottom-none">
                <a id="d-{{day}}-save" href="#" class="btn btn-secondary">Save Status</a>
            </p>
        </div>
    </div>
</div>
</div>
</script>