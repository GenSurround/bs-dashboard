<div class="dashboard-section section-boats_i_run">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-captain_dashboard_cel blue"></i>
        Get the BST App</h2>
    </div>
    <div class="indented-content margin-top-full margin-bottom-double">
        
            <img class="pull-left margin-right-full" width="83" height="83" src="images/dashboard/BST-app-logo.png" alt="BST app logo">

            <h3 class="padding-top-half margin-bottom-half">Boatsetter Today</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas accumsan, nisi a tincidunt volutpat, nulla ligula gravida tellus, ut tincidunt purus massa vitae ipsum.</p>

            <p class="margin-top-double">
                <a href="" class="margin-right-full">
                    <img width="185" height="55" src="images/dashboard/app-store-logo.png" alt="App Store">
                </a>
                <br class="visible-xs">
                <br class="visible-xs">
                <a href="" class="margin-right-full">
                    <img width="185" height="55" src="images/dashboard/google-play-logo.png" alt="Google Play">
                </a>
            </p>

        <hr class="dark margin-top-double margin-bottom-full">
        <div class="dashboard-section-header">
            <h3 class="intro-text">
            <i class="bs-icon-bs-paper"></i>
            Paper Inspection
            </h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit <a href="javascript:void(0);">click here</a>.</p>
        </div>
        
    </div>
</div>