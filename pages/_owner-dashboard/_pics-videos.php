<div class="dashboard-section section-pics-videos">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_pics-videos"></i>
        Pics & Videos</h2>
        <p class="intro-text">Make sure renters see all the amazing things your boat has to offer...don't leave anything out!</p>
    </div>

    <div class="help-photos">
        <h3 class="margin-bottom-half">Need Help with Photos?</h3>
        <p class="margin-bottom-half">Find out if you qualify for a free professional photo shoots today!</p>
        <a href="" class="btn btn-primary btn-sm">
            SCHEDULE NOW
        </a>
    </div>

    <div class="pics-videos-uploads">
        <div class="row">
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia1">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_cover.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Cover Photo</p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia2">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_left-side.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Left Side</p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia3">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_right-side.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Right Side</p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia4">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_front.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Front</p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia5">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_back.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Back</p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia6">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_angle.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Angle</p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia7">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_helm.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Helm</p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia8">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_cabin.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Cabin 1</p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia9">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_cabin.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Cabin 2</p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia10">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_other.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Other</p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia11">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_other.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Other</p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3">
                <div class="dropzone-trigger" id="dzmedia12">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_other.svg">
                    <span class="hidden-xs">Drag and drop here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Other</p>
            </div>
            <div class="col-xs-12">
                <div class="dropzone-trigger" id="dzmedia12">
                    <p class="dz-message">
                    <img width="100" height="60" src="images/dashboard/pics_other.svg">
                    <span class="hidden-xs">Drag multiple files here</span>
                    <br><br>
                    <a class="btn btn-secondary btn-knockout btn-sm">Upload</a>
                    </p>
                </div>
                <p>Other</p>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-xs-12 text-right">
    <div class="save-progress">
        <a class="btn btn-secondary btn-save">Save</a>
    </div>
  </div>
</div>