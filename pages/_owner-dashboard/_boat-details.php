<div class="dashboard-section section-boat-details">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_boat-details"></i>
        Boat Details</h2>
        <p class="intro-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos maxime veniam, qui. Ullam culpa fugit, dolores debitis dicta tempora officia.</p>
    </div>
</div>


<div class="dashboard-section section-boat-details-general-info border-bottom">
    <div class="dashboard-section-header">
        <h3 class="intro-text">
        <i class="bs-icon-bs_general-info blue"></i>
        General info</h3>
        <a id="general-info-edit-trigger" class="pull-right add-more" data-toggle="collapse" data-target="#general-info-edit">Edit <i class="nc-icon-outline x075 arrows-1_double-right"></i></a>
    </div>

    <div id="dashboard-section-content" class="collapse in">
        <div class="general-info-static">
            <div class="row margin-top-double margin-bottom-double font-size-16">
                <div class="col-xs-12 col-sm-6">
                    <div class="row data-set">
                        <div class="data-set-name col-xs-6 col-sm-4"><p>Boat Name</p></div>
                        <div class="data-set-value col-xs-6 col-sm-8"><p>Warren Kelly</p></div>
                    </div>
                    <div class="row data-set">
                        <div class="data-set-name col-xs-6 col-sm-4"><p>Make </p></div>
                        <div class="data-set-value col-xs-6 col-sm-8"><p>Azimut</p></div>
                    </div>
                    <div class="row data-set">
                        <div class="data-set-name col-xs-6 col-sm-4"><p>Year Built</p></div>
                        <div class="data-set-value col-xs-6 col-sm-8"><p>2016</p></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="row data-set" >
                        <div class="data-set-name col-xs-6 col-sm-4 tooltip-registration"  data-tooltip-content="#tooltip-registration_content" ><p>Registration #</p></div>
                        <div class="data-set-value col-xs-6 col-sm-8"><p>12145-5</p></div>
                    </div>
                    <div class="row data-set">
                        <div class="data-set-name col-xs-6 col-sm-4"><p>Model</p></div>
                        <div class="data-set-value col-xs-6 col-sm-8"><p>60 Flybridge</p></div>
                    </div>
                    <div class="row data-set">
                        <div class="data-set-name col-xs-6 col-sm-4"><p>Market Value </p></div>
                        <div class="data-set-value col-col-xs-6  col-sm-8"><p>1,300,000</p></div>
                    </div>
                </div>

                <div id="tooltip-registration_content" class="tooltip-content" >
                    <span><i class="bs-icon-bs_certificate font-size-24"></i></span>
                    <span>Make sure the registration is up to date. Owner is responsible for any fines related to registrations.</span>
                </div>
            </div>
        </div>

        <div id="general-info-edit" class="collapse">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <p>
                        <label for="boat-name">Boat Name</label>
                        <input id="boat-name" class="bs-inputs"  type="text" placeholder="Warren Kelly">
                    </p>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <p>
                        <label for="registration-number">Registration #</label>
                        <input id="registration-number" class="bs-inputs"  type="text" placeholder="12145-5">
                    </p>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <p>
                        <label for="make">Make</label>
                        <input id="make" class="bs-inputs"  type="text" placeholder="Azimut">
                    </p>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <p>
                        <label for="model">Model</label>
                        <input id="model" class="bs-inputs"  type="text" placeholder="60 Flybridge">
                    </p>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <label for="year-built">Year Built</label>
                    <div id="year-built-slider" class="inline-slider"></div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <label for="market-value">Market Value</label>
                    <div id="market-value-slider" class="inline-slider"></div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="checkRadioContainer">
                        <label class="small-svg">
                        <svg id="Layer_1" style="enable-background:new 0 0 107 61;" version="1.1" viewBox="0 0 107 61" x="0px" xmlns="http://www.w3.org/2000/svg" y="0px">     <style>     .st0{fill:none;stroke:#0073A2;stroke-width:2;stroke-linecap:square;stroke-miterlimit:10;}     </style>     <g>     <path class="st0" d="M8.6,42.3l87.7-19.1c0,0-4.8,30.2-32,30.2s-55.7,0-55.7,0V42.3z"></path>     <polyline class="st0" points="73.4,27.6 59.6,13.9 26.4,37.9"></polyline>     </g></svg>
                        <input name="radioGroup" type="radio"> <span>Power</span></label> <label class="medium-svg"> <!-- Generator: Adobe Illustrator 19.2.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                        <svg id="Layer_1" style="enable-background:new 0 0 43.3 43;" version="1.1" viewBox="0 0 43.3 43" x="0px" xmlns="http://www.w3.org/2000/svg" y="0px">   <style>   .st0{fill:none;stroke:#444444;stroke-width:1.5;stroke-linecap:square;stroke-miterlimit:10;}   </style>   <polygon class="st0" id="color_90_" points="34.3,39.3 4.3,39.3 2.3,33.3 40.3,33.3"></polygon>   <polygon class="st0" points="28.3,15.3 28.3,27.3 36.3,27.3"></polygon>   <polygon class="st0" points="4.3,27.3 22.3,27.3 22.3,3.3"></polygon></svg>
                        <input name="radioGroup" type="radio"> <span>Sail</span></label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="checkRadioContainer">
                        <label class="small-svg not-affiliated"><!-- Generator: Adobe Illustrator 19.2.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                        <svg id="Layer_1" style="enable-background:new 0 0 51 48;" version="1.1" viewBox="0 0 51 48" x="0px" xmlns="http://www.w3.org/2000/svg" y="0px">     <style>     .st0{fill:none;stroke:#454545;stroke-width:1.8;stroke-linecap:square;stroke-miterlimit:10;}     </style>     <line class="st0" x1="40" x2="11.7" y1="10.7" y2="39"></line>     <line class="st0" x1="40" x2="11.7" y1="39" y2="10.7"></line></svg>
                        <input name="radioGroup" type="radio"> <span>No</span></label> <label class="medium-svg affiliated"> <!-- Generator: Adobe Illustrator 19.2.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                        <svg id="Layer_1" style="enable-background:new 0 0 51 48;" version="1.1" viewBox="0 0 51 48" x="0px" xmlns="http://www.w3.org/2000/svg" y="0px">     <style>     .st0{fill:none;stroke:#454545;stroke-width:1.8;stroke-linecap:square;stroke-miterlimit:10;}     </style>     <polyline class="st0" points="6.4,27 18.4,39 44.4,13"></polyline></svg>
                        <input name="radioGroup" type="radio"> <span>Yes</span></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="dashboard-section section-boat-details-boat-profile border-bottom">
    <div class="dashboard-section-header">
        <h3 class="intro-text">
        <i class="bs-icon-bs_boat-profile blue"></i>
        Boat Profile
        </h3>
        <a class="pull-right add-more" data-toggle="collapse" data-target="#boat-profile-edit">Edit <i class="nc-icon-outline x075 arrows-1_double-right"></i></a>
    </div>

    <div class="row margin-top-double margin-bottom-double font-size-16">
        <div class="col-xs-12">
            <div class="row data-set">
                <div class="data-set-name col-xs-4 col-md-2"><p>Title</p></div>
                <div class="data-set-value col-xs-8 col-md-10"><p>Miami Beach Boat Rental - Warren Kelly</p>
            </div>
            </div>
            <div class="row data-set">
                <div class="data-set-name col-xs-4 col-md-2"><p>Description</p>
                </div>
                <div class="data-set-value col-xs-8 col-md-10"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc iaculis molestie metus, ac malesuada massa pulvinar at. Fusce congue congue sapien, et faucibus nisi luctus id. Donec efficitur id neque a pharetra. </p>
                </div>
            </div>
        </div>
    </div>

    <div id="boat-profile-edit" class="collapse">
        <div class="row">
            <div class="col-xs-12">
                <p>
                    <label for="title">Title</label>
                    <input id="title" class="bs-inputs"  type="text" placeholder="Miami Beach Boat Rental - Warren Kelly">
                </p>
                <p>
                    <label for="description">Description</label>
                    <textarea id="description" class="bs-inputs" placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc iaculis molestie metus, ac malesuada massa pulvinar at. Fusce congue congue sapien, et faucibus nisi luctus id. Donec efficitur id neque a pharetra. Fusce congue congue sapien, et faucibus nisi luctus id. Donec efficitur id neque a pharetra. "></textarea>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="dashboard-section section-boat-details-specifications border-bottom">
    <div class="dashboard-section-header">
        <h3 class="intro-text">
        <i class="bs-icon-bs_specifications blue"></i>
        Specifications
        </h3>
        <a class="pull-right add-more" data-toggle="collapse" data-target="#specifications-edit">Edit <i class="nc-icon-outline x075 arrows-1_double-right"></i></a>
    </div>
    
    <div class="row bs-icon-list margin-top-double margin-bottom-double">
        <div class="col-xs-12 col-sm-6">
            <p><i class="bs-icon-bs_dimension"></i><span>60 feet</span></p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p><i class="bs-icon-bs_passengers"></i><span>p to 12 Passengers</span></p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p><i class="bs-icon-bs_sleeps"></i><span>Sleeps 6</span></p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p><i class="bs-icon-bs_bath"></i><span>3 Heads</span></p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p><i class="bs-icon-bs_cabin-54"></i><span>2 Cabins</span></p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p><i class="bs-icon-bs_engines"></i><span>1 R6 Engine</span></p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p><i class="bs-icon-bs_horsepower"></i><span>1,600 Horsepower</span></p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p><i class="bs-icon-bs_propulsion"></i><span>1 Outboard Engine</span></p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p><i class="bs-icon-bs_tank-capacity"></i><span>740 Gallon Tank</span></p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p><i class="bs-icon-bs_max-speed-1"></i><span>Up to 30 mph</span></p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p><i class="bs-icon-bs_gas"></i><span>Diesel</span></p>
        </div>
    </div>

    <div id="specifications-edit" class="collapse">
        <div class="row margin-bottom-full">
            <div class="col-xs-12 col-sm-6">
                <label><i class="bs-icon-bs_dimension"></i><span>Length</span></label>
                <div id="dimension_slider" class="inline-slider"></div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <label><i class="bs-icon-bs_passengers"></i><span>Passengers</span></label>
                <div id="passengers_slider" class="inline-slider"></div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <label><i class="bs-icon-bs_cabin-54"></i><span>Cabins</span></label>
                <div id="cabin_slider" class="inline-slider"></div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <label><i class="bs-icon-bs_sleeps"></i><span>Sleeps</span></label>
                <div id="sleeps_slider" class="inline-slider"></div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <label><i class="bs-icon-bs_bath"></i><span>Heads</span></label>
                <div id="bath_slider" class="inline-slider"></div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <label><i class="bs-icon-bs_engines"></i><span>Horsepower</span></label>
                <div id="horsepower_slider" class="inline-slider"></div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <label><i class="bs-icon-bs_horsepower"></i><span>Engines</span></label>
                <div id="engines_slider" class="inline-slider"></div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <label><i class="bs-icon-bs_tank-capacity"></i><span>Tank Capacity</span></label>
                <div id="tank-capacity_slider" class="inline-slider"></div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <label><i class="bs-icon-bs_max-speed-1"></i><span>Max Speed</span></label>
                <div id="max-speed_slider" class="inline-slider"></div>
            </div>
<!--             <div class="col-xs-12 col-sm-6">
                <label><i class="bs-icon-bs_gas"></i><span>Diesel</span></label>
            </div> -->
            <div class="col-xs-12 col-sm-6">
                <label class="bs-select-icon"><i class="bs-icon-bs_gas"></i></label>
                <select class="cs-select cs-skin-underline">
                    <option value="" disabled selected>Fuel Type</option>
                    <option value="1">Diesel</option>
                    <option value="2">Gas</option>
                </select>
            </div>

            <div class="col-xs-12 col-sm-6">
                <label class="bs-select-icon"><i class="bs-icon-bs_propulsion"></i></label>
                <select class="cs-select cs-skin-underline">
                    <option value="" disabled selected>Fuel Type</option>
                    <option value="1">1 Outboard Engine</option>
                    <option value="2">2 Outboard Engine</option>
                    <option value="3">3 Outboard Engine</option>
                </select>
            </div>

        </div>
    </div>
   

</div>


<div class="dashboard-section section-boat-activities border-bottom">
    <div class="dashboard-section-header">
        <h3 class="intro-text">
        <i class="bs-icon-bs_activities blue"></i>
        Activities
        </h3>
    </div>

    <div class="row margin-top-double margin-bottom-double">
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check1" class="simple" type="checkbox">
            <label for="check1" class="horizontal">
                <span>Cruising</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check2" class="simple" type="checkbox">
            <label for="check2" class="horizontal">
                <span>Romantic Getaways</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check3" class="simple" type="checkbox">
            <label for="check3" class="horizontal">
                <span>Snorkeling</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check4" class="simple" type="checkbox">
            <label for="check4" class="horizontal">
                <span>Sailing</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check5" class="simple" type="checkbox">
            <label for="check5" class="horizontal">
                <span>Island excursions</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check6" class="simple" type="checkbox">
            <label for="check6" class="horizontal">
                <span>Waterskiing</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check7" class="simple" type="checkbox">
            <label for="check7" class="horizontal">
                <span>Sandbar fun</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check8" class="simple" type="checkbox">
            <label for="check8" class="horizontal">
                <span>Fishing</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check9" class="simple" type="checkbox">
            <label for="check9" class="horizontal">
                <span>Business meetings</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check10" class="simple" type="checkbox">
            <label for="check10" class="horizontal">
                <span>Family bonding</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check11" class="simple" type="checkbox">
            <label for="check11" class="horizontal">
                <span>Sight seeing</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check12" class="simple" type="checkbox">
            <label for="check12" class="horizontal">
                <span>Corporate events</span>
            </label>
        </p></div>
        
    </div>



    
</div>

<div class="dashboard-section section-boat-features-extras">
    <div class="dashboard-section-header">
        <h3 class="intro-text">
        <i class="bs-icon-bs_features-and-extras blue"></i>
        Features & extras
        </h3>
    </div>

    <div class="row margin-top-double margin-bottom-double">
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check21" class="simple" type="checkbox">
            <label for="check21" class="horizontal">
                <span>Air Conditioning</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check22" class="simple" type="checkbox">
            <label for="check22" class="horizontal">
                <span>Shade</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check23" class="simple" type="checkbox">
            <label for="check23" class="horizontal">
                <span>Fish Box</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check24" class="simple" type="checkbox">
            <label for="check24" class="horizontal">
                <span>Stereo/ipod</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check25" class="simple" type="checkbox">
            <label for="check25" class="horizontal">
                <span>Cabin</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check26" class="simple" type="checkbox">
            <label for="check26" class="horizontal">
                <span>Davit</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check27" class="simple" type="checkbox">
            <label for="check27" class="horizontal">
                <span>Depth Finder</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check28" class="simple" type="checkbox">
            <label for="check28" class="horizontal">
                <span>Cooking On Board</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check29" class="simple" type="checkbox">
            <label for="check29" class="horizontal">
                <span>Outriggers</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check210" class="simple" type="checkbox">
            <label for="check210" class="horizontal">
                <span>Gps Chart Plotter</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check211" class="simple" type="checkbox">
            <label for="check211" class="horizontal">
                <span>Drinking Water</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check212" class="simple" type="checkbox">
            <label for="check212" class="horizontal">
                <span>Rod Holders</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check27" class="simple" type="checkbox">
            <label for="check27" class="horizontal">
                <span>Vhf Radio</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check28" class="simple" type="checkbox">
            <label for="check28" class="horizontal">
                <span>Water Wash Down</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check29" class="simple" type="checkbox">
            <label for="check29" class="horizontal">
                <span>Dive Platform</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check210" class="simple" type="checkbox">
            <label for="check210" class="horizontal">
                <span>Battery Charger</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check211" class="simple" type="checkbox">
            <label for="check211" class="horizontal">
                <span>Livewell</span>
            </label>
        </p></div>
        <div class="col-xs-12 col-sm-6"><p>
            <input id="check212" class="simple" type="checkbox">
            <label for="check212" class="horizontal">
                <span>Ski Tow Bar</span>
            </label>
        </p></div>
        
    </div>
    
</div>

<div class="row">
  <div class="col-xs-12 text-right">
    <div class="save-progress">
        <a class="btn btn-secondary btn-save">Save</a>
    </div>
  </div>
</div>