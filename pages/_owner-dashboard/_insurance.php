<div class="dashboard-section section-insurance">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_insurance"></i>
        Insurance</h2>
        <p class="intro-text">Choose from two tiers of coverage for your boat rentals: select premium for more peace of mind or basic to increase your earnings.</p>
    </div>


    <div class="row">
        <div class="col-xs-12 text-center">
            <div class="insurance-plans ">
                <h2 class="text-center">
                    <i class="bs-icon-bs_premium"></i>
                    Premium
                </h2>
                <ul>
                    <li>Earn 21% of your rentals</li>
                    <li>lit ament, consectetur adipis</li>
                    <li>Lorem ipsum dolor.</li>
                    <li>lit amer conasdoka</li>
                    <li>lorepansd apsjdas</li>
                    <li>asdlkasd</li>
                </ul>
                <p class="selection">
                    <a class="btn btn-knockout btn-secondary">Select</a>
                </p>
            </div>
            <div class="insurance-plans">
                <h2 class="text-center">
                    <i class="bs-icon-bs_basic-insurance"></i>
                    Basic
                </h2>
                <ul>
                    <li>Earn 21% of your rentals</li>
                    <li>lit ament, consectetur adipis</li>
                    <li>Lorem ipsum dolor.</li>
                </ul>
                <p class="selection">
                    <a class="btn btn-knockout btn-secondary">Select</a>
                </p>
            </div>
            <hr>
            <p class="own-insurance tooltipster"  title="Please call XYZ for ABC">
                <input id="check0" type="checkbox">
                <label for="check0" class="horizontal">
                    <span>I’d like to use my own insurance.</span>
                </label>
            </p>

            <p class="note small margin-top-double">

                *Note: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, neque.
            </p>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-xs-12 text-right">
    <div class="save-progress">
        <a class="btn btn-secondary btn-save">Save</a>
    </div>
  </div>
</div>