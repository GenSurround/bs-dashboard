<div class="section-trips">
    
    <ul class="nav nav-tabs flat-tabs margin-bottom-double" role="tablist">
        <li role="presentation" 
            class="tab-pane <?php is_active( $tab ,'requests');  ?>">
            <a href="#trip-requests" aria-controls="trip-requests" role="tab" data-toggle="tab">Trip Requests</a>
        </li>
        <li role="presentation" 
            class="tab-pane <?php is_active( $tab ,'upcoming');  ?>">
            <a href="#upcoming-trips" aria-controls="upcoming-trips" role="tab" data-toggle="tab">Upcoming Trips</a>
        </li>
        <li role="presentation" 
            class="tab-pane <?php is_active( $tab ,'completed');  ?>">
            <a href="#completed-trips" aria-controls="completed-trips" role="tab" data-toggle="tab">Completed Trips</a
        ></li>
        <li role="presentation" 
            class="tab-pane <?php is_active( $tab ,'cancelled');  ?>">
            <a href="#cancelled-trips" aria-controls="cancelled-trips" role="tab" data-toggle="tab">Cancelled Trips</a
        ></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content flat-tabs">
        <div role="tabpanel" class="tab-pane <?php is_active( $tab ,'requests');  ?>" id="trip-requests">
            <?php include '_trips/_trip-requests.php'; ?>
        </div>
        <div role="tabpanel" class="tab-pane <?php is_active( $tab ,'upcoming');  ?>" id="upcoming-trips">
            <?php include '_trips/_upcoming-trips.php'; ?>
        </div>
        <div role="tabpanel" class="tab-pane <?php is_active( $tab ,'completed');  ?>" id="completed-trips">
            <?php include '_trips/_completed-trips.php'; ?>
        </div>
        <div role="tabpanel" class="tab-pane <?php is_active( $tab ,'cancelled');  ?>" id="cancelled-trips">
            <?php include '_trips/_cancelled-trips.php'; ?>
        </div>
    </div>
</div>