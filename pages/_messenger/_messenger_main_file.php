<div class="messenger-content clearfix">
	<div class="messenger-left-section">
	   <?php include 'includes/_messenger/_left-sidebar.php'; ?>
	</div>
	<div class="messenger-right-section">
			<div class="welcome-message">
				<h2>Hi Alvaro! </h2>
				<p>Welcome to Boatsetter Messenger.</p>
				<p>Select a message on to start a conversation.</p>
			</div>
		<div class="messenger-right-top-section clearfix">
			<?php include 'includes/_messenger/_main-board.php'; ?>
			<?php include 'includes/_messenger/_right-sidebar.php'; ?>
		</div>
		<div class="messenger-right-bottom-section">
			<?php include 'includes/_messenger/_message-box.php'; ?>
		</div>
	</div>
</div>