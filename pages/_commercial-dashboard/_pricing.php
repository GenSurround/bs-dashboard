<div class="dashboard-section section-trips-length">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_pricing"></i>
        Trip Length & Pricing</h2>
        <p class="intro-text">Enter your prices <strong>INCLUDING CAPTAIN FEES</strong> and how long would you like to rent your boat for? Rental rates are calculated based on different factors such as location, similar boats, chances of getting rentals and market value.  </p>
    </div>
    <div class="pricing-calculator row">
        <div class="pricing-selector half-day unset col-xs-4 col-sm-4 text-center">
            <input id="half-day" class="bs-checkbox bs-checkbox-lg" type="checkbox">
            <label for="half-day">
                <i class="bs-icon-bs_half-day"></i><span> Half-day <span class="small inline">&nbsp; (4 hours)</span></span>
            </label>
            <hr class="border-bottom">
            <div class="settings">
                <p class="amount">
                    $480
                </p>
                <span class="small bottom"> Lorem ipsum dolor.</span>
            </div>
        </div>
        <div class="pricing-selector full-day unset col-xs-4 col-sm-4 text-center border-left-gray">
            <input id="full-day" class="bs-checkbox bs-checkbox-lg" type="checkbox">
            <label for="full-day" >
                <i class="bs-icon-bs_full-day"></i><span> Full-day <span class="small inline">&nbsp; (8 hours)</span></span>
            </label>
            <hr class="border-bottom">
            <div class="settings">
                <div class="fixed-price">
                    <p class="amount">
                        $1,450
                    </p>
                    <p><a class="set-own-price btn btn-sm btn-knockout btn-secondary">
                        Set my own price
                    </a></p>
                </div>
                <div class="custom-price">
                    <p class="amount">&nbsp;</p>
                    <div id="full-day-custom-price" class="big-tooltip" data-low="1500" data-high="3000" data-smart="1450"></div>
                    <div id="metrorenter" class="metrorenter row">
                        <div class="col-xs-12 col-sm-6">
                            <?php include 'images/bs-icons/spare/bs_metrorenter.svg'; ?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <span class="metrorenter-msg"><span></span></span>
                        </div>
                        
                    </div>
                    <p><a class="save-own-price btn btn-sm btn-knockout btn-secondary">Save Price</a></p>
                </div>
            </div>
        </div>
        <div class="pricing-selector week unset col-xs-4 col-sm-4 text-center border-left-gray">
            
            <input id="week" class="bs-checkbox bs-checkbox-lg" type="checkbox">
            <label for="week">
                <i class="bs-icon-bs_week"></i><span> Week</span>
            </label>
            <hr class="border-bottom">
            <div class="settings">
                <p class="amount">&nbsp;</p>
                <div id="full-day-price" class="big-tooltip"></div>
            </div>
            <br>
            <p class="small">Discount for trips a week or longer</p>
            
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-center">
            <p><i class="bs-icon-bs_general-info blue bs-icon-lg"></i>&nbsp; Remember the price includes all Captain fees. </p>
            <p class="small">Note: Your earnings are 72% of the rental price. The balance covers insurance, towing and Boatsetter fees.</p>
        </div>
    </div>
</div>
<div class="dashboard-section section-custom-trips">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_custom"></i>
        Custom Trips</h2>
        <p class="intro-text">How long would you like to rent your boat for? Rental rates are calculated based on different factors such as location, similar boats, chances of getting rentals and market value.</p>
    </div>
    <div class="custom-trips row margin-top-double margin-bottom-double">
        <div class="custom-trip-slot col-xs-4 col-sm-4 text-center">
            <a class="add-custom-trip btn btn-secondary btn-knockout btn-sm margin-top-full">
                Add Custom Trip
            </a>
        </div>
        <div class="custom-trip-slot col-xs-4 col-sm-4 text-center border-left-gray">
        </div>
        <div class="custom-trip-slot col-xs-4 col-sm-4 text-center border-left-gray">
        </div>
    </div>
</div>
<br>
<div class="dashboard-section section-other-options">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_engines"></i>
        Other Options</h2>
        <p class="intro-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
    </div>
    <div class="pricing-other-options row">
        <div class="security-deposit col-xs-4 col-sm-4 text-center">
            <label for="security" class="blue">
                <i class="bs-icon-bs_security"></i><span> Security Deposit</span>
            </label>
            <hr class="border-bottom">
            <div class="settings margin-top-double">
                <p>
                    $1000 is BS Default Deposit amount.
                </p>
                <div class="fixed-price">
                    <p class="amount">
                        $1000
                    </p>
                    <p><a class="set-own-price btn btn-sm btn-knockout btn-secondary">
                        Set my own amount
                    </a></p>
                </div>
            </div>
            <div class="custom-price">
                <p class="amount">
                    &nbsp;
                </p>
                <div id="security-deposit-custom-price" class="big-tooltip" data-low="1500" data-high="3000" data-smart="1450"></div>
                <div id="metrorenter" class="metrorenter row">
                </div>
                <p><a class="save-own-price btn btn-sm btn-knockout btn-secondary">Save Price</a></p>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 text-center border-left-gray">
            <label for="security" class="blue">
                <i class="bs-icon-bs_gas"></i><span> Gas</span>
            </label>
            <hr class="border-bottom">
            <div class="settings margin-top-double">
                <p>
                    <input id="ct1_gas" class="simple" type="checkbox">
                    <label for="ct1_gas" class="horizontal">
                        <span>Prices include gas.</span>
                    </label>
                </p>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 text-center border-left-gray">
            <label for="security" class="blue">
                <i class="bs-icon-bs_captain_tip"></i><span> Captain Tip</span>
            </label>
            <hr class="border-bottom">
            <div class="settings margin-top-double">
                <p>
                    <input id="ct1_captain-tip" class="simple" type="checkbox">
                    <label for="ct1_captain-tip" class="horizontal">
                        <span>Prices include Captain Tip.</span>
                    </label>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- HANDLEBARS TEMPLATE FOR CUSTOM TRIP -->
<script id="custom-trip-template" type="text/x-handlebars-template">
<div id="{{trip_ID}}" class="custom-trip">
    <div class="set">
        <p class="blue"><i class="bs-icon-bs_time-65 bs-icon-lg"></i> Duration</p>
        <div class="row padding radio-checkbox">
            <div class="col-xs-4">
                <input id="{{trip_ID}}_check-hours" class="checkbox simple" type="checkbox" checked
                data-slider="#{{trip_ID}}_hours" data-kind="hours">
                <label for="{{trip_ID}}_check-hours" class="horizontal">
                    <span>Hours</span>
                </label>
            </div>
            <div class="col-xs-4">
                <input id="{{trip_ID}}_check-days" class="checkbox simple" type="checkbox"
                data-slider="#{{trip_ID}}_days" data-kind="days">
                <label for="{{trip_ID}}_check-days" class="horizontal">
                    <span>Days</span>
                </label>
            </div>
            <div class="col-xs-4">
                <input id="{{trip_ID}}_check-weeks" class="checkbox simple" type="checkbox"
                data-slider="#{{trip_ID}}_weeks" data-kind="weeks">
                <label for="{{trip_ID}}_check-weeks" class="horizontal">
                    <span>Weeks</span>
                </label>
            </div>
            <br><br>
            <div class="col-xs-12 margin-top-double border-bottom">
                <div id="{{trip_ID}}_hours" class="big-tooltip">
                    
                </div>
                <div id="{{trip_ID}}_days" class="big-tooltip">
                    
                </div>
                <div id="{{trip_ID}}_weeks" class="big-tooltip">
                    
                </div>
            </div>
        </div>
        <p class="blue margin-top-half"><i class="bs-icon-bs_dollar-sign bs-icon-lg"></i> Price</p>
        <div class="row padding">
            <div class="col-xs-12 margin-top-full border-bottom">
                <div id="{{trip_ID}}_price" class="big-tooltip">
                    
                </div>
            </div>
        </div>
        <p class="margin-top-full">
            <a id="{{trip_ID}}_save" class="btn btn-secondary btn-sm">
                Save Trip
            </a>
        </p>
    </div>
    <div class="values">
        <div class="wrapper">
            
            <input id="{{trip_ID}}_toggle" class="bs-checkbox bs-checkbox-lg" type="checkbox" checked>
            <label for="{{trip_ID}}_toggle">
                <i class="bs-icon-bs_custom_trip"></i><span> Custom Trip {{index}}</span>
            </label>
            <hr class="border-bottom">
            <div class="settings padding">
                <p class="property text-left blue"><i class="bs-icon-bs_time-65 bs-icon-lg"></i> Duration: <span id="{{trip_ID}}_duration-value" class="amount pull-right">12123</span></p>
                <p class="property text-left blue"><i class="bs-icon-bs_dollar-sign bs-icon-lg"></i> Price: <span id="{{trip_ID}}_price-value" class="amount pull-right">12123</span></p>
            </div>
        </div>
        <p class="margin-top-full">
            <a id="{{trip_ID}}_edit" class="btn btn-secondary btn-knockout btn-sm">
                Edit Trip
            </a>
        </p>
    </div>
</div>
</script>


<div class="dashboard-section section-smart-pricing">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_smart-pricing"></i>
        Smart Pricing</h2>
        <p class="intro-text">Maximize your earnings and save time by allowing Boasetter to dynamically adjust your prices each day based upon seasonality, demand, day of the week, holidays, etc. </p>
    </div>

    <div class="row margin-top-double margin-bottom-double">
        <div class="col-xs-12 col-sm-6 text-center">
            <input id="smart-pricing" class="bs-checkbox bs-checkbox-lg" type="checkbox">
            <label for="smart-pricing" class="horizontal">
                <span>Turn on Smart Pricing</span>
            </label>
        </div>
        <div class="col-xs-12 col-sm-6 text-center">
            <a class="btn btn-sm btn-knockout btn-secondary" data-toggle="modal" data-target="#modal-smart-pricing">
                Preview Pricing
            </a>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-xs-12 text-right">
    <div class="save-progress">
        <a class="btn btn-secondary btn-save">Save</a>
    </div>
  </div>
</div>


<?php include "_calendar/december-pricing.php"; ?>