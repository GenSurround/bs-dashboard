<div class="modal fade" id="modal-requirements" tabindex="-1" role="dialog" aria-labelledby="captain-contact">
  <div class="modal-dialog" role="document">
    <div class="modal-content padding">
      <div class="modal-body">
        <h3 class="modal-title blue">Insurance Qualifications</h3>

        <p>Insurance company requires you to complete the following:</p>

        <table width="100%" class="font-size-12">
            <tr>
                <td>&nbsp;</td>
                <td align="center"><p>Yes</p></td>
                <td align="center"><p>No</p></td>
            </tr>
            <tr>
                <td><p>Any losses in the last 10 years (insured or not)?</p></td>
                <td><input type="radio" name="q1" value="yes" class="bs-radio" id="q1-yes"><label for="q1-yes"></label></td>
                <td><input type="radio" name="q1" value="no" class="bs-radio" id="q1-no"><label for="q1-no"></label></td>
            </tr>
            <tr>
                <td><p>Convicted of a criminal offense or pleaded "no contest" to a criminal action?</p></td>
                <td><input type="radio" name="q2" value="yes" class="bs-radio" id="q2-yes"><label for="q2-yes"></label></td>
                <td><input type="radio" name="q2" value="no" class="bs-radio" id="q2-no"><label for="q2-no"></label></td>
            </tr>
            <tr>
                <td><p>Insurance declined, canceled or non-renewed in the last five years?</p></td>
                <td><input type="radio" name="q3" value="yes" class="bs-radio" id="q3-yes"><label for="q3-yes"></label></td>
                <td><input type="radio" name="q3" value="no" class="bs-radio" id="q3-no"><label for="q3-no"></label></td>
            </tr>
            <tr>
                <td><p>More than two moving violations in the last five years?</p></td>
                <td><input type="radio" name="q4" value="yes" class="bs-radio" id="q4-yes"><label for="q4-yes"></label></td>
                <td><input type="radio" name="q4" value="no" class="bs-radio" id="q4-no"><label for="q4-no"></label></td>
            </tr>
            <tr>
                <td><p>Driver's license suspended or revoked?</p></td>
                <td><input type="radio" name="q5" value="yes" class="bs-radio" id="q5-yes"><label for="q5-yes"></label></td>
                <td><input type="radio" name="q5" value="no" class="bs-radio" id="q5-no"><label for="q5-no"></label></td>
            </tr>
        </table>
    </div>

      <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary">Send</button>
      </div>
    </div>
  </div>
</div>