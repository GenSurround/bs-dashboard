<div class="dashboard-section section-boat-managers">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_boat-managers"></i>
        Boat Managers</h2>
        <p class="intro-text">Please select those who will have permission to assist with boat rentals (i.e. Check In/Check Out, talk to renters, etc). Your selected captain(s) can be authorized to be boat manager(s).</p>
    </div>

    <div class="boat-managers">
        <table>
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th class="text-center"><span>Accept/Deny <br>Trips</span></th>
                <th class="text-center"><span> Modify/Cancel <br>Trips</span></th>
                <th class="text-center"><span>Perform <br>Inspections</span></th>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <td class="name">Alvaro (you)</td>
                <td><span class="pending">Pending</span></td>
                <td class="text-center" data-header="Accept / Deny Trips">
                    <input id="check1" class="simple" type="checkbox">
                    <label for="check1" class="horizontal"></label>
                </td>
                <td class="text-center" data-header="Modify / Cancel Trips">
                    <input id="check2" class="simple" type="checkbox">
                    <label for="check2" class="horizontal"></label>
                </td>
                <td class="text-center" data-header="Perform / Inspections">
                    <input id="check3" class="simple" type="checkbox">
                    <label for="check3" class="horizontal"></label>
                </td>
                <td><p><a class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal-requirements">Get Approved</a></p></td>
            </tr>
            <tr>
                <td class="name">Roberto (Captain)</td>
                <td><span class="pending">Pending</span></td>
                <td class="text-center" data-header="Accept / Deny Trips">
                    <input id="check4" class="simple" type="checkbox">
                    <label for="check4" class="horizontal"></label>
                </td>
                <td class="text-center" data-header="Modify / Cancel Trips">
                    <input id="check5" class="simple" type="checkbox">
                    <label for="check5" class="horizontal"></label>
                </td>
                <td class="text-center" data-header="Perform / Inspections">
                    <input id="check6" class="simple" type="checkbox">
                    <label for="check6" class="horizontal"></label>
                </td>
                <td><p><a class="btn btn-secondary btn-sm" data-toggle="modal" 
                        data-target="#modal-terms">Send Request</a></p></td>
            </tr>
        </table>
        <p><a class="add-more" >Add More <i class="nc-icon-outline x075 arrows-1_double-right"></i></a></p>
    </div>
</div>

<div id="add-more-boat-managers" class="dashboard-section section-add-boat-managers">
    <div class="dashboard-section-header">
        <h3 class="intro-text">
        <i class="bs-icon-bs_add-boat-managers"></i>
        Add more Boat Managers</h3>
        <p class="intro-text">Please complete the following. If you need help finding managers for your boat <a href="">click here.</a></p>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <p>
                <input class="bs-inputs" type="text" placeholder="First Name">
            </p>
        </div>
        <div class="col-xs-6">
            <p>
                <input class="bs-inputs" type="text" placeholder="Last Name">
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <p>
                <input class="bs-inputs" type="mail" placeholder="Email Address">
            </p>
        </div>
        <div class="col-xs-6">
            <p>
                <input class="bs-inputs" type="tel" placeholder="Phone Number">
            </p>
        </div>
    </div>
    <div class="row permissions">
        <div class="col-xs-4">
            <input id="check210" class="simple" type="checkbox">
            <label for="check210" class="horizontal">
                <span>Accept / Deny Trips</span>
            </label></div>
        <div class="col-xs-4">
            <input id="check211" class="simple" type="checkbox">
            <label for="check211" class="horizontal">
                <span>Modify / Cancel Trips</span>
            </label></div>
        <div class="col-xs-4">
            <input id="check212" class="simple" type="checkbox">
            <label for="check212" class="horizontal">
                <span>Perform Inspections</span>
            </label></div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p class="margin-top-full"><!-- <a class="add-more" href="">Add More <i class="nc-icon-outline x075 arrows-1_double-right"></i></a> --></p>
        </div>
        <div class="col-xs-12">
            <p><a class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal-requirements">Submit</a></p>
        </div>

    </div>
</div>

<div class="row">
  <div class="col-xs-12 text-right">
    <div class="save-progress">
        <a class="btn btn-secondary btn-save">Save</a>
    </div>
  </div>
</div>


<!-- Modal -->
<?php include "_modals/insurance-qa.php"; ?> 

<!-- Modal -->
<div class="modal fade" id="modal-terms" tabindex="-1" role="dialog" aria-labelledby="term">
  <div class="modal-dialog" role="document">
    <div class="modal-content padding">
      <div class="modal-body">
        <h3 class="modal-title blue">Agreement</h3>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error tempora aliquam dolore perferendis eaque obcaecati, sint, numquam, odio magni fugit optio eum voluptatem iste, temporibus esse aperiam facere non illum.</p>

        <div class="row">
            <div class="col-xs-12">

                    
                <div class="scroll-box nano">
                    <div class="scroll-box-content nano-content">
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, velit voluptates fuga! Aliquam fugiat nam praesentium hic est voluptas, incidunt, esse quo doloribus tenetur dolore libero sed animi iusto, ullam ad numquam vero earum sit at expedita laudantium odit tempore minus. Ea est sit omnis explicabo natus ratione tenetur quam, quidem quia molestiae praesentium facilis. Commodi minima similique modi, ut rem blanditiis. Vitae hic obcaecati ex recusandae, quo dicta facere quod quos facilis id, porro accusamus minus! Quis similique cumque consequuntur! Quaerat ducimus tempore perspiciatis sunt omnis sequi facere maxime, placeat, sapiente minus ratione delectus libero magnam dolor ex reiciendis neque rerum temporibus error, vel aliquam! Accusantium cumque consequuntur distinctio ullam corrupti! Corrupti id quos facilis, sapiente sint est labore, velit nam, quod ratione maiores! Sint quisquam eveniet dolorem distinctio dolorum assumenda consequuntur sapiente corporis tempore cum laboriosam id doloribus ea facilis nihil qui optio, nemo odit rerum, ipsum doloremque dicta. Deleniti nostrum debitis voluptas accusamus quam deserunt. Dolore ea magni, impedit maiores sapiente eligendi perspiciatis dignissimos natus a illum deserunt officia temporibus molestias aliquam nihil voluptatum asperiores mollitia ad, maxime fugiat. A ad labore quod ea suscipit dolorem placeat maxime laborum aliquid neque! Laudantium earum quo et cupiditate voluptatum, voluptates eius, repellat rem. Perferendis aliquam earum dicta porro saepe sequi enim unde explicabo, nobis ut veniam laboriosam. Numquam iste consequatur, incidunt voluptatem impedit! Modi excepturi assumenda numquam error praesentium mollitia possimus aperiam! Omnis voluptate vero eum vitae soluta amet iusto quae expedita earum hic et eveniet cum odio aspernatur numquam rem iure officiis eaque ullam nesciunt, ipsam, quaerat! Cupiditate magni vitae numquam quisquam eius reprehenderit iste architecto facere omnis alias modi veritatis saepe quam, accusantium ad distinctio vero non libero voluptatum sit explicabo aspernatur, quae perspiciatis inventore. Fugit mollitia dicta alias, nisi iste eos, ducimus, culpa iure quod impedit aspernatur quas praesentium maiores beatae. A officiis cupiditate, voluptas. Cum vero praesentium asperiores modi nam, consectetur illum ut velit dicta dolor quibusdam. Libero laudantium optio voluptatibus harum architecto officia maxime provident est ad, aliquid pariatur error aut aperiam quaerat enim dolorum, beatae nobis inventore earum perspiciatis. Inventore amet, sequi ipsam mollitia necessitatibus. Quam minima nihil, ipsam odit blanditiis. Deleniti debitis ipsum rem, nesciunt quibusdam explicabo odit nisi autem asperiores ut, nostrum laborum omnis sint possimus nam ullam quasi saepe necessitatibus adipisci sunt provident? Doloremque officiis adipisci placeat nostrum, quo aut delectus ad eum saepe dolore deleniti totam veniam. Nam in dolor ipsum provident obcaecati temporibus, rerum aliquid debitis sequi fugit ipsam nulla ducimus eius eveniet repudiandae possimus, praesentium excepturi animi? Odit fugit rem maiores reprehenderit debitis eius veniam iste, vitae nihil. Ducimus alias provident aut ipsam magni ea voluptas molestias quas iste neque, sit, ipsum quod quam excepturi. Ipsum dignissimos laboriosam culpa, nulla saepe illum atque est magni unde delectus, deserunt enim id nisi, mollitia cupiditate laudantium eaque libero corrupti. Maxime quaerat ipsum aliquid. Dolorum maiores quae recusandae, quam magnam deleniti earum perspiciatis quia, adipisci odit unde repellat, obcaecati debitis, cumque ea molestiae officia dolor laudantium animi laborum. Repellat, cum.
                    </p>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

      <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary">I Agree</button>
      </div>
    </div>
  </div>
</div>