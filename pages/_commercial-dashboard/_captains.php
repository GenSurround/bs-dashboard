<div class="dashboard-section section-captains">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_captains"></i>
        Captains</h2>
        <p class="intro-text">Please select your Captains settings. Your assigned captain(s) can be authotized to manage your trips. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
    </div>
    <div class="row margin-top-double margin-bottom-double">
        <div class="col-xs-6 col-sm-6 text-center">
            <input id="i1" class="captain-select bs-checkbox bs-checkbox-lg" type="checkbox">
            <label for="i1">
                <i class="bs-icon-bs_helm1"></i><span> Optional</span>
                <span class="small">
                    Captains are <strong>Optional:</strong> <br> on my boat’s rentals.
                </span>
            </label>
        </div>
        <div class="col-xs-6 col-sm-6 text-center border-left-gray">
            
            <input id="i2" class="captain-select bs-checkbox bs-checkbox-lg" type="checkbox">
            <label for="i2">
                <i class="bs-icon-bs_captain-required"></i><span> Required</span>
                <span class="small">
                    Captains are <strong>Required:</strong> <br> on my boat’s rentals.
                </span>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-center">
            <p class="small">*Note: Renters will pay for captain’s fees.</p>
        </div>
    </div>
</div>
<div class="dashboard-section section-assign-captains no-border-bottom">
    <div class="dashboard-section-header">
        <h3 class="intro-text">
        <i class="bs-icon-bs_assign-captains"></i>
        Select Captains
        </h3>
    </div>
    
    <div class="row find-captain margin-top-half margin-bottom-full">
        <div class="col-xs-12 col-sm-6">
            <p class="bs-inputs-icon">
                <input class="bs-inputs bs-inputs-lg" type="text" placeholder="Search for a captain by name">
                <i class="bs-icon-bs_search"></i>
            </p>
            
        </div>
        <div class="col-xs-12 col-sm-6">
            <p class="text-right margin-top-half">
                <a class="add-more" data-toggle="modal" data-target="#modal-captain-contact">Suggest my own captain <i class="nc-icon-outline x075 arrows-1_double-right"></i></a>
            </p>
        </div>
    </div>
    
    <div class="row captain-list">
        
        <div class="captain col-xs-12">
            <div class="col-xs-12 col-sm-4">
                <div class="user-profile">
                    <span class="img-wrapper">
                        <img src="images/placeholders/cap1.jpg" >
                    </span>
                    <span class="user-details">
                        <span class="user-name">
                            Pedro Manuel
                        </span>
                        <span class="user-rating">
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                        </span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="row text-center">
                    <div class="captain-feature">
                        <i class="bs-icon-bs_location"></i>
                        <span class="captain-feature-name">Location</span>
                        <span class="captain-feature-value">Miami, FL</span>
                    </div>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_experience"></i>
                        <span class="captain-feature-name">Experience</span>
                        <span class="captain-feature-value">11 Trips</span>
                    </div>
                    <br class="only-xxs"/>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_response"></i>
                        <span class="captain-feature-name">Avg Resp</span>
                        <span class="captain-feature-value">23 min</span>
                    </div>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_boat"></i>
                        <span class="captain-feature-name">Assigned</span>
                        <span class="captain-feature-value">10 Boats</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 text-right">
                <p class="margin-top-full">
                    <a href="" class="btn btn-knockout btn-secondary btn-gray btn-sm">Select</a>
                    <a href="" class="btn btn-knockout btn-secondary btn-sm">Message</a>
                </p>
            </div>
        </div>
        <div class="captain col-xs-12">
            <div class="col-xs-12 col-sm-4">
                <div class="user-profile">
                    <span class="img-wrapper">
                        <img src="images/placeholders/cap1.jpg" >
                    </span>
                    <span class="user-details">
                        <span class="user-name">
                            Pedro Manuel
                        </span>
                        <span class="user-rating">
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                        </span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="row text-center">
                    <div class="captain-feature">
                        <i class="bs-icon-bs_location"></i>
                        <span class="captain-feature-name">Location</span>
                        <span class="captain-feature-value">Miami, FL</span>
                    </div>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_experience"></i>
                        <span class="captain-feature-name">Experience</span>
                        <span class="captain-feature-value">11 Trips</span>
                    </div>
                    <br class="only-xxs"/>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_response"></i>
                        <span class="captain-feature-name">Avg Resp</span>
                        <span class="captain-feature-value">23 min</span>
                    </div>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_boat"></i>
                        <span class="captain-feature-name">Assigned</span>
                        <span class="captain-feature-value">10 Boats</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 text-right">
                <p class="margin-top-full">
                    <a href="" class="btn btn-knockout btn-secondary btn-gray btn-sm">Requested</a>
                    <a href="" class="btn btn-knockout btn-secondary btn-sm">Message</a>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="dashboard-section section-captains-requests no-border-bottom margin-top-double">
    <div class="dashboard-section-header">
        <h3 class="intro-text">
        <i class="bs-icon-bs_captain-request"></i>
        Captain Requests
        </h3>
        <p class="intro-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque quasi sapiente repellat minima numquam aliquam, in temporibus, tempora ratione! Perspiciatis?</p>
    </div>
    <div class="row captain-list">
        
        <div class="captain col-xs-12">
            <div class="col-xs-12 col-sm-4">
                <div class="user-profile">
                    <span class="img-wrapper">
                        <img src="images/placeholders/cap1.jpg" >
                    </span>
                    <span class="user-details">
                        <span class="user-name">
                            Pedro Manuel
                        </span>
                        <span class="user-rating">
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                        </span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="row text-center">
                    <div class="captain-feature">
                        <i class="bs-icon-bs_location"></i>
                        <span class="captain-feature-name">Location</span>
                        <span class="captain-feature-value">Miami, FL</span>
                    </div>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_experience"></i>
                        <span class="captain-feature-name">Experience</span>
                        <span class="captain-feature-value">11 Trips</span>
                    </div>
                    <br class="only-xxs"/>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_response"></i>
                        <span class="captain-feature-name">Avg Resp</span>
                        <span class="captain-feature-value">23 min</span>
                    </div>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_boat"></i>
                        <span class="captain-feature-name">Assigned</span>
                        <span class="captain-feature-value">10 Boats</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 text-right">
                <p class="margin-top-full">
                    <a href="" class="btn btn-knockout btn-secondary btn-sm">Ignore</a>
                    <a href="" class="btn btn-primary btn-gray btn-sm">Accept</a>
                </p>
            </div>
        </div>
        <div class="captain col-xs-12">
            <div class="col-xs-12 col-sm-4">
                <div class="user-profile">
                    <span class="img-wrapper">
                        <img src="images/placeholders/cap1.jpg" >
                    </span>
                    <span class="user-details">
                        <span class="user-name">
                            Pedro Manuel
                        </span>
                        <span class="user-rating">
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                        </span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="row text-center">
                    <div class="captain-feature">
                        <i class="bs-icon-bs_location"></i>
                        <span class="captain-feature-name">Location</span>
                        <span class="captain-feature-value">Miami, FL</span>
                    </div>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_experience"></i>
                        <span class="captain-feature-name">Experience</span>
                        <span class="captain-feature-value">11 Trips</span>
                    </div>
                    <br class="only-xxs"/>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_response"></i>
                        <span class="captain-feature-name">Avg Resp</span>
                        <span class="captain-feature-value">23 min</span>
                    </div>
                    <div class="captain-feature">
                        <i class="bs-icon-bs_boat"></i>
                        <span class="captain-feature-name">Assigned</span>
                        <span class="captain-feature-value">10 Boats</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 text-right">
                <p class="margin-top-full">
                    <a href="" class="btn btn-knockout btn-secondary btn-sm">Ignore</a>
                    <a href="" class="btn btn-primary btn-gray btn-sm">Accept</a>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 text-right">
        <div class="save-progress">
            <a class="btn btn-secondary btn-save">Save</a>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-captain-notice" tabindex="-1" role="dialog" aria-labelledby="captain-notice">
    <div class="modal-dialog" role="document">
        <div class="modal-content padding">
            <div class="modal-body">
                <p>
                    Our insurance policy requires that you have a minimum of two years of experience, or twenty hours operating during the last two years, vessels of a similar length (25 foot or larger), similar speed capabilities (approximately 36MPH).
                </p>
                <p><strong>Important Note:</strong> Due to regulations you can not manage rentals with more than 6 passengers if you are the captain.  </p>
                <p class="text-center margin-bottom-none">
                    <a id="aye-aye" class="btn btn-secondary">Yes, I have what it takes</a>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-captain-contact" tabindex="-1" role="dialog" aria-labelledby="captain-contact">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content padding">
            <div class="modal-body">
                <h2 class="modal-title blue text-center">Associate my own Captain</h2>
                <p class=" margin-bottom-full">Please provide contact info of your captain(s). Each person must complete the insurance qualification process and provide coats guard documentation.</p>
                <div class="row flex-row">
                    <div class="col-xs-12 col-sm-3">
                        <div class="row text-center">
                            <div class="col-xs-12">
                                <div class="">
                                    <img src="images/dashboard/captain-profile.png" class="profile-photo">
                                    <br>
                                    <span>Upload  profile photo or drag and drop it here</span>
                                </div>
                            </div>
                        </div>
                        <div class="row push-bottom">
                            <div class="col-xs-12">
                                <p class="">
                                    <button  type="button" class="btn btn-secondary btn-knockout btn-block"><i class="bs-icon-bs_pics-videos"></i> Upload Photo</button>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="row">
                            <div class="col-xs-6">
                                <p>
                                    <input class="bs-inputs" type="text" placeholder="First Name">
                                </p>
                            </div>
                            <div class="col-xs-6">
                                <p>
                                    <input class="bs-inputs" type="text" placeholder="Last Name">
                                </p>
                            </div>
                            <div class="col-xs-6">
                                <p>
                                    <input class="bs-inputs" type="mail" placeholder="Email Address">
                                </p>
                            </div>
                            <div class="col-xs-6">
                                <p>
                                    <input class="bs-inputs" type="tel" placeholder="Phone Number (Mobile)">
                                </p>
                            </div>
                            <div class="col-xs-6">
                                <p>
                                    <input class="bs-inputs" type="mail" placeholder="USCG Ref #">
                                </p>
                            </div>
                            <div class="col-xs-6">
                                <p>
                                    <input class="bs-inputs" type="tel" placeholder="Other Credentials">
                                </p>
                            </div>
                        </div>
                        <div class="row margin-top-full">
                            <div class="col-xs-12 col-sm-6">
                                <p class="text-left">
                                    <button  type="button" class="btn btn-secondary btn-knockout btn-normal"><i class="bs-icon-bs_licence"></i> Upload License Copy</button>
                                </p>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <p class="text-right">
                                    <button  type="button" class="btn btn-secondary  btn-normal">Send</button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<?php include "_modals/insurance-qa.php"; ?>