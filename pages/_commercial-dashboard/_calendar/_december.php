<div class="bs-calendar-grid">
    <div class="table-row table-header">
        <div class="table-cell">SUN</div>
        <div class="table-cell">MON</div>
        <div class="table-cell">TUE</div>
        <div class="table-cell">WED</div>
        <div class="table-cell">THU</div>
        <div class="table-cell">FRI</div>
        <div class="table-cell">SAT</div>
    </div>
    <div class="table-row">
        <div class="table-cell"></div>
        <div class="table-cell"></div>
        <div class="table-cell"></div>
        <div class="table-cell"></div>

        <div class="table-cell date" data-date='{"day":"1", "month":"10", "year":"2016"}'><span class="day-number">1</span></div>
        <div class="table-cell date" data-date='{"day":"2", "month":"10", "year":"2016"}'><span class="day-number">2</span></div>
        <div class="table-cell date" data-date='{"day":"3", "month":"10", "year":"2016"}'><span class="day-number">3</span>
        </div>
        </div>
        <div class="table-row">
        <div class="table-cell date" data-date='{"day":"4", "month":"10", "year":"2016"}'><span class="day-number">4</span></div>
        <div class="table-cell date" data-date='{"day":"5", "month":"10", "year":"2016"}'><span class="day-number">5</span></div>
        <div class="table-cell date" data-date='{"day":"6", "month":"10", "year":"2016"}'><span class="day-number">6</span></div>
        <div class="table-cell date" data-date='{"day":"7", "month":"10", "year":"2016"}'><span class="day-number">7</span></div>

        <div class="table-cell date" data-date='{"day":"8", "month":"10", "year":"2016"}'><span class="day-number">8</span></div>
        <div class="table-cell date" data-date='{"day":"9", "month":"10", "year":"2016"}'><span class="day-number">9</span></div>
        <div class="table-cell date" data-date='{"day":"10", "month":"10", "year":"2016"}'><span class="day-number">10</span></div>
        </div>
        <div class="table-row">
        <div class="table-cell date" data-date='{"day":"11", "month":"10", "year":"2016"}'><span class="day-number">11</span>
            <div class="trips">
            </div>
        </div>
        <div class="table-cell date" data-date='{"day":"12", "month":"10", "year":"2016"}'><span class="day-number">12</span></div>
        <div class="table-cell date" data-date='{"day":"13", "month":"10", "year":"2016"}'><span class="day-number">13</span></div>
        <div class="table-cell date" data-date='{"day":"14", "month":"10", "year":"2016"}'><span class="day-number">14</span>
            <div class="trips">
                <div id="trip-0001-trigger" data-source="#trip-0001-tooltipster" class="trip cancelled tooltipster">
                    <img width="20" height="20" src="images/bs-calendar/bs_cancelled.svg" alt="cancelled">
                    <span class="ref"><span class="code">#0001</span></span>
                </div>
                <div id="trip-0002-trigger" data-source="#trip-0002-tooltipster" class="trip completed tooltipster"">
                    <img width="20" height="20" src="images/bs-calendar/bs_completed.svg" alt="completed">
                    <span class="ref"><span class="code">#0002</span></span>
                </div>
                <div id="trip-0003-trigger" data-source="#trip-0003-tooltipster" class="trip pending tooltipster">
                    <img width="20" height="20" src="images/bs-calendar/bs_pending.svg" alt="pending">
                    <span class="ref"><span class="code">#0003</span></span>
                </div>
                <div id="trip-0004-trigger" data-source="#trip-0004-tooltipster" class="trip upcoming tooltipster">
                    <img width="20" height="20" src="images/bs-calendar/bs_upcoming.svg" alt="upcoming">
                    <span class="ref"><span class="code">#0004</span></span>
                </div>
            </div>
        </div>

        <div class="table-cell date" data-date='{"day":"15", "month":"10", "year":"2016"}'><span class="day-number">15</span></div>
        <div class="table-cell date" data-date='{"day":"16", "month":"10", "year":"2016"}'><span class="day-number">16</span></div>
        <div class="table-cell date" data-date='{"day":"17", "month":"10", "year":"2016"}'><span class="day-number">17</span>
            <div class="trips">

            </div>
        </div>
        </div>
        <div class="table-row">
        <div class="table-cell date" data-date='{"day":"18", "month":"10", "year":"2016"}'><span class="day-number">18</span></div>
        <div class="table-cell date" data-date='{"day":"19", "month":"10", "year":"2016"}'><span class="day-number">19</span></div>
        <div class="table-cell date" data-date='{"day":"20", "month":"10", "year":"2016"}'><span class="day-number">20</span></div>
        <div class="table-cell date" data-date='{"day":"21", "month":"10", "year":"2016"}'><span class="day-number">21</span></div>

        <div class="table-cell date" data-date='{"day":"22", "month":"10", "year":"2016"}'><span class="day-number">22</span></div>
        <div class="table-cell date" data-date='{"day":"23", "month":"10", "year":"2016"}'><span class="day-number">23</span></div>
        <div class="table-cell date" data-date='{"day":"24", "month":"10", "year":"2016"}'><span class="day-number">24</span></div>
        </div>
        <div class="table-row">
        <div class="table-cell date" data-date='{"day":"25", "month":"10", "year":"2016"}'><span class="day-number">25</span></div>
        <div class="table-cell date" data-date='{"day":"26", "month":"10", "year":"2016"}'><span class="day-number">26</span></div>
        <div class="table-cell date" data-date='{"day":"27", "month":"10", "year":"2016"}'><span class="day-number">27</span></div>
        <div class="table-cell date" data-date='{"day":"28", "month":"10", "year":"2016"}'><span class="day-number">28</span></div>

        <div class="table-cell date" data-date='{"day":"29", "month":"10", "year":"2016"}'><span class="day-number">29</span></div>
        <div class="table-cell date" data-date='{"day":"30", "month":"10", "year":"2016"}'><span class="day-number">30</span></div>
        <div class="table-cell date" data-date='{"day":"31", "month":"10", "year":"2016"}'><span class="day-number">31</span></div>
    </div>
</div>

<div id="day-trips" class="day-trips" >

<?php include "../../../pages/_owner-dashboard/_trips-tooltips/_cancelled.php"; ?>
<?php include "../../../pages/_owner-dashboard/_trips-tooltips/_completed.php"; ?>
<?php include "../../../pages/_owner-dashboard/_trips-tooltips/_pending.php"; ?>
<?php include "../../../pages/_owner-dashboard/_trips-tooltips/_upcoming.php"; ?>

</div>