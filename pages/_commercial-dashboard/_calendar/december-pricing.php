<!-- Modal -->
<div class="modal mobile-modal fade" id="modal-smart-pricing" tabindex="-1" role="dialog" aria-labelledby="smart-pricing">
  <div class="modal-dialog" role="document">
    <div class="modal-content padding">
      <div class="modal-header">
<!--         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h2 class="modal-title text-center"> <i class="bs-icon-bs_smart-pricing"></i> Smart Pricing Overview</h2>
      </div>
      <div class="modal-body">

          <div class="bs-calendar smart-pricing">
              
              <div class="bs-calendar-header">
                      <h1>October 2016</h1>
                  <div class="bs-calendar-nav">
                      <a class="prev">
                          <i class="bs-icon-bs_left-arrow blue"></i> <span class="long">September</span>
                          <span class="short">Sep</span>
                      </a>
                      <a class="next">
                          <span class="long">November</span>
                          <span class="short">Nov</span> <i class="bs-icon-bs_right-arrow blue"></i> 
                      </a>
                  </div>
              </div>

              <div class="bs-calendar-selected-month">
                  <div class="bs-calendar-grid">
                      <div class="table-row table-header">
                          <div class="table-cell">SUN</div>
                          <div class="table-cell">MON</div>
                          <div class="table-cell">TUE</div>
                          <div class="table-cell">WED</div>
                          <div class="table-cell">THU</div>
                          <div class="table-cell">FRI</div>
                          <div class="table-cell">SAT</div>
                      </div>
                      <div class="table-row">
                          <div class="table-cell"><span class="day-number">1</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div></div>
                          <div class="table-cell"><span class="day-number">2</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div></div>
                          <div class="table-cell">
                              <span class="day-number">3</span>
                              <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell">
                              <span class="day-number">4</span>
                              <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">5</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">6</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">7</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                      </div>
                      <div class="table-row">
                          <div class="table-cell"><span class="day-number">8</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">9</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">10</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">11</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">12</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">13</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">14</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                      </div>
                      <div class="table-row">
                          <div class="table-cell"><span class="day-number">15</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">16</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">17</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">18</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">19</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">20</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">21</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                      </div>
                      <div class="table-row">
                          <div class="table-cell"><span class="day-number">22</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">23</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">24</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">25</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">26</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">27</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">28</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                      </div>
                      <div class="table-row">
                          <div class="table-cell"><span class="day-number">29</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">30</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"><span class="day-number">31</span>
                          <div class="amounts">
                                  <span class="half-day"><i class="bs-icon-bs_half-day"></i> <span class="amount">$1500</span></span>
                                  <span class="full-day"><i class="bs-icon-bs_full-day"></i> <span class="amount">$1500</span></span>
                              </div>
                          </div>
                          <div class="table-cell"></div>
                          <div class="table-cell"></div>
                          <div class="table-cell"></div>
                          <div class="table-cell"></div>
                      </div>
                  </div>      
              </div>

              <div class="legend">
                  <span><i class="bs-icon-bs_half-day"></i> Half Day</span>
                  <span><i class="bs-icon-bs_full-day"></i> Full Day</span>
              </div>

          </div>
      </div>

      <!-- <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary">Send</button>
      </div> -->
    </div>
  </div>
</div>