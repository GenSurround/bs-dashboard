<div class="dashboard-section section-insurance">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_my_insurance blue"></i>
        My Insurance</h2>
        <p class="intro-text">Please select your Insurance Plan. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
    </div>
    <div class="row indented-content">
        <div class="col-xs-12 col-sm-6">
            <p>
                <input id="insurance-company" class="bs-inputs" type="text" placeholder="Insurance Company">
            </p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p>
                <input id="policy-number" class="bs-inputs" type="text" placeholder="Policy #">
            </p>
        </div>
        <div class="col-xs-12 col-xs-6">
            <p class="margin-top-full"><a class="btn btn-secondary btn-knockout btn-block"><i class="bs-icon-bs_insurance_copy"></i>  Upload Insurance Copy</a></p>
        </div>
        <div class="col-xs-12 col-xs-6">
            <p class="margin-top-full"><a class="btn btn-secondary btn-block " data-toggle="modal" data-target="#modal-we-will-contact-you">Save</a></p>
        </div>
    </div>
</div>


<div class="dashboard-section section-insurance">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_insurance blue"></i>
        BS Insurance</h2>
        <p class="intro-text">Choose from two tiers of coverage for your boat rentals: select premium for more peace of mind or basic to increase your earnings.</p>
    </div>


    <div class="row">
        <div class="col-xs-12 text-center">
            <div class="insurance-plans ">
                <h2 class="text-center">
                    <i class="bs-icon-bs_premium"></i>
                    Premium
                </h2>
                <ul>
                    <li>Earn 21% of your rentals</li>
                    <li>lit ament, consectetur adipis</li>
                    <li>Lorem ipsum dolor.</li>
                    <li>lit amer conasdoka</li>
                    <li>lorepansd apsjdas</li>
                    <li>asdlkasd</li>
                </ul>
                <p class="selection">
                    <a class="btn btn-knockout btn-secondary">Select</a>
                </p>
            </div>
            <div class="insurance-plans">
                <h2 class="text-center">
                    <i class="bs-icon-bs_basic-insurance"></i>
                    Basic
                </h2>
                <ul>
                    <li>Earn 21% of your rentals</li>
                    <li>lit ament, consectetur adipis</li>
                    <li>Lorem ipsum dolor.</li>
                </ul>
                <p class="selection">
                    <a class="btn btn-knockout btn-secondary">Select</a>
                </p>
            </div>
            <hr>
            <p class="own-insurance tooltipster"  title="Please call XYZ for ABC">
                <input id="check0" type="checkbox">
                <label for="check0" class="horizontal">
                    <span>I’d like to use my own insurance.</span>
                </label>
            </p>

            <p class="note small margin-top-double">

                *Note: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, neque.
            </p>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-xs-12 text-right">
    <div class="save-progress">
        <a class="btn btn-secondary btn-save">Save</a>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-we-will-contact-you" tabindex="-1" role="dialog" aria-labelledby="captain-contact">
  <div class="modal-dialog " role="document">
    <div class="modal-content padding">
      <div class="modal-body">
        <h3 class="modal-title blue">A BS Representative will contact you soon!</h3>
            <p>
                We will contact you to get you started on your new insurance plan. <br>If you have other questions, please contact BoatSetter Customer Service by calling <a class="strong" href="tel:(844) 232-8738">(844) 232-8738</a> or by email<a class="strong" href="mailto:contact@boatsetter.com"> contact@boatsetter.com</a>
            </p>
        </div>

    </div>
  </div>
</div>