<div class="dashboard-section section-bs-bucks">
	<div class="dashboard-section-header">
		<h2>
		<img width="187" height="52" src="images/dashboard/bs-bucks.svg" alt="">
		</h2>
	</div>
	<div class="row border-top-bottom margin-top-double">
		<div class="col-xs-12 col-sm-4 margin-top-full">
			<p class="text-center">
				Total <br>
				<span class="price big">
					<i class="bs-icon-bucks_wallet bs-icon-32 blue"></i>
				2,000</span>
			</p>
		</div>
		<div class="col-xs-12 col-sm-4 margin-top-full">
			<p class="text-center">
				This Month <br>
				<span class="price big">
					<i class="bs-icon-bucks_month-icon bs-icon-32 blue"></i>
				400</span>
			</p>
		</div>
		<div class="col-xs-12 col-sm-4 margin-top-full">
			<p class="text-center">
				Lifetime <br>
				<span class="price big">
					<i class="bs-icon-bucks_lifetime bs-icon-32 blue"></i>
				4,300</span>
			</p>
		</div>
	</div>
	<div class="row margin-top-double">
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<div class="row">
				<div class="col-xs-6">
					<p>
						<label class="bs-select small margin-bottom-none">Filter from</label>
						<span class="bs-datepicker">
							<input id="filter-from" class="bs-inputs short"  type="text" placeholder="01/10/2017">
						</span>
					</p>
				</div>
				<div class="col-xs-6">
					<p>
						<label class="bs-select small margin-bottom-none">To</label>
						<span class="bs-datepicker">
							<input id="filter-to" class="bs-inputs short"  type="text" placeholder="01/10/2017">
						</span>
					</p>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-lg-4 col-lg-push-4">
			<div class="row">
				<div class="col-xs-6">
					<p>		
						<label class="bs-select small margin-bottom-none">&nbsp;</label>		

						<select class="cs-select no-icon cs-skin-underline">
							<option value="1">
							<i class="bs-icon-bucks_lifetime bs-icon-32 blue"></i> 
							Sort by date</option>
						</select>
					</p>
				</div>
				<div class="col-xs-6">
					<label class="bs-select small margin-bottom-none">&nbsp;</label>
					<p class="input-like">
						<a> <i class="bs-icon-export blue"></i> Export as CSV</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<table class="bs-table margin-top-full">
		<tr class="bs-table-header row">
			<th class="col-xs-6">BS Bucks</th>
			<th class="col-xs-6">Expires</th>
		</tr>
		<tr class="row">
			<td class="col-xs-6">100</td>
			<td class="col-xs-6">05/20/2015</td>
		</tr>
		<tr class="row">
			<td class="col-xs-6">100</td>
			<td class="col-xs-6">05/20/2015</td>
		</tr>
		<tr class="row">
			<td class="col-xs-6">100</td>
			<td class="col-xs-6">05/20/2015</td>
		</tr>
		<tr class="row">
			<td class="col-xs-6">100</td>
			<td class="col-xs-6">05/20/2015</td>
		</tr>
	</table>
	
</div>
<div class="dashboard-section section-bs-bucks margin-top-double">
	<div class="dashboard-section-header">
		<h3 class="intro-text">
		<i class="bs-icon-bucks_redeem"></i>
		Redeem your BS Bucks</h3>
		<p></p>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-4 text-center">
			<h3 class="bigger blue">
				BS experience and boat rentals
			</h3>
			<p class="img-wrapper border-radius o-h">
				<img src="images/placeholders/boat2.jpg" class="img-responsive">
			</p>
			<a class="btn btn-secondary btn-normal">Book</a>
			<p></p>
		</div>
		<div class="col-xs-12 col-sm-4 text-center">
			<h3 class="bigger blue">
				Share your BS Bucks with a friend
			</h3>
			<p class="img-wrapper border-radius o-h">
				<img src="images/placeholders/couple1.jpg" class="img-responsive">
			</p>
			<a class="btn btn-secondary btn-normal">Gift</a>
			<p></p>
		</div>
		<div class="col-xs-12 col-sm-4 text-center">
			<h3 class="bigger blue">
				Order BS Swag and have it delivered

			</h3>
			<p class="img-wrapper border-radius o-h">
				<img src="images/placeholders/swag1.jpg" class="img-responsive">
			</p>
			<a class="btn btn-secondary btn-normal">Get Swag</a>
			<p></p>
		</div>
	</div>
</div>