<div class="row indented-content">
	<div class="col-xs-12 col-sm-6">
		<p>
		Next Payment Date: <strong>10/31/2017</strong> <br>
		Set up your direct deposit to start receiving <br>
		payments in your account. <i class="bs-icon-bs_warning red"></i>
		</p>
	 </div>
	<div class="col-xs-12 col-sm-6 text-center">
		<a class="btn btn-secondary btn-knockout" data-toggle="modal" data-target="#direct-deposit">
			Direct Deposit Set Up
		</a>
		&nbsp;
		<a class="btn btn-secondary">
			Get Paid Now
		</a>
	</div>
</div>
<div class="row margin-top-full">
	
	<div class="col-xs-12 col-sm-6 col-lg-4">
		<div class="row">
			<div class="col-xs-6">
				<p>
					<label class="bs-select small margin-bottom-none">Filter from</label>
					<span class="bs-datepicker">
						<input id="filter-from" class="bs-inputs short"  type="text" placeholder="01/10/2017">
					</span>
				</p>
			</div>
			<div class="col-xs-6">
				<p>
					<label class="bs-select small margin-bottom-none">To</label>
					<span class="bs-datepicker">
						<input id="filter-to" class="bs-inputs short"  type="text" placeholder="01/10/2017">
					</span>
				</p>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-lg-4 col-lg-push-4">
		<div class="row">
			<div class="col-xs-6">
				<p>		
					<label class="bs-select small margin-bottom-none">&nbsp;</label>		

					<select class="cs-select no-icon cs-skin-underline">
						<option value="1">
						<i class="bs-icon-bucks_lifetime bs-icon-32 blue"></i> 
						Sort by date</option>
					</select>
				</p>
			</div>
			<div class="col-xs-6">
				<label class="bs-select small margin-bottom-none">&nbsp;</label>
				<p class="input-like">
					<a> <i class="bs-icon-export blue"></i> Export as CSV</a>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xs-12">
		<table class="bs-table margin-top-full">
			<tr class="bs-table-header row">
				<th class="col-xs-3">Trip ID</th>
				<th class="col-xs-3">Date</th>
				<th class="col-xs-3">Amount $</th>
				<th class="col-xs-3">BS Bucks</th>
			</tr>
			<tr class="row">
				<td class="col-xs-3"><a href="">639</a></td>
				<td class="col-xs-3">05/20/2015</td>
				<td class="col-xs-3">$500</td>
				<td class="col-xs-3">100</td>
			</tr>
			<tr class="row">
				<td class="col-xs-3"><a href="">649</a></td>
				<td class="col-xs-3">05/20/2015</td>
				<td class="col-xs-3">$500</td>
				<td class="col-xs-3">100</td>
			</tr>
			<tr class="row">
				<td class="col-xs-3"><a href="">685</a></td>
				<td class="col-xs-3">05/20/2015</td>
				<td class="col-xs-3">$500</td>
				<td class="col-xs-3">100</td>
			</tr>
			<tr class="row">
				<td class="col-xs-3"><a href="">681</a></td>
				<td class="col-xs-3">05/20/2015</td>
				<td class="col-xs-3">$500</td>
				<td class="col-xs-3">100</td>
			</tr>
		</table>
	</div>
	
</div>


<!-- Modal -->
<div class="modal mobile-modal fade" id="direct-deposit" tabindex="-1" role="dialog" aria-labelledby="smart-pricing">
  <div class="modal-dialog" role="document">
    <div class="modal-content padding">
      <div class="modal-header">

        <h2 class="modal-title blue"> Direct Deposit (ACH) Settings</h2>
        <p>Your earnings will be deposited directly into your account within six days of the end of the trip.</p>

      </div>

      <div class="modal-body">
      	<div id="payment-account-info-edit" class="">
      	            <div class="row">

      	            	<div class="col-xs-12"> 
      	            	<p>
      	            	    <span class="margin-right-full">
      	            	        <input name="wat" id="radio1" class="simple" type="radio">
      	            	        <label for="radio1" class="horizontal">Checking</label>
      	            	    </span>

      	            	    <span class="margin-right-full">
      	            	        <input name="wat" id="radio2" class="simple" type="radio">
      	            	        <label for="radio2" class="horizontal">Savings</label>
      	            	    </span>
      	            	</p>  

      	            	</div>
      	                <div class="col-xs-12">
      	                    <p class="with-number" data-number="1">
      	                        <label for="Holder Name" class="sr-only">Holder Name</label>
      	                        <input id="Holder Name" class="bs-inputs" type="text" placeholder="Account Holder Name (as shown in your cheque book)">
      	                    </p>
      	                </div>

      	                <div class="col-xs-12 col-sm-6">
      	                    <p class="with-number" data-number="2">
      	                        <label for="Holder Name" class="sr-only">Routing Number</label>
      	                        <input id="Holder Name" class="bs-inputs" type="text" placeholder="Routing Number">
      	                    </p>
      	                </div>

      	                <div class="col-xs-12 col-sm-6">
      	                    <p class="with-number" data-number="3">
      	                        <label for="Holder Name" class="sr-only">Account Number</label>
      	                        <input id="Holder Name" class="bs-inputs" type="text" placeholder="Account Number">
      	                    </p>
      	                </div>

      	                <div class="col-xs-12 col-sm-6">
       

      	                    <p>
      	                        <span class="mobile-block">
      	                            <input name="wat" id="check2" class="simple" type="checkbox">
      	                            <label for="check2" class="horizontal">Apply to all my boats.</label>
      	                        </span>


      	                    </p>

	  	                </div>

	  	                <div class="col-xs-12 col-sm-6 text-right">
	  	                	<div class="btn btn-secondary btn-knockout">Cancel</div>
	  	                	<div class="btn btn-secondary">Save</div>
	  	                </div>
  	                    <div class="col-xs-12">
  	                        <p class="text-center margin-top-full">
  	                            <img width="400" src="images/dashboard/check-example.png" class="img-responsive">
  	                        </p>
  	                    </div>       
      	            </div>
      	        </div>
      </div>
      
    </div>
  </div>
</div>