<div class="row margin-top-full">
	
	<div class="col-xs-12 col-sm-6 col-lg-4">
		<div class="row">
			<div class="col-xs-6">
				<p>
					<label class="bs-select small margin-bottom-none">Filter from</label>
					<span class="bs-datepicker">
						<input id="filter-from" class="bs-inputs short"  type="text" placeholder="01/10/2017">
					</span>
				</p>
			</div>
			<div class="col-xs-6">
				<p>
					<label class="bs-select small margin-bottom-none">To</label>
					<span class="bs-datepicker">
						<input id="filter-to" class="bs-inputs short"  type="text" placeholder="01/10/2017">
					</span>
				</p>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-lg-4 col-lg-push-4">
		<div class="row">
			<div class="col-xs-6">
				<p>		
					<label class="bs-select small margin-bottom-none">&nbsp;</label>		

					<select class="cs-select no-icon cs-skin-underline">
						<option value="1">
						<i class="bs-icon-bucks_lifetime bs-icon-32 blue"></i> 
						Sort by date</option>
					</select>
				</p>
			</div>
			<div class="col-xs-6">
				<label class="bs-select small margin-bottom-none">&nbsp;</label>
				<p class="input-like">
					<a> <i class="bs-icon-export blue"></i> Export as CSV</a>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xs-12">
		<table class="bs-table margin-top-full">
			<tr class="bs-table-header row">
				<th class="col-xs-3">Trip ID</th>
				<th class="col-xs-3">Date</th>
				<th class="col-xs-3">Amount $</th>
				<th class="col-xs-3">Status</th>
			</tr>
			<tr class="row">
				<td class="col-xs-3"><a href="">639</a></td>
				<td class="col-xs-3">05/20/2015</td>
				<td class="col-xs-3">$500</td>
				<td class="col-xs-3">Pending</td>
			</tr>
			<tr class="row">
				<td class="col-xs-3"><a href="">649</a></td>
				<td class="col-xs-3">05/20/2015</td>
				<td class="col-xs-3">$500</td>
				<td class="col-xs-3">Pending</td>
			</tr>
			<tr class="row">
				<td class="col-xs-3"><a href="">685</a></td>
				<td class="col-xs-3">05/20/2015</td>
				<td class="col-xs-3">$500</td>
				<td class="col-xs-3">Paid</td>
			</tr>
			<tr class="row">
				<td class="col-xs-3"><a href="">681</a></td>
				<td class="col-xs-3">05/20/2015</td>
				<td class="col-xs-3">$500</td>
				<td class="col-xs-3">Paid</td>
			</tr>
		</table>
	</div>
	
</div>