<div class="row indented-content">
	<div class="col-xs-12 col-sm-6">
		<p>
		Next Payment Date: <strong>10/31/2017</strong> <br>
		Direct Deposit Account *****1234 &nbsp; <a href="">Update</a>
		</p>
	 </div>
	<div class="col-xs-12 col-sm-6 text-center">
		<a class="btn btn-secondary">
			Get Paid Now
		</a>
	</div>
</div>

<div class="row margin-top-full">
	
	<div class="col-xs-12 col-sm-6 col-lg-4">
		<div class="row">
			<div class="col-xs-6">
				<p>
					<label class="bs-select small margin-bottom-none">Filter from</label>
					<span class="bs-datepicker">
						<input id="filter-from" class="bs-inputs short"  type="text" placeholder="01/10/2017">
					</span>
				</p>
			</div>
			<div class="col-xs-6">
				<p>
					<label class="bs-select small margin-bottom-none">To</label>
					<span class="bs-datepicker">
						<input id="filter-to" class="bs-inputs short"  type="text" placeholder="01/10/2017">
					</span>
				</p>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-lg-4 col-lg-push-4">
		<div class="row">
			<div class="col-xs-6">
				<p>		
					<label class="bs-select small margin-bottom-none">&nbsp;</label>		

					<select class="cs-select no-icon cs-skin-underline">
						<option value="1">
						<i class="bs-icon-bucks_lifetime bs-icon-32 blue"></i> 
						Sort by date</option>
					</select>
				</p>
			</div>
			<div class="col-xs-6">
				<label class="bs-select small margin-bottom-none">&nbsp;</label>
				<p class="input-like">
					<a> <i class="bs-icon-export blue"></i> Export as CSV</a>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xs-12">
		<table class="bs-table">
			<tr class="bs-table-header">
				<th>Trip ID</th>
				<th>Date</th>
				<th>Amount $</th>
				<th>Status</th>
				<th>BS Bucks</th>
			</tr>
			<tr>
				<td> <a href="">689</a> </td>
				<td>05/20/2015</td>
				<td>$500</td>
				<td>Pending</td>
				<td>100</td>
			</tr>
			<tr>
				<td> <a href="">689</a> </td>
				<td>05/20/2015</td>
				<td>$500</td>
				<td>Pending</td>
				<td>100</td>
			</tr>
			<tr>
				<td> <a href="">689</a> </td>
				<td>05/20/2015</td>
				<td>$500</td>
				<td>Pending</td>
				<td>100</td>
			</tr>
			<tr>
				<td> <a href="">689</a> </td>
				<td>05/20/2015</td>
				<td>$500</td>
				<td>Pending</td>
				<td>100</td>
			</tr>
		</table>

	</div>
</div>