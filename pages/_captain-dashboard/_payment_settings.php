<div class="dashboard-section section-payment_settings">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-captain_dashboard_direct-pay"></i>
        Payment Settings (ACH)</h2>
        <p class="intro-text">Direct Deposit Information (ACH) Please provide your banking details; this is where your funds will be direct deposited.</p>
    </div>

    <div class="dashboard-section">
        <div class="dashboard-section-header">
            <h3 class="intro-text">
            <i class="bs-icon-captain_dashboard_account-info blue"></i>
            Account info
            </h3>
            <a id="payment-account-edit-trigger" class="pull-right edit-inputs">Edit <i class="nc-icon-outline x075 arrows-1_double-right"></i></a>
        </div>

        <div id="payment-account-info-values" >
            <div class="row data-set margin-top-full">
                <div class="col-xs-12 col-sm-6">
                    <p class="data-set-name">Boat Name</p>
                    <p class="data-set-value">Alvaro Morin</p>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <p class="data-set-name">Account Type</p>
                    <p class="data-set-value">Savings</p>
                </div>
            </div>
            <div class="row data-set margin-top-full">
                <div class="col-xs-12 col-sm-6">
                    <p class="data-set-name">Enter the Bank name or routing number</p>
                    <p class="data-set-value">012345678</p>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <p class="data-set-name">Account Number</p>
                    <p class="data-set-value">012345678</p>
                </div>
            </div>
        </div>

        <div id="payment-account-info-edit" class="hidden">
            <div class="row">
                <div class="col-xs-12">
                    <p class="with-number" data-number="1">
                        <label for="Holder Name" class="sr-only">Holder Name</label>
                        <input id="Holder Name" class="bs-inputs"  type="text"  placeholder="Account Holder Name (as shown in your cheque book)">
                    </p>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <p class="with-number" data-number="2">
                        <label for="Holder Name" class="sr-only">Routing Number</label>
                        <input id="Holder Name" class="bs-inputs"  type="text"  placeholder="Routing Number">
                    </p>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <p class="with-number" data-number="3">
                        <label for="Holder Name" class="sr-only">Account Number</label>
                        <input id="Holder Name" class="bs-inputs"  type="text"  placeholder="Account Number">
                    </p>
                </div>

                <div class="col-xs-12">
                    <p>
                        <span class="margin-right-full">
                            <input name="wat" id="radio1" class="simple" type="radio">
                            <label for="radio1" class="horizontal">Checking</label>
                        </span>

                        <span class="margin-right-full">
                            <input name="wat" id="radio2" class="simple" type="radio">
                            <label for="radio2" class="horizontal">Savings</label>
                        </span>
                    </p>         

                    <p>
                        <span class="mobile-block margin-right-full">
                            <input name="wat" id="check1" class="simple" type="checkbox">
                            <label for="check1" class="horizontal">Send email to recipient when any payment is scheduled.</label>
                        </span>

                        <span class="mobile-block margin-right-full">
                            <input name="wat" id="check2" class="simple" type="checkbox">
                            <label for="check2" class="horizontal">Apply to all my boats.</label>
                        </span>
                    </p>

                    <div class="col-xs-12">
                        <p class="text-center margin-top-full">
                            <img width="500" height="261" src="images/dashboard/check-example.png" class="img-responsive">
                        </p>
                    </div>       
                </div>
            </div>
        </div>


    </div>

    <div class="dashboard-section">
        <div class="dashboard-section-header">
            <h3 class="intro-text ">
            <i class="bs-icon-captain_dashboard_documentation blue"></i>
            Documentation
            </h3>
            <a id="documentation-edit-trigger" class="pull-right edit-inputs">Edit <i class="nc-icon-outline x075 arrows-1_double-right"></i></a>
        </div>

        <div id="documentation-values">
            <div class="row data-set margin-top-full">
                <div class="col-xs-12">
                    <p>
                        Edit to complete and sign the W9 form. You can also email it to us at <a href="mailto:support@boatsetter.com">support@boatsetter.com</a>
                    </p>
                </div>
            </div>
        </div>

        <div id="documentation-edit" class="hidden">
            <div class="row data-set margin-top-full">
                <div class="col-xs-12">
                    <p>
                        Please complete and sign the W9 form below and attach it (or drag and drop it) below. You can also email it to us at <a href="mailto:support@boatsetter.com">support@boatsetter.com</a>
                    </p>

                </div>
                <div class="col-xs-6 col-sm-4">
                    <p><a class="btn btn-secondary btn-knockout">W9 Form</a></p>
                </div>
                <div class="col-xs-6 col-sm-8">
                    <p>Drag and drop here or <a class="btn btn-secondary btn-knockout btn-sm">Upload</a></p>
                </div>
            </div>
        </div>


    </div>
</div>