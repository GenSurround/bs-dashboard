<div class="dashboard-section section-availability">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_availability blue"></i>
        Availability</h2>
        <p class="intro-text">Click on each date to update your boat's availability.</p>
    </div>

    <div class="availability-calendar">
        <div class="bs-calendar">
            <div class="bs-calendar-header">
                    <h1>December 2016</h1>
                <div class="bs-calendar-nav">
                    <a class="prev">
                        <i class="bs-icon-bs_left-arrow blue"></i> <span class="long">November</span>
                        <span class="short">Nov</span>
                    </a>
                    <a class="next">
                        <span class="long">January</span>
                        <span class="short">Jan</span> <i class="bs-icon-bs_right-arrow blue"></i> 
                    </a>
                </div>
            </div>

            <div class="bs-calendar-months">
                <div class="bs-calendar-selected-month">

                </div>
            </div>

            <div class="legend">
                <span><img width="20" height="20" src="images/bs-calendar/bs_booked.svg" alt="Pending Approval"> Trip Booked</span>
                <span><img width="20" height="20" src="images/bs-calendar/bs_unavailable.svg" alt="Upcoming"> Unavailable for Charters</span>
            </div>
        </div>
    </div>
</div>

<script id="day-tooltipster"
type="text/x-handlebars-template">
<div class="tooltipster-trips">
    <div id="d-{{day}}-tooltipster" class="bs-trip trip-tooltip collapsed">
        <div class="trip-header row">
            <div class="col-xs-12">
                <h3>Set Your Status:</h3>
            </div>
            <div class="col-xs-12 col-sm-6 margin-bottom-full">
                <input name="availability" id="d-{{day}}-available" class="simple" type="radio" checked="checked" value="available">
                <label for="d-{{day}}-available" class="btn btn-block btn-available ">
                    <img width="20" height="20" src="images/bs-calendar/bs_booked.svg" alt="unavailable">
                    Available
                </label>
            </div>
            <div class="col-xs-12 col-sm-6 margin-bottom-full">
                <input name="availability" id="d-{{day}}-unavailable" class="simple" type="radio" value="unavailable">
                <label for="d-{{day}}-unavailable" class="btn btn-block btn-unavailable btn-knockout">
                    <img width="20" height="20" src="images/bs-calendar/bs_unavailable.svg" alt="unavailable">
                    Unavailable
                </label>
            </div>
            <div class="col-xs-12 col-sm-6 text-center">
                <span class="margin-bottom-full">
                    <input name="range" id="d-{{day}}-single" class="simple" type="radio" checked="checked">
                    <label for="d-{{day}}-single" class="horizontal">Just Today</label>
                </span>
            </div>
            <div class="col-xs-12 col-sm-6 text-center">
                <span class="margin-bottom-full">
                    <input name="range" id="d-{{day}}-multi" class="simple" type="radio">
                    <label for="d-{{day}}-multi" class="horizontal">Multiple Days</label>
                </span>
            </div>
        </div>
        <hr class="border-bottom-gray padding-bottom-full margin-top-none">
        <div class="row trip-body">
            <div class="col-xs-12 col-sm-4">
                <p>Starting today</p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <p>
                    <label class="bs-select small margin-bottom-none">Filter from</label>
                    <span class="bs-datepicker">
                        <input id="d-{{day}}-filter-from" class="bs-inputs short"  type="text" value="{{day}}/{{month}}/{{year}}" disabled="disabled">
                    </span>
                </p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <label class="bs-select small margin-bottom-none">To</label>
                <span class="bs-datepicker">
                    <input id="d-{{day}}-filter-to" class="bs-inputs short"  type="text" value="{{day}}/{{month}}/{{year}}" disabled="disabled">
                </span>
            </div>
        </div>
        <div class="row">
            <p class="text-center margin-bottom-none">
                <a id="d-{{day}}-save" href="#" class="btn btn-secondary">Save Status</a>
            </p>
        </div>
    </div>
</div>
</div>
</script>