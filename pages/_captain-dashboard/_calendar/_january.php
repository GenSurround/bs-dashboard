<div class="bs-calendar-grid">
    <div class="table-row table-header">
        <div class="table-cell">SUN</div>
        <div class="table-cell">MON</div>
        <div class="table-cell">TUE</div>
        <div class="table-cell">WED</div>
        <div class="table-cell">THU</div>
        <div class="table-cell">FRI</div>
        <div class="table-cell">SAT</div>
    </div>
    <div class="table-row">
        <div id="d01" class="table-cell date"
            data-date='{"day":"01", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">1</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d02" class="table-cell date"
            data-date='{"day":"02", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">2</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d03" class="table-cell date"
            data-date='{"day":"03", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">3</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d04" class="table-cell date"
            data-date='{"day":"04", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">4</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d05" class="table-cell date"
            data-date='{"day":"05", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">5</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d06" class="table-cell date"
            data-date='{"day":"06", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">6</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d07" class="table-cell date"
            data-date='{"day":"07", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">7</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
    </div>
    <div class="table-row">
        <div id="d08" class="table-cell date"
            data-date='{"day":"08", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">8</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d09" class="table-cell date"
            data-date='{"day":"09", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">9</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d10" class="table-cell date"
            data-date='{"day":"10", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">10</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d11" class="table-cell date"
            data-date='{"day":"11", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">11</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>            
        </div>
        <div id="d12" class="table-cell date"
            data-date='{"day":"12", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">12</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d13" class="table-cell date"
            data-date='{"day":"13", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">13</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d14" class="table-cell date"
            data-date='{"day":"14", "month":"01", "year":"2017"}'
            data-status="booked">
            <span class="day-number">14</span>
            <div class="trips"> 
                <div class="status"></div>
                <div id="trip-0001-trigger" data-source="#trip-0001-tooltipster" class="trip cancelled tooltipster">
                    <img width="20" height="20" src="images/bs-calendar/bs_booked.svg" alt="booked">
                    <span class="ref"><span class="code">#0001</span></span>
                </div>
            </div>
        </div>
    </div>
    <div class="table-row">
        <div id="d15" class="table-cell date"
            data-date='{"day":"15", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">15</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d16" class="table-cell date"
            data-date='{"day":"16", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">16</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d17" class="table-cell date"
            data-date='{"day":"17", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">17</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d18" class="table-cell date"
            data-date='{"day":"18", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">18</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d19" class="table-cell date"
            data-date='{"day":"19", "month":"01", "year":"2017"}'
            data-status="booked">
            <span class="day-number">19</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d20" class="table-cell date"
            data-date='{"day":"20", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">20</span>
            <div class="trips"> 
                <div class="status"></div>               
            </div>
        </div>
        <div id="d21" class="table-cell date"
            data-date='{"day":"21", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">21</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
    </div>
    <div class="table-row">
        <div id="d22" class="table-cell date"
            data-date='{"day":"22", "month":"01", "year":"2017"}'
            data-status="unavailable">
            <span class="day-number">22</span>
            <div class="trips"> 
                <div class="status">
                    <img width="20" height="20" src="images/bs-calendar/bs_unavailable.svg" alt="unavailable">
                    <span class="ref"><span class="code">N/A</span></span>
                </div>
            </div>
        </div>
        <div id="d23" class="table-cell date"
            data-date='{"day":"23", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">23</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d24" class="table-cell date"
            data-date='{"day":"24", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">24</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d25" class="table-cell date"
            data-date='{"day":"25", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">25</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d26" class="table-cell date"
            data-date='{"day":"26", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">26</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d27" class="table-cell date"
            data-date='{"day":"27", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">27</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d28" class="table-cell date"
            data-date='{"day":"28", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">28</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
    </div>
    <div class="table-row">
        <div id="d29" class="table-cell date"
            data-date='{"day":"29", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">29</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d30" class="table-cell date"
            data-date='{"day":"30", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">30</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div id="d31" class="table-cell date"
            data-date='{"day":"31", "month":"01", "year":"2017"}'
            data-status="available">
            <span class="day-number">31</span>
            <div class="trips"> 
                <div class="status"></div>
            </div>
        </div>
        <div class="table-cell"></div>
        <div class="table-cell"></div>
        <div class="table-cell"></div>
        <div class="table-cell"></div>
    </div>
</div>
<div id="day-trips" class="day-trips" >
    <?php include "../../../pages/_captain-dashboard/_trips-tooltips/_booked.php"; ?>
</div>