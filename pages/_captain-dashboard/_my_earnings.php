<div class="dashboard-section section-boats_i_run">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_earning"></i>
        My Earnings</h2>
        <p class="intro-text">Lorem ipsum dolor sit amet.</p>
    </div>


    <div class="section-trips">
        
        <ul class="nav nav-tabs flat-tabs wide text-center margin-bottom-double" role="tablist">
            <li role="presentation" 
                class="tab-pane active">
                <a href="#we-owe-you" aria-controls="we-owe-you" role="tab" data-toggle="tab">
                    We Owe You <br>
                    <span class="price big"><i class="bs-icon-captain_dashboard_profil"></i> $2,000</span>

                </a>
            </li>
            <li role="presentation" 
                class="tab-pane">
                <a href="#this-month" aria-controls="this-month" role="tab" data-toggle="tab">
                Earning This Month <br>
                <span class="price big"><i class="bs-icon-month-earning"></i> $3,000</span>
                </a>
            </li>
            <li role="presentation" 
                class="tab-pane">
                <a href="#lifetime" aria-controls="lifetime" role="tab" data-toggle="tab">
                Lifetime <br>
                <span class="price big"><i class="bs-icon-lifetime-earning"></i> $12,000</span>
                </a
            ></li>
        </ul>
        <!-- Tab panes -->
        
        <div class="tab-content flat-tabs">
            <div role="tabpanel" class="tab-pane active" id="we-owe-you">
                <?php include '_earnings/_we-owe-you.php'; ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="this-month">
                <?php include '_earnings/_this-month.php'; ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="lifetime">
                <?php include '_earnings/_lifetime.php'; ?>

            </div>
        </div>
    </div>
</div>