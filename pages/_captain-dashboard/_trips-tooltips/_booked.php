<div class="tooltipster-trips">
	<div id="trip-0001-tooltipster" class="bs-trip trip-tooltip collapsed">
		<div class="trip-header row">
			<div class="col-xs-12">
				<h2><a href="" class="blue">Trip #0001</a> - December 14th, 2016</h2>
				<span class="trip-status upcoming">
					<img width="20" height="20" src="images/bs-calendar/bs_upcoming.svg" alt="upcoming">
					<span class="hidden-xs">upcoming</span>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-6">
				<div class="row user-profile">
					<div class="col-xs-12">
						<span class="img-wrapper">
							<img src="images/placeholders/dude1.jpg" >
						</span>
						<span class="user-details">
							<span class="user-kind margin-bottom-none">
								<strong>The Renter:</strong>
							</span>
							<span class="user-name">
								Pedro Manuel
							</span>
							<span class="user-send-message small">
								<a href=""><i class="bs-icon-bs_messages"></i> Send Message</a>
							</span>
						</span>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 border-left-gray">
				<div class="row user-profile">
					<div class="col-xs-12">
						<span class="img-wrapper">
							<img src="images/placeholders/boat1.jpg" >
						</span>
						<span class="user-details">
							<span class="user-kind margin-bottom-none">
								<strong>The Boat:</strong>
							</span>
							<span class="user-name">
								Aguanile
							</span>
							<span class="user-send-message small">
								<a href=""><i class="bs-icon-bs_messages"></i> Send Message</a>
							</span>
						</span>
					</div>
				</div>
			</div>
			<div class="col-xs-12"><hr class="border-bottom-gray padding-bottom-full margin-top-none"></div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h4 class="small">Itinerary:</h4>
			</div>
		</div>
		<div class="row trip-body">
			<div class="properties col-xs-12 col-sm-4">
				<p><i class="bs-icon-bs_half-day"></i> Half Day</p>
			</div>
			<div class="properties col-xs-12 col-sm-4">
				<p><i class="bs-icon-bs_passengers"></i> 8 Passengers</p>
			</div>
			<div class="properties col-xs-12 col-sm-4">
				<p><i class="bs-icon-bs_time-65"></i> 8:30 am - 12:30 pm</p>
			</div>
		</div>

		<div class="row trip-body">
			<div class="col-xs-12 col-sm-4">
				<p class="price margin-top-half margin-bottom-none"><i class="bs-icon-bs_earning blue"></i>
				$480</p>
			</div>
			<div class="col-xs-12 col-sm-4">
				<p class="margin-top-full margin-bottom-none">
					<a href="" class="btn btn-secondary btn-knockout btn-sm btn-block">Cancel</a>
				</p>
			</div>
			<div class="col-xs-12 col-sm-4">
				<p class="margin-top-full margin-bottom-none">
					<a href="" class="btn btn-secondary btn-knockout btn-sm btn-block">View Messages</a>
				</p>
			</div>
		</div>
	</div>
</div>
</div>