<div class="dashboard-section section-boats_i_run">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_boat-details blue"></i>
        Boats I Run</h2>
        <p class="intro-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus cumque perspiciatis dolor id asperiores. Error quis voluptate ratione tempore amet?</p>
    </div>
    <div class="row find-boat margin-top-half margin-bottom-full">
        <div class="col-xs-12 col-sm-5 pull-right">
            <p class="bs-inputs-icon">
                <input class="bs-inputs bs-inputs-lg" type="text" placeholder="Find a Boat by Name">
                <i class="bs-icon-bs_search"></i>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="boat col-xs-12 border-bottom">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="user-profile">
                        <span class="img-wrapper">
                            <img src="images/placeholders/boat.jpg" >
                        </span>
                        <span class="user-details">
                            <span class="user-name">
                                White Dolphin - 34'
                            </span>
                            <span class="user-rating">
                                <i class="fa fa-star yellow"></i>
                                <i class="fa fa-star yellow"></i>
                                <i class="fa fa-star yellow"></i>
                                <i class="fa fa-star yellow"></i>
                                <i class="fa fa-star yellow"></i>
                                <small>(6 reviews)</small>
                            </span>
                            
                        </span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 pull-right text-right text-center-xs">
                        <p class="margin-top-full"><a> <i class="bs-icon-bs_boat-profile bs-icon-lg"></i> View boat profile</a></p>
                        <p class="margin-top-full"><a> <i class="bs-icon-bs_messages bs-icon-lg"></i> Message Owner</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="row text-center">
                        <div class="boat-feature">
                            <i class="bs-icon-bs_location"></i>
                            <span class="boat-feature-name">Location</span>
                            <span class="boat-feature-value">Miami, FL</span>
                        </div>
                        <div class="boat-feature">
                            <i class="bs-icon-bs_passengers"></i>
                            <span class="boat-feature-name">Capacity</span>
                            <span class="boat-feature-value">8 Passengers</span>
                        </div>
                        <br class="only-xxs"/>
                        <div class="boat-feature">
                            <i class="bs-icon-captain_dashboard_trips"></i>
                            <span class="boat-feature-name">My Trips</span>
                            <span class="boat-feature-value">18 Trips</span>
                        </div>
                        <div class="boat-feature">
                            <i class="bs-icon-members_captain-01"></i>
                            <span class="boat-feature-name">Captain Since</span>
                            <span class="boat-feature-value">May, 2015</span>
                        </div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-6">
                    <p class="margin-top-full text-right">
                        <a href="" class="btn btn-knockout btn-secondary ">Quit Assignment</a>
                    </p>
                </div>
            </div>
            
        </div>
        
    </div>
</div>