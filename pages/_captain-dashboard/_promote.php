<div class="dashboard-section section-boats_i_run">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_promote-your-boat blue"></i>
        Promote my services</h2>
        <p class="intro-text">Let your network know that you're a Boatsetter captain! Share your fleet with: a web page link, a custom email template and posts for Facebook, Whatsapp & Twitter.</p>
    </div>
    <div class="indented-content margin-bottom-double">
        <div class="dashboard-section-header">
            <h3 class="intro-text">
                <i class="bs-icon-bs-mypage"></i>
                My page
            </h3>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-10">
                <p>Just copy/paste this link to send your network right to your fleet.</p>
                <div class="bs-input-group input-group">
                    <input id="copy-clipboard" type="text" class="form-control copy" placeholder="www.boatsetter.com/boats-yachts/daytona-beach-fl/rental-1472" value="www.boatsetter.com/boats-yachts/daytona-beach-fl/rental-1472">
                    <span class="bs-input-group input-group-btn">
                        <button id="copy-button" class="btn btn-secondary" type="button">Copy</button>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="indented-content margin-bottom-double">
        <div class="dashboard-section-header">
            <h3 class="intro-text">
                <i class="bs-icon-bs-direct-share"></i>
                Share via email
            </h3>

        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-10">
                <p>Use this email template to share your fleet with your contact list. (Send reminders: Book early for 4th of July!)
                <br>
                <a href="javascript:void(0);" data-target="#modal-email" data-toggle="modal">Click here</a> to see a preview.</p>
                <div class="bs-input-group input-group margin-bottom-full">
                    <input type="text" class="form-control" placeholder="enter email addresses">
                    <span class="bs-input-group input-group-btn">
                        <button class="btn btn-secondary" type="button">Share</button>
                    </span>
                </div>
                <p>Import your contacts from 
                <br class="visible-xs">
                <a href="" class="import-btn import-gmail"><img src="images/bs-icons/spare/gmail.png" alt="gmail" width="30" height="30"> Gmail</a> 
                <a href="" class="import-btn import-yahoo"><img src="images/bs-icons/spare/yahoo.png" alt="gmail" width="30" height="30"> Yahoo</a>
                <a href="" class="import-btn import-outlook"><img src="images/bs-icons/spare/outlook.png" alt="gmail" width="30" height="30"> Outlook</a> 

                </p> 
            </div>
        </div>
    </div>

    <div class="indented-content margin-bottom-double">
        <div class="dashboard-section-header">
            <h3 class="intro-text">
                <i class="bs-icon-account_social"></i>
                Share on Social
            </h3>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-10">
                <p>With one click, you can share your services with your social networks! (Post in advance of long weekends, holidays etc.) <a href="javascript:void(0);" data-target="#modal-facebook" data-toggle="modal">Click here</a> to see a preview.</p>

                <p>
                    <a class="btn btn-inline btn-knockout btn-facebook-2">
                        <img src="images/bs-icons/spare/facebook.png" alt="gmail" width="30" height="30">
                    Facebook</a>
                    &nbsp;
                    <a class="btn btn-inline btn-knockout btn-messenger-2">
                        <img src="images/bs-icons/spare/messenger.png" alt="gmail" width="30" height="30">
                    Messenger</a>
                    &nbsp;
                    <a class="btn btn-inline btn-knockout btn-twitter-2">
                        <img src="images/bs-icons/spare/twitter.png" alt="gmail" width="30" height="30">
                    Twitter</a>
                </p>
            </div>
        </div>
    </div>
</div>


<div class="modal mobile-modal fade" id="modal-facebook" tabindex="-1" role="dialog" aria-labelledby="smart-pricing">
  <div class="modal-dialog" role="document">
    <div class="modal-content padding">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">

        <img src="images/dashboard/promo/facebook-captain.jpg" class="img-responsive">
          
      </div>

    </div>
  </div>
</div>


<div class="modal mobile-modal fade" id="modal-email" tabindex="-1" role="dialog" aria-labelledby="smart-pricing">
  <div class="modal-dialog" role="document">
    <div class="modal-content padding">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">

        <img src="images/dashboard/promo/email-captain.jpg" class="img-responsive">
          
      </div>

    </div>
  </div>
</div>