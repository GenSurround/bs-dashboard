
<div class="dashboard-section section-pendind-trips">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_trip-request-30 blue"></i>
        Pending Trips</h2>
        <p class="intro-text">Please accept or decline the following trip requests:</p>
    </div>

    <div id="trip-1108" class="bs-trip collapsed">
        <div class="trip-header row">
            <div class="col-xs-12">
                <h2> <i class="trip-state nc-icon-outline arrows-1_minimal-right"></i> <a href="" class="blue">Trip #1108</a> - October 14th, 2016</h2>
                <span class="open-bs-messenger">
                    <span class="img-wrapper">
                        <img src="images/bs_messenger.svg" alt="">
                    </span>
                    <span class="red-badge">3</span>
                </span>
            </div>

        </div>
        <div class="trip-body row">
            <div class="properties col-xs-8">
                <div class="row margin-top-full">
                    <div class="col-xs-6"><p><i class="bs-icon-bs_half-day"></i> Half Day</p></div>
                    <div class="col-xs-6"><p><i class="bs-icon-bs_passengers"></i> 8 Passengers</p></div>
                    <div class="col-xs-6"><p><i class="bs-icon-bs_time-65"></i> 8:30 am - 12:30 pm</p></div>
                    <div class="col-xs-6"><p><i class="bs-icon-bs_captain-required"></i> With Captain</p></div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="row  margin-top-full">
                    <div class="col-xs-12 margin-top-full text-right">
                        <span class="price">
                            <i class="bs-icon-bs_earning blue"></i> $480
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="trip-footer trip-footer-collapsed row">
            <div class="col-xs-12">
                <p class="text-right">
                    <a class="more-detail btn btn-secondary btn-knockout">Trip Details</a>
                    <a href="" class="btn btn-secondary btn-knockout">Decline</a>
                    <a href="" class="btn btn-primary"><i class="bs-icon-bs_availability"></i> Accept Trip</a>
                </p>
            </div>
        </div>
        <div class="trip-more-info">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="row user-profile">
                        <div class="col-xs-12">
                            <span class="img-wrapper">
                                <img src="images/placeholders/dude1.jpg" >
                            </span>
                            <span class="user-details">
                                <span class="user-kind margin-bottom-none">
                                    <strong>The Renter:</strong>
                                </span>
                                <span class="user-name">
                                    Pedro Manuel
                                </span>
                                <span class="user-send-message small">
                                    <a href=""><i class="bs-icon-bs_messages"></i> Send Message</a>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="row user-more-info margin-top-full">
                        <div class="col-xs-6">             
                            <img height="28" src="images/bs_messenger.svg"> 
                        </div>
                        <div class="col-xs-6 text-right">
                            <span class="small">3 Messages</span>
                        </div>
                        <div class="col-xs-12 margin-top-half">
                            <p class="user-description margin-bottom-half small">
                            Lorem ipsum dolor sit amet, conseetur adipiscing elit. Etiam eu tristique lacus. Lorem ipsum dolor sit amet, conseetur adipiscing elit. Etiam eu tristique lacus.
                            </p>
                            <p class="margin-top-none small">
                                Fri Sep 20, 2:34 pm
                            </p>
                        </div>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 border-left-gray">
                    <div class="row user-profile">
                        <div class="col-xs-12">
                            <span class="img-wrapper">
                                <img src="images/placeholders/cap1.jpg" >
                            </span>
                            <span class="user-details">
                                <span class="user-kind margin-bottom-none">
                                    <strong>The Captain:</strong>
                                </span>
                                <span class="user-name">
                                    Alfred Molina
                                </span>
                                <span class="user-send-message small">
                                    <a href=""><i class="bs-icon-bs_messages"></i> Send Message</a>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="row captain user-more-info margin-top-full">
                        <div class="col-xs-12 margin-top-half">
                            <div class="row captain-features text-center">
                                <div class="captain-feature">
                                    <i class="bs-icon-bs_location"></i>
                                    <span class="captain-feature-name">Location</span>
                                    <span class="captain-feature-value">Miami, FL</span>
                                </div>
                                <div class="captain-feature">
                                    <i class="bs-icon-bs_experience"></i>
                                    <span class="captain-feature-name">Experience</span>
                                    <span class="captain-feature-value">11 Trips</span>
                                </div>
                                <div class="captain-feature">
                                    <i class="bs-icon-bs_response"></i>
                                    <span class="captain-feature-name">Avg Resp</span>
                                    <span class="captain-feature-value">23 min</span>
                                </div>
                                <div class="captain-feature">
                                    <i class="bs-icon-bs_boat"></i>
                                    <span class="captain-feature-name">Assigned</span>
                                    <span class="captain-feature-value">10 Boats</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="text-right margin-top-full  margin-bottom-none">
                        <span class="more-detail btn btn-secondary btn-knockout">
                            Less Details
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>        