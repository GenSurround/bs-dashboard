<div class="dashboard-section section_my_price">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-captain_dashboard_price blue"></i>
        My Price</h2>
        <p class="intro-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus cumque perspiciatis dolor id asperiores. Error quis voluptate ratione tempore amet?</p>
    </div>
    <div class="dashboard-section">
        <div class="dashboard-section-header">
            <h3 class="intro-text">
            <i class="bs-icon-captain_dashboard_profil"></i>
            Captain Profit
            </h3>
        </div>
        <div class="indented-content">
            <table class="captain-profit-table">
                <tr>
                    <th></th>
                    <th><img width="43" height="14" src="images/bs-icons/boats/small-boat.svg" alt=""></th>
                    <th><img width="43" height="22" src="images/bs-icons/boats/medium-boat.svg" alt=""></th>
                    <th><img width="65" height="23" src="images/bs-icons/boats/bigger-boat.svg" alt=""></th>
                </tr>
                <tr> <th>&nbsp;</th>
                <th><div class="blue">
                0 - 29</div></th>
                <th><div class="blue">
                30 - 45</div></th>
                <th><div class="blue">
                46+</div></th>
            </tr>
            <tr>
                <td><i class="bs-icon-bs_half-day bigger"></i> Half-Day</td>
                <td>$200</td>
                <td>$240</td>
                <td>$350</td>
            </tr>
            <tr>
                <td><i class="bs-icon-bs_full-day bigger"></i> Full Day</td>
                <td>$300</td>
                <td>$375</td>
                <td>$600</td>
            </tr>
        </table>
    </div>
</div>
<div class="dashboard-section-header">
    <h3 class="intro-text">
    <i class="bs-icon-cash-flow"></i>
    How much could I be earning?</h3>
    <p class="intro-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus cumque perspiciatis dolor id asperiores. Error quis voluptate ratione tempore amet?</p>
</div>
<div class="indented-content">
    <div class="row">
        <div class="col-xs-12 col-sm-7">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <p>     
                        <label class="bs-select margin-bottom-none">Boats</label>

                        <select class="cs-select no-icon cs-skin-underline">
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </p>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <p>     
                        <label class="bs-select margin-bottom-none">Rental Type</label>        

                        <select class="cs-select no-icon cs-skin-underline">
                            <option value="">Big Boat / Half Day</option>
                            <option value="">Big Boat / Full Day</option>
                            <option value="">Medium Boat / Half Day</option>
                            <option value="">Medium Boat / Full Day</option>
                            <option value="">Small Boat / Half Day</option>
                            <option value="">Small Boat / Full Day</option>
                        </select>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <label><span>Trips</span></label>
                    <div id="trips-slider" class="inline-slider"></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-sm-push-1">
            <div class="my-cash-flow-results">
                <p class="title"><i class="bs-icon-cash-flow blue bigger"></i> My cash flow</p>
                <p class="blue margin-bottom-none"><span class="price">$ 2,250</span>/month</p>
            </div>
        </div>
    </div>
    
</div>
</div>