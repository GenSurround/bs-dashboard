<div class="dashboard-section section-boats_i_run">
    <div class="dashboard-section-header">
        <h2 class="intro-text">
        <i class="bs-icon-bs_search blue"></i>
        Find Boats to Run</h2>
        <p class="intro-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus cumque perspiciatis dolor id asperiores. Error quis voluptate ratione tempore amet?</p>
    </div>
    <div class="row find-boat margin-top-half margin-bottom-full">
        <div class="col-xs-12 col-sm-8">
            <p class="bs-inputs-icon">
                <input class="bs-inputs bs-inputs-lg" type="text" placeholder="Location">
                <i class="bs-icon-bs_location"></i>
            </p>
        </div>
        <div class="col-xs-12 col-sm-4">
            <p>
                <a href="" class="btn btn-secondary btn-block">Find Boats </a>
            </p>
        </div>
    </div>

    <div class="row find-boat margin-top-half margin-bottom-full">
        <div class="col-xs-12 col-sm-4">
            <p class="bs-inputs-icon">
                <input class="bs-inputs bs-inputs-lg" type="text" placeholder="Miami, FL">
                <i class="bs-icon-bs_location"></i>
            </p>
        </div>
        <div class="col-xs-12 col-sm-8">
        <p class="bs-inputs-icon">
            <input class="bs-inputs bs-inputs-lg" type="text" placeholder="Filter Search by Name, Description or Brand">
            <i class="bs-icon-bs_search"></i>
        </p>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-5">
            <label><span>Length</span></label>
            <div id="length-slider" class="inline-slider"></div>
        </div>
        <div class="col-xs-12 col-sm-5">
            <label><span>Passengers</span></label>
            <div id="passengers-slider" class="inline-slider"></div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <p class=" margin-top-double">
                <a href="" class="btn btn-secondary btn-block ">
                    Filter Results
                </a>
            </p>
        </div>
    </div>

    <div class="row boats margin-top-double">
        <div class="col-xs-12 col-sm-4 boat-tile">
            <div class="img-wrapper">
                <img src="images/placeholders/boat2.jpg" alt="" class="img-responsive">
            </div>
            <h3 class="margin-top-half margin-bottom-quarter">White Dolphin - 34'</h3>
            <span class="user-rating">
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <small>(6 reviews)</small>
            </span>

            <div class="margin-top-half">
                <i class="bs-icon-bs_location"></i>
                <span class="boat-feature-value">Miami, FL</span>
                <br>
                <i class="bs-icon-bs_passengers"></i>
                <span class="boat-feature-value">8 Passengers</span>
            </div>
            <div class="text-center margin-top-half">
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_messages bs-icon-lg"></i> Message Owner</a></p>
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_boat-profile bs-icon-lg"></i> View boat profile</a></p>
            </div>
            <p><a href="" class="btn btn-primary btn-block">Apply to be a Captain</a></p>
        </div>
        <div class="col-xs-12 col-sm-4 boat-tile">
            <div class="img-wrapper">
                <img src="images/placeholders/boat2.jpg" alt="" class="img-responsive">
            </div>
            <h3 class="margin-top-half margin-bottom-quarter">White Dolphin - 34'</h3>
            <span class="user-rating">
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <small>(6 reviews)</small>
            </span>

            <div class="margin-top-half">
                <i class="bs-icon-bs_location"></i>
                <span class="boat-feature-value">Miami, FL</span>
                <br>
                <i class="bs-icon-bs_passengers"></i>
                <span class="boat-feature-value">8 Passengers</span>
            </div>
            <div class="text-center margin-top-half">
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_messages bs-icon-lg"></i> Message Owner</a></p>
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_boat-profile bs-icon-lg"></i> View boat profile</a></p>
            </div>
            <p><a href="" class="btn btn-primary btn-block">Apply to be a Captain</a></p>
        </div>
        <div class="col-xs-12 col-sm-4 boat-tile">
            <div class="img-wrapper">
                <img src="images/placeholders/boat2.jpg" alt="" class="img-responsive">
            </div>
            <h3 class="margin-top-half margin-bottom-quarter">White Dolphin - 34'</h3>
            <span class="user-rating">
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <small>(6 reviews)</small>
            </span>

            <div class="margin-top-half">
                <i class="bs-icon-bs_location"></i>
                <span class="boat-feature-value">Miami, FL</span>
                <br>
                <i class="bs-icon-bs_passengers"></i>
                <span class="boat-feature-value">8 Passengers</span>
            </div>
            <div class="text-center margin-top-half">
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_messages bs-icon-lg"></i> Message Owner</a></p>
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_boat-profile bs-icon-lg"></i> View boat profile</a></p>
            </div>
            <p><a href="" class="btn btn-primary btn-block">Apply to be a Captain</a></p>
        </div>
        <div class="col-xs-12 col-sm-4 boat-tile">
            <div class="img-wrapper">
                <img src="images/placeholders/boat2.jpg" alt="" class="img-responsive">
            </div>
            <h3 class="margin-top-half margin-bottom-quarter">White Dolphin - 34'</h3>
            <span class="user-rating">
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <small>(6 reviews)</small>
            </span>

            <div class="margin-top-half">
                <i class="bs-icon-bs_location"></i>
                <span class="boat-feature-value">Miami, FL</span>
                <br>
                <i class="bs-icon-bs_passengers"></i>
                <span class="boat-feature-value">8 Passengers</span>
            </div>
            <div class="text-center margin-top-half">
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_messages bs-icon-lg"></i> Message Owner</a></p>
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_boat-profile bs-icon-lg"></i> View boat profile</a></p>
            </div>
            <p><a href="" class="btn btn-primary btn-block">Apply to be a Captain</a></p>
        </div>
        <div class="col-xs-12 col-sm-4 boat-tile">
            <div class="img-wrapper">
                <img src="images/placeholders/boat2.jpg" alt="" class="img-responsive">
            </div>
            <h3 class="margin-top-half margin-bottom-quarter">White Dolphin - 34'</h3>
            <span class="user-rating">
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <small>(6 reviews)</small>
            </span>

            <div class="margin-top-half">
                <i class="bs-icon-bs_location"></i>
                <span class="boat-feature-value">Miami, FL</span>
                <br>
                <i class="bs-icon-bs_passengers"></i>
                <span class="boat-feature-value">8 Passengers</span>
            </div>
            <div class="text-center margin-top-half">
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_messages bs-icon-lg"></i> Message Owner</a></p>
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_boat-profile bs-icon-lg"></i> View boat profile</a></p>
            </div>
            <p><a href="" class="btn btn-primary btn-block">Apply to be a Captain</a></p>
        </div>
        <div class="col-xs-12 col-sm-4 boat-tile">
            <div class="img-wrapper">
                <img src="images/placeholders/boat2.jpg" alt="" class="img-responsive">
            </div>
            <h3 class="margin-top-half margin-bottom-quarter">White Dolphin - 34'</h3>
            <span class="user-rating">
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <i class="fa fa-star yellow"></i>
                <small>(6 reviews)</small>
            </span>

            <div class="margin-top-half">
                <i class="bs-icon-bs_location"></i>
                <span class="boat-feature-value">Miami, FL</span>
                <br>
                <i class="bs-icon-bs_passengers"></i>
                <span class="boat-feature-value">8 Passengers</span>
            </div>
            <div class="text-center margin-top-half">
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_messages bs-icon-lg"></i> Message Owner</a></p>
                    <p class="margin-bottom-half"><a> <i class="bs-icon-bs_boat-profile bs-icon-lg"></i> View boat profile</a></p>
            </div>
            <p><a href="" class="btn btn-primary btn-block">Apply to be a Captain</a></p>
        </div>
    </div>
</div>