<div class="dashboard-section section-trips">
	<div class="wrapper-trip completed">
	    <div class="completed-trips">
	        <div class="dashboard-section section-upcoming-trips">
	            <div class="dashboard-section-header">
	                <h2 class="intro-text">
	                <i class="bs-icon-bs_completed-73 blue"></i>
	                Completed Trips</h2>
	                <p class="intro-text">Review the information of your completed trips.
	                </p>
	            </div>
	        </div>

	        <div id="trip-1108" class="bs-trip collapsed">
	            <div class="trip-header row">
	                <div class="col-xs-9">
	                    <h2> <i class="trip-state nc-icon-outline arrows-1_minimal-right"></i> <a class="blue">Trip #1108</a><span> - </span>September 24th, 2016</h2>
	                    
	                </div>
	                <div class="col-xs-3">
	                    <div class="row">
	                        <div class="col-xs-12 text-right">
	                            <span class="price">
	                                <i class="bs-icon-bs_dollar-sign blue"></i> $8080
	                            </span>
	                        </div>
	                    </div>
	                </div>
	                
	            </div>
	            <div class="wrapper-body">
	                <div class="trip-body row">
	                    <div class="properties col-xs-12">
	                        <div class="row margin-top-full">
	                            <div class="col-xs-6 col-md-3 text-left"><p><i class="bs-icon-bs_half-day"></i> Half Day</p></div>
	                            <div class="col-xs-6 col-md-3 text-left"><p><i class="bs-icon-bs_passengers"></i> 8 Passengers</p></div>
	                            <div class="col-xs-6 col-md-3 text-left"><p><i class="bs-icon-bs_time-65"></i> 8:30 am - 12:30 pm</p></div>
	                            <div class="col-xs-6 col-md-3 text-left"><p><i class="bs-icon-bs_captain-required"></i> With Captain</p></div>
	                        </div>
	                    </div>
	                    
	                    <div class="wrapper-data">
	                        <div class="col-xs-12 col-sm-6 data two-columns">
	                            <img src="images/placeholders/boat1.jpg" alt="" class="img-circle">
	                            <span class="wrapper-text">
	                                <span class="kind"><strong>The Boat:</strong></span>
	                                <span class="name">Friendzone Demolisher</span>
	                                <span class="stars">
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                </span>
	                            </span>
	                            <div class="deploy row">
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-bs_messages blue"></i>Message Owner</a></div>
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-captain_dashboard_boat-profile blue"></i>Boat Profile</a></div>
	                            </div>
	                        </div>
	                        <div class="col-xs-12 col-sm-6 data two-columns">
	                            <img src="images/placeholders/cap1.jpg" alt="" class="img-circle">
	                            <span class="wrapper-text">
	                                <span class="kind"><strong>The Captain:</strong></span>
	                                <span class="name">Miguel Alejandro</span>
	                                <span class="stars">
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                </span>
	                            </span>
	                            <div class="deploy row">
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-bs_messages blue"></i>Message Captain</a></div>
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-members_captain-01 blue"></i>Captain Profile</a></div>
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-bs_calendar blue"></i>Add to Calendar</a></div>
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-bs_cancelled blue"></i>Trip Cancellation</a></div>
	                            </div>
	                        </div>
	                    </div>    
	                </div>
	                <div class="trip-footer trip-footer-collapsed row text-left">
	                    <div class="col-xs-12 col-sm-4  text-left">
	                        <span class="open-bs-messenger">
	                            <a href="#">
	                                <span class="">
	                                    <img src="images/bs_messenger.svg" alt="">
	                                </span>
	                            </a>
	                            <span class="red-badge">3</span>
	                        </span>
	                    </div>
	                    <div class="col-xs-12 col-sm-8  text-right wrapper-btn">
	                        <a href="" class="btn btn-primary btn-knockout">Book Again</a>
	                        <a href="" class="more-detail btn btn-secondary btn-knockout">Leave  a Review</a>
	                        <a href="" class="btn btn-secondary btn-knockout">Trip Details</a>
	                    </div>
	                </div>
	            </div>
	        </div>

	       
	    </div>

	    <div class="cancelled-trips">
	        <div class="dashboard-section section-upcoming-trips">
	            <div class="dashboard-section-header">
	                <h2 class="intro-text">
	                <i class="bs-icon-bs_cancelled blue"></i>
	                Cancelled Trips</h2>
	                <p class="intro-text">Review the information of your cancelled trips.
	                </p>
	            </div>
	        </div>

	        
	        <div id="trip-1108" class="bs-trip collapsed">
	            <div class="trip-header row">
	                <div class="col-xs-9">
	                    <h2> <i class="trip-state nc-icon-outline arrows-1_minimal-right"></i> <a class="blue">Trip #1108</a><span> - </span>September 24th, 2016</h2>
	                    
	                </div>
	                <div class="col-xs-3">
	                    <div class="row">
	                        <div class="col-xs-12 text-right">
	                            <span class="price">
	                                <i class="bs-icon-bs_dollar-sign blue"></i> $8080
	                            </span>
	                        </div>
	                    </div>
	                </div>
	                
	            </div>
	            <div class="wrapper-body">
	                <div class="trip-body row">
	                    <div class="properties col-xs-12">
	                        <div class="row margin-top-full">
	                            <div class="col-xs-6 col-md-3 text-left"><p><i class="bs-icon-bs_half-day"></i> Half Day</p></div>
	                            <div class="col-xs-6 col-md-3 text-left"><p><i class="bs-icon-bs_passengers"></i> 8 Passengers</p></div>
	                            <div class="col-xs-6 col-md-3 text-left"><p><i class="bs-icon-bs_time-65"></i> 8:30 am - 12:30 pm</p></div>
	                            <div class="col-xs-6 col-md-3 text-left"><p><i class="bs-icon-bs_captain-required"></i> With Captain</p></div>
	                        </div>
	                    </div>
	                    
	                    <div class="wrapper-data">
	                        <div class="col-xs-12 col-sm-6 data two-columns">
	                            <img src="images/placeholders/boat1.jpg" alt="" class="img-circle">
	                            <span class="wrapper-text">
	                                <span class="kind"><strong>The Boat:</strong></span>
	                                <span class="name">Friendzone Demolisher</span>
	                                <span class="stars">
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                </span>
	                            </span>
	                            <div class="deploy row">
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-bs_messages blue"></i>Message Owner</a></div>
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-captain_dashboard_boat-profile blue"></i>Boat Profile</a></div>
	                            </div>
	                        </div>
	                        <div class="col-xs-12 col-sm-6 data two-columns">
	                            <img src="images/placeholders/cap1.jpg" alt="" class="img-circle">
	                            <span class="wrapper-text">
	                                <span class="kind"><strong>The Captain:</strong></span>
	                                <span class="name">Miguel Alejandro</span>
	                                <span class="stars">
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                    <i class="fa fa-star yellow"></i>
	                                </span>
	                            </span>
	                            <div class="deploy row">
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-bs_messages blue"></i>Message Captain</a></div>
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-members_captain-01 blue"></i>Captain Profile</a></div>
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-bs_calendar blue"></i>Add to Calendar</a></div>
	                                <div class="col-xs-12 col-md-6 text-left"><a href="#"><i class="bs-icon-bs_cancelled blue"></i>Trip Cancellation</a></div>
	                            </div>
	                        </div>
	                    </div>


	                    

	                    
	                </div>
	                <div class="trip-footer trip-footer-collapsed row text-left">
	                    <div class="col-xs-12 col-sm-4  text-left">
	                        <span class="open-bs-messenger">
	                            <a href="#">
	                                <span class="">
	                                    <img src="images/bs_messenger.svg" alt="">
	                                </span>
	                            </a>
	                            <span class="red-badge">3</span>
	                        </span>
	                    </div>
	                    <div class="col-xs-12 col-sm-8  text-right wrapper-btn">
	                        <a href="" class="btn btn-primary btn-knockout">Book Again</a>
	                        <a href="" class="btn btn-secondary btn-knockout">Trip Details</a>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>