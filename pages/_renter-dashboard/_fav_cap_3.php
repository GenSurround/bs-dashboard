<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<span class="fav-message"><i class="bs-icon-bs_messages"></i>Message</span>
			<span class="fav-more">More Details</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info cap-back">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
					<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<div class="fav-achievements clearfix">
				<div>
					<i class="bs-icon-bs_availablility blue"></i>
					<h3>Trips Completed</h3>
					<span>13</span>
				</div>
				<div>
					<i class="bs-icon-bs_boat-details blue"></i>
					<h3>Assigned Boats</h3>
					<p>10</p>
				</div>
				
			</div>
			<div class="fav-contact clearfix">
	    		<span class="fav-cap-profile"><i class="bs-icon-members_captain-01 blue"></i>Profile</span>
	    		<span class="fav-cap-message"><i class="bs-icon-bs_messages blue"></i>Message</span>
    		</div>
		</div>
	</div>
</div>
<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<span class="fav-message"><i class="bs-icon-bs_messages"></i>Message</span>
			<span class="fav-more">More Details</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info cap-back">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
					<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<div class="fav-achievements clearfix">
				<div>
					<i class="bs-icon-bs_availablility blue"></i>
					<h3>Trips Completed</h3>
					<span>13</span>
				</div>
				<div>
					<i class="bs-icon-bs_boat-details blue"></i>
					<h3>Assigned Boats</h3>
					<p>10</p>
				</div>
				
			</div>
			<div class="fav-contact clearfix">
	    		<span class="fav-cap-profile"><i class="bs-icon-members_captain-01 blue"></i>Profile</span>
	    		<span class="fav-cap-message"><i class="bs-icon-bs_messages blue"></i>Message</span>
    		</div>
		</div>
	</div>
</div>
<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<span class="fav-message"><i class="bs-icon-bs_messages"></i>Message</span>
			<span class="fav-more">More Details</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info cap-back">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
					<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<div class="fav-achievements clearfix">
				<div>
					<i class="bs-icon-bs_availablility blue"></i>
					<h3>Trips Completed</h3>
					<span>13</span>
				</div>
				<div>
					<i class="bs-icon-bs_boat-details blue"></i>
					<h3>Assigned Boats</h3>
					<p>10</p>
				</div>
				
			</div>
			<div class="fav-contact clearfix">
	    		<span class="fav-cap-profile"><i class="bs-icon-members_captain-01 blue"></i>Profile</span>
	    		<span class="fav-cap-message"><i class="bs-icon-bs_messages blue"></i>Message</span>
    		</div>
		</div>
	</div>
</div>
<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<span class="fav-message"><i class="bs-icon-bs_messages"></i>Message</span>
			<span class="fav-more">More Details</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info cap-back">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
					<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<div class="fav-achievements clearfix">
				<div>
					<i class="bs-icon-bs_availablility blue"></i>
					<h3>Trips Completed</h3>
					<span>13</span>
				</div>
				<div>
					<i class="bs-icon-bs_boat-details blue"></i>
					<h3>Assigned Boats</h3>
					<p>10</p>
				</div>
				
			</div>
			<div class="fav-contact clearfix">
	    		<span class="fav-cap-profile"><i class="bs-icon-members_captain-01 blue"></i>Profile</span>
	    		<span class="fav-cap-message"><i class="bs-icon-bs_messages blue"></i>Message</span>
    		</div>
		</div>
	</div>
</div>
<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<span class="fav-message"><i class="bs-icon-bs_messages"></i>Message</span>
			<span class="fav-more">More Details</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info cap-back">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
					<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<div class="fav-achievements clearfix">
				<div>
					<i class="bs-icon-bs_availablility blue"></i>
					<h3>Trips Completed</h3>
					<span>13</span>
				</div>
				<div>
					<i class="bs-icon-bs_boat-details blue"></i>
					<h3>Assigned Boats</h3>
					<p>10</p>
				</div>
				
			</div>
			<div class="fav-contact clearfix">
	    		<span class="fav-cap-profile"><i class="bs-icon-members_captain-01 blue"></i>Profile</span>
	    		<span class="fav-cap-message"><i class="bs-icon-bs_messages blue"></i>Message</span>
    		</div>
		</div>
	</div>
</div>
<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<span class="fav-message"><i class="bs-icon-bs_messages"></i>Message</span>
			<span class="fav-more">More Details</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<figure>
				<img src="images/placeholders/cap1.jpg" alt="">
			</figure>
			<div class="fav-info cap-back">
				<h2>Pedro Manuel</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
					<p><i class="bs-icon-captain_dashboard_location"></i>Miami, Fl</p>
			</div>
			<div class="fav-achievements clearfix">
				<div>
					<i class="bs-icon-bs_availablility blue"></i>
					<h3>Trips Completed</h3>
					<span>13</span>
				</div>
				<div>
					<i class="bs-icon-bs_boat-details blue"></i>
					<h3>Assigned Boats</h3>
					<p>10</p>
				</div>
				
			</div>
			<div class="fav-contact clearfix">
	    		<span class="fav-cap-profile"><i class="bs-icon-members_captain-01 blue"></i>Profile</span>
	    		<span class="fav-cap-message"><i class="bs-icon-bs_messages blue"></i>Message</span>
    		</div>
		</div>
	</div>
</div>