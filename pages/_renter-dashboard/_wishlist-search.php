<div class="dashboard-section section-wishlist-search">
    <div class="row find-boat margin-top-half margin-bottom-full">
        <div class="col-xs-5 col-sm-5 pull-left wrapper-search">
            <p class="bs-inputs-icon">
                <input class="bs-inputs bs-inputs-lg" type="text" placeholder="Search Trip Wishlists">
                <i class="bs-icon-bs_search"></i>
            </p>
        </div>
        <div class="pull-right">
        	<a href="renter-dashboard.php?p=wishlist-manage" class="more-detail btn btn-secondary btn-knockout">Manage Trip Wishlists</a>
    	</div>
    </div>

	<div class="row boat border-bottom margin-top-half margin-bottom-half">
		<div class="user-profile">
		    <span class="img-wrapper">
		        <img src="images/placeholders/boat.jpg" >
		    </span>
		    <span class="user-details">
		        <span class="user-name">
		            Family Getaway
		        </span>
		        <span class="user-rating">
		        	<a href="renter-dashboard.php?p=wishlist-compare" class="blue">10 Saved Trips</a>
		        </span>
		    </span>
		    <span class="pull-right">
		    	<div>
		                <a class="btn btn-primary pull-right btn-view">View</a>
		    	</div>
		    	<div>
		                <a href="" class="blue margin-top-full pull-right">Share with Friends <i class="bs-icon-bs_share bs-icon-lg"></i></a>
		    	</div>
		    </span>
		</div>
	</div>
	<div class="row boat border-bottom margin-top-half margin-bottom-half">
		<div class="user-profile">
		    <span class="img-wrapper">
		        <img src="images/placeholders/boat.jpg" >
		    </span>
		    <span class="user-details">
		        <span class="user-name">
		            Weekend with Friends
		        </span>
		        <span class="user-rating">
		        	<a href="renter-dashboard.php?p=wishlist-compare" class="blue">5 Saved Trips</a>
		        </span>
		    </span>
		    <span class="pull-right">
		    	<div>
		                <a class="btn btn-primary pull-right btn-view">View</a>
		    	</div>
		    	<div>
		                <a href="" class="blue margin-top-full pull-right">Share with Friends <i class="bs-icon-bs_share bs-icon-lg"></i></a>
		    	</div>
		    </span>
		</div>
	</div>
	<div class="row boat border-bottom margin-top-half margin-bottom-half">
		<div class="user-profile">
		    <span class="img-wrapper">
		        <img src="images/placeholders/boat.jpg" >
		    </span>
		    <span class="user-details">
		        <span class="user-name">
		            Mom's Birthday
		        </span>
		        <span class="user-rating">
		        	<a href="" class="blue">3 Saved Trips</a>
		        </span>
		    </span>
		    <span class="pull-right">
		    	<div>
	                <a class="btn btn-primary pull-right btn-view">View</a>
		    	</div>
		    	<div>
	                <a href="renter-dashboard.php?p=wishlist-compare" class="blue margin-top-full pull-right">Share with Friends <i class="bs-icon-bs_share bs-icon-lg"></i></a>
		    	</div>
		    </span>
		</div>
	</div>
	<div class="row boat border-bottom margin-top-half margin-bottom-half">
		<div class="user-profile">
		    <span class="img-wrapper round-btn">
		        <a class= "round-btn--link" href="">+</a>
		    </span>
		    <span class="user-details">
		        <span class="user-name">
		            Add new Trip Wishlists
		        </span>
		        <span>
		        	<a class="gray">Start Adding Trips</a>
		        </span>
		    </span>
		    <span class="pull-right">
		    	<div>
		                <a class="btn btn-disabled pull-right btn-view">Add</a>
		    	</div>
		    </span>
		</div>
	</div>
</div>