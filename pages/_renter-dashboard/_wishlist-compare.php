<div class="dashboard-section section-wishlist-compare row">
    <div class="row find-boat margin-top-half margin-bottom-full">
        <div class="dashboard-section-header">
            <h2 class="intro-text">Weekend with Friends</h2>
        </div>
        <div class="col-xs-5 col-sm-5 pull-left wrapper-search">
            <p class="bs-inputs-icon">
                <input class="bs-inputs bs-inputs-lg" type="text" placeholder="Search">
                <i class="bs-icon-bs_search"></i>
            </p>
        </div>
        <div class="pull-right action">
            <a href="renter-dashboard.php?p=wishlist-trip-comparison" class="more-detail btn btn-secondary btn-knockout">        
                <i class="bs-icon-bs_specifications"></i>
            Compare Trips</a>
        </div>
    </div>

    <div class="compare-wrapper row">
        <div class="trip-compare col-xs-12 col-sm-6 col-md-4 ui-draggable-handle">
            <div class="trip-info">
                <div class="trip-info--header">
                    <label class="pull-left removetag">x Remove</label>
                    <label class="pull-right"> 
                        <div class="checkbox">
                            <input id="checkbox1" type="checkbox" class="pull-right">
                            <label for="checkbox1">
                                Compare
                            </label>
                        </div>
                    </label>
                </div>
                <div class="trip-info--pictures">
                    <div class="picture-boat-wrapper">
                        <img class="picture-boat" src="images/dashboard/fav1.jpg">
                    </div>
                    <img class="picture-captain" src="images/placeholders/cap1.jpg">
                </div>
                <div class="trip-info--text">
                    <div class="col-xs-12 margin-bottom-full">
                        <h2>Friendzone Demolisher</h2>
                        <label>Miami, FL</label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="no-padding left">
                            <i class="bs-icon-renter_dashboard_add-to-calendar"></i>
                        </div>
                        <div class="no-padding left">
                            <label class="label1">Date:</label><br>
                            <label class="label2">Jul 22, 2017</label>
                        </div>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="no-padding right">
                            <label class="label1">Half-Day:</label><br>
                            <label class="label2">$ 875</label>
                        </div>
                        <div class="no-padding right">
                            <i class="bs-icon-captain_dashboard_half-day"></i>
                        </div>
                    </div>
                </div>
                <div class="trip-info--btn">
                    <a class="btn btn-primary">Book this Trip</a>
                </div>
            </div>
        </div>
        <div class="trip-compare col-xs-12 col-sm-6 col-md-4 ui-draggable-handle">
            <div class="trip-info">
                <div class="trip-info--header">
                    <label class="pull-left removetag">x Remove</label>
                    <label class="pull-right"> 
                        <div class="checkbox">
                            <input id="checkbox2" type="checkbox" class="pull-right">
                            <label for="checkbox2">
                                Compare
                            </label>
                        </div>
                    </label>
                </div>
                <div class="trip-info--pictures">
                    <div class="picture-boat-wrapper">
                        <img class="picture-boat" src="images/dashboard/fav2.jpg">
                    </div>
                    <img class="picture-captain" src="images/placeholders/cap1.jpg">
                </div>
                <div class="trip-info--text">
                    <div class="col-xs-12 margin-bottom-full">
                        <h2>White Dolphin</h2>
                        <label>Miami, FL</label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="no-padding left">
                            <i class="bs-icon-renter_dashboard_add-to-calendar"></i>
                        </div>
                        <div class="no-padding left">
                            <label class="label1">Date:</label><br>
                            <label class="label2">Jul 22, 2017</label>
                        </div>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="no-padding right">
                            <label class="label1">Half-Day:</label><br>
                            <label class="label2">$ 875</label>
                        </div>
                        <div class="no-padding right">
                            <i class="bs-icon-captain_dashboard_half-day"></i>
                        </div>
                    </div>
                </div>
                <div class="trip-info--btn">
                    <a class="btn btn-primary">Book this Trip</a>
                </div>
            </div>
        </div>
        <div class="trip-compare col-xs-12 col-sm-6 col-md-4 ui-draggable-handle">
            <div class="trip-info">
                <div class="trip-info--header">
                    <label class="pull-left removetag">x Remove</label>
                    <label class="pull-right"> 
                        <div class="checkbox">
                            <input id="checkbox3" type="checkbox" class="pull-right">
                            <label for="checkbox3">
                                Compare
                            </label>
                        </div>
                    </label>
                </div>
                <div class="trip-info--pictures">
                    <div class="picture-boat-wrapper">
                        <img class="picture-boat" src="images/dashboard/fav3.jpg">
                    </div>
                    <img class="picture-captain" src="images/placeholders/cap1.jpg">
                </div>
                <div class="trip-info--text">
                    <div class="col-xs-12 margin-bottom-full">
                        <h2>My Great Boat</h2>
                        <label>Miami, FL</label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="no-padding left">
                            <i class="bs-icon-renter_dashboard_add-to-calendar"></i>
                        </div>
                        <div class="no-padding left">
                            <label class="label1">Date:</label><br>
                            <label class="label2">Jul 22, 2017</label>
                        </div>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="no-padding right">
                            <label class="label1">Half-Day:</label><br>
                            <label class="label2">$ 875</label>
                        </div>
                        <div class="no-padding right">
                            <i class="bs-icon-captain_dashboard_half-day"></i>
                        </div>
                    </div>
                </div>
                <div class="trip-info--btn">
                    <a class="btn btn-primary">Book this Trip</a>
                </div>
            </div>
        </div>
        <div class="trip-compare col-xs-12 col-sm-6 col-md-4 ui-draggable-handle">
            <div class="trip-info">
                <div class="trip-info--header">
                    <label class="pull-left removetag">x Remove</label>
                    <label class="pull-right"> 
                        <div class="checkbox">
                            <input id="checkbox4" type="checkbox" class="pull-right">
                            <label for="checkbox4">
                                Compare
                            </label>
                        </div>
                    </label>
                </div>
                <div class="trip-info--pictures">
                    <div class="picture-boat-wrapper">
                        <img class="picture-boat" src="images/dashboard/fav4.jpg">
                    </div>
                    <img class="picture-captain" src="images/placeholders/cap1.jpg">
                </div>
                <div class="trip-info--text">
                    <div class="col-xs-12 margin-bottom-full">
                        <h2>Terminator</h2>
                        <label>Miami, FL</label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="no-padding left">
                            <i class="bs-icon-renter_dashboard_add-to-calendar"></i>
                        </div>
                        <div class="no-padding left">
                            <label class="label1">Date:</label><br>
                            <label class="label2">Jul 22, 2017</label>
                        </div>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="no-padding right">
                            <label class="label1">Half-Day:</label><br>
                            <label class="label2">$ 875</label>
                        </div>
                        <div class="no-padding right">
                            <i class="bs-icon-captain_dashboard_half-day"></i>
                        </div>
                    </div>
                </div>
                <div class="trip-info--btn">
                    <a class="btn btn-primary">Book this Trip</a>
                </div>
            </div>
        </div>
        <div class="trip-compare col-xs-12 col-sm-6 col-md-4 ui-draggable-handle">
            <div class="trip-info">
                <div class="trip-info--header">
                    <label class="pull-left removetag">x Remove</label>
                    <label class="pull-right"> 
                        <div class="checkbox">
                            <input id="checkbox5" type="checkbox" class="pull-right">
                            <label for="checkbox5">
                                Compare
                            </label>
                        </div>
                    </label>
                </div>
                <div class="trip-info--pictures">
                    <div class="picture-boat-wrapper">
                        <img class="picture-boat" src="images/dashboard/fav1.jpg">
                    </div>
                    <img class="picture-captain" src="images/placeholders/cap1.jpg">
                </div>
                <div class="trip-info--text">
                    <div class="col-xs-12 margin-bottom-full">
                        <h2>No Dot Days</h2>
                        <label>Miami, FL</label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="no-padding left">
                            <i class="bs-icon-renter_dashboard_add-to-calendar"></i>
                        </div>
                        <div class="no-padding left">
                            <label class="label1">Date:</label><br>
                            <label class="label2">Jul 22, 2017</label>
                        </div>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="no-padding right">
                            <label class="label1">Half-Day:</label><br>
                            <label class="label2">$ 875</label>
                        </div>
                        <div class="no-padding right">
                            <i class="bs-icon-captain_dashboard_half-day"></i>
                        </div>
                    </div>
                </div>
                <div class="trip-info--btn">
                    <a class="btn btn-primary">Book this Trip</a>
                </div>
            </div>
        </div>
    </div>

</div>