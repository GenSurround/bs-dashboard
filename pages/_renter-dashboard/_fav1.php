
<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
				<img src="images/dashboard/fav1.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Friendzone Demolisher 1</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-plans clearfix">
				<div class="fav-plan">
				    <i class="bs-icon-captain_dashboard_half-day"></i>
					<h4>Half-Day<span>$875</span></h4>
				</div>
				<div class="fav-plan">
				    <i class="bs-icon-bs_full-day"></i>
					<h4>Full-Day<span>$1,275</span></h4>
				</div>
			</div>
			<span class="fav-more">More</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<div class="fav-info">
				<h2>Friendzone Demolisher</h2>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-cta">
	            <ul>
	              <li><i class="bs-icon-bs_boat-profile blue"></i>View Boat Profile </li>
	              <li><i class="bs-icon-bs_messages blue"></i><span>Message Owner</span></li>
	              <li><i class="bs-icon-bs_share blue"></i>Share with Friends</li>
	            </ul>
    		</div>
    		<span class="fav-plan-trip">Plan Trip</span>
		</div>
	</div>
</div>
<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
					<img src="images/dashboard/fav2.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Boat 2</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-plans clearfix">
				<div class="fav-plan">
				    <i class="bs-icon-captain_dashboard_half-day"></i>
					<h4>Half-Day<span>$875</span></h4>
				</div>
				<div class="fav-plan">
				    <i class="bs-icon-bs_full-day"></i>
					<h4>Full-Day<span>$1,275</span></h4>
				</div>
			</div>
			<span class="fav-more">More</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<div class="fav-info">
				<h2>Friendzone Demolisher</h2>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-cta">
	            <ul>
	              <li><i class="bs-icon-bs_boat-profile blue"></i>View Boat Profile </li>
	              <li><i class="bs-icon-bs_messages blue"></i><span>Message Owner</span></li>
	              <li><i class="bs-icon-bs_share blue"></i>Share with Friends</li>
	            </ul>
    		</div>
    		<span class="fav-plan-trip">Plan Trip</span>
		</div>
	</div>
</div>
<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
					<img src="images/dashboard/fav3.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Friendzone Demolisher 1</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-plans clearfix">
				<div class="fav-plan">
				    <i class="bs-icon-captain_dashboard_half-day"></i>
					<h4>Half-Day<span>$875</span></h4>
				</div>
				<div class="fav-plan">
				    <i class="bs-icon-bs_full-day"></i>
					<h4>Full-Day<span>$1,275</span></h4>
				</div>
			</div>
			<span class="fav-more">More</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<div class="fav-info">
				<h2>Friendzone Demolisher</h2>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-cta">
	            <ul>
	              <li><i class="bs-icon-bs_boat-profile blue"></i>View Boat Profile </li>
	              <li><i class="bs-icon-bs_messages blue"></i><span>Message Owner</span></li>
	              <li><i class="bs-icon-bs_share blue"></i>Share with Friends</li>
	            </ul>
    		</div>
    		<span class="fav-plan-trip">Plan Trip</span>
		</div>
	</div>
</div>
<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
				<img src="images/dashboard/fav4.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Friendzone Demolisher 1</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-plans clearfix">
				<div class="fav-plan">
				    <i class="bs-icon-captain_dashboard_half-day"></i>
					<h4>Half-Day<span>$875</span></h4>
				</div>
				<div class="fav-plan">
				    <i class="bs-icon-bs_full-day"></i>
					<h4>Full-Day<span>$1,275</span></h4>
				</div>
			</div>
			<span class="fav-more">More</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<div class="fav-info">
				<h2>Friendzone Demolisher</h2>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-cta">
	            <ul>
	              <li><i class="bs-icon-bs_boat-profile blue"></i>View Boat Profile </li>
	              <li><i class="bs-icon-bs_messages blue"></i><span>Message Owner</span></li>
	              <li><i class="bs-icon-bs_share blue"></i>Share with Friends</li>
	            </ul>
    		</div>
    		<span class="fav-plan-trip">Plan Trip</span>
		</div>
	</div>
</div>
<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
				<img src="images/dashboard/fav1.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Friendzone Demolisher 1</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-plans clearfix">
				<div class="fav-plan">
				    <i class="bs-icon-captain_dashboard_half-day"></i>
					<h4>Half-Day<span>$875</span></h4>
				</div>
				<div class="fav-plan">
				    <i class="bs-icon-bs_full-day"></i>
					<h4>Full-Day<span>$1,275</span></h4>
				</div>
			</div>
			<span class="fav-more">More</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<div class="fav-info">
				<h2>Friendzone Demolisher</h2>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-cta">
	            <ul>
	              <li><i class="bs-icon-bs_boat-profile blue"></i>View Boat Profile </li>
	              <li><i class="bs-icon-bs_messages blue"></i><span>Message Owner</span></li>
	              <li><i class="bs-icon-bs_share blue"></i>Share with Friends</li>
	            </ul>
    		</div>
    		<span class="fav-plan-trip">Plan Trip</span>
		</div>
	</div>
</div>
<div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="heart">
				<i class="bs-icon-renter_dashboard_heart-1 red"></i>
			</div>
			<div class="calendar fav-tooltip">
				<div>
					<h3>Last Booked</h3>
					<p>Oct. 11th, 2016</p>
				</div>
				<i class="bs-icon-renter_dashboard_booked blue"></i>
			</div>
			<figure>
				<img src="images/dashboard/fav4.jpg" alt="">
			</figure>
			<div class="fav-info">
				<h2>Friendzone Demolisher 1</h2>
				<div class="profile-items">
			        <span class="user-rating">
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <i class="fa fa-star yellow"></i>
				        <a class="reviews" href="">(11)</a>
			        </span>
		        </div>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-plans clearfix">
				<div class="fav-plan">
				    <i class="bs-icon-captain_dashboard_half-day"></i>
					<h4>Half-Day<span>$875</span></h4>
				</div>
				<div class="fav-plan">
				    <i class="bs-icon-bs_full-day"></i>
					<h4>Full-Day<span>$1,275</span></h4>
				</div>
			</div>
			<span class="fav-more">More</span>
		</div>
		<div class="back">
			<div class="flip-back">
				<i class="bs-icon-bs_left-arrow blue"></i>
			</div>
			<div class="fav-info">
				<h2>Friendzone Demolisher</h2>
				<p>Miami, Fl</p>
			</div>
			<div class="fav-cta">
	            <ul>
	              <li><i class="bs-icon-bs_boat-profile blue"></i>View Boat Profile </li>
	              <li><i class="bs-icon-bs_messages blue"></i><span>Message Owner</span></li>
	              <li><i class="bs-icon-bs_share blue"></i>Share with Friends</li>
	            </ul>
    		</div>
    		<span class="fav-plan-trip">Plan Trip</span>
		</div>
	</div>
</div>


