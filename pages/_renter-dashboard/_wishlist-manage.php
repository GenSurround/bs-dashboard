<div class="dashboard-section section-wishlist-manage">
	<div class="dashboard-section-header">	
        <h2 class="intro-text">
        <i class="bs-icon-bs_experience"></i>
        Manage Wishlist</h2>
    </div>
	<div class="row boat border-bottom margin-top-half margin-bottom-half">
		<div class="user-profile">
		    <span class="img-wrapper">
		        <img src="images/placeholders/boat.jpg" >
		    </span>
		    <span class="user-details">
		        <span>
		        	<p class="blue margin-bottom-half">Edit name:</p>
		        </span>
				<input id="name-1" class="bs-inputs editable"  type="text" value="Family Getaway">
		    </span>
		    <span class="pull-right">
                <a href="" class="blue margin-top-full pull-right"><i class="bs-icon-bs-bin bs-icon-lg"></i></a>
		    </span>
		</div>
	</div>
	<div class="row boat border-bottom margin-top-half margin-bottom-half">
		<div class="user-profile">
		    <span class="img-wrapper">
		        <img src="images/placeholders/boat.jpg" >
		    </span>
		    <span class="user-details">
		        <span>
		        	<p class="blue margin-bottom-half">Edit name:</p>
		        </span>
				<input id="name-1" class="bs-inputs editable"  type="text" value="Weekend with Friends">
		    </span>
		    <span class="pull-right">
                <a href="" class="blue margin-top-full pull-right"><i class="bs-icon-bs-bin bs-icon-lg"></i></a>
		    </span>
		</div>
	</div>
	<div class="row boat border-bottom margin-top-half margin-bottom-half">
		<div class="user-profile">
		    <span class="img-wrapper">
		        <img src="images/placeholders/boat.jpg" >
		    </span>
		    <span class="user-details">
		        <span>
		        	<p class="blue margin-bottom-half">Edit name:</p>
		        </span>
				<input id="name-1" class="bs-inputs editable"  type="text" value="Mom's Birthday">
		    </span>
		    <span class="pull-right">
                <a href="" class="blue margin-top-full pull-right"><i class="bs-icon-bs-bin bs-icon-lg"></i></a>
		    </span>
		</div>
	</div>
		<a class="btn btn-secondary pull-right margin-top-full row"> Save Updates </a>

</div>