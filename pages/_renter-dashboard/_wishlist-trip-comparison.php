<div class="dashboard-section row">
    <div class="dashboard-section-header">
        <h3 class="intro-text">
        <i class="bs-icon-bs_specifications"></i>
        Trip Comparison</h3>
        <a id="general-info-edit-trigger" class="pull-right add-more">< Back to wishlist</i></a>
    </div>

    <div class="row trip-comparison">
        <div class="col-xs-12 col-md-6">
            <div class="trip-comparison__trip">
                <div class="boat text-center  border-bottom-gray">
                    <div class="boat-photo margin-bottom-half">
                        <img width="120" height="120" src="http://bs-dashboard/images/placeholders/boat.jpg" alt="" class="img-circle">
                    </div>  
                    <h3 class="boat-name margin-bottom-quarter">
                        Friendzone Demolisher
                    </h3>
                        <p class="rating">
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <small>(6 reviews)</small>
                        </p>
                        <p class="location">
                            <i class="bs-icon-bs_location bs-icon-lg"></i>
                            Miami, FL
                        </p>
                </div>
                <div class="captain border-bottom-gray">
                        <img src="images/placeholders/boat.jpg" class="img-wrapper img-circle">
                        <div class="details text-center">
                            <p class="user-name">
                               <i class="bs-icon-members_captain-01 bs-icon-lg"></i>
                                <strong>The Captain:</strong> <br>
                               Pedro Manuel
                            </p>
                        </div>
                </div>
                <div class="itinerary border-bottom-gray">
                    <h3 class="margin-top-half margin-bottom-half"><i class="bs-icon-bs_week bs-icon-lg"></i> Itinerary <a href="#" class="small"><span class="gray">(Change)</span></a></h3>
                    <table class="table table-condensed small margin-bottom-half">
                        <tbody>
                            <tr class="light-gray">
                                <td class="black price-highlight" style="border-width:0; padding-bottom:10px"> Start Date/Time
                                </td>
                                <td class="text-right dark-gray" style="border-width:0; padding-bottom:10px">Nov 25,2016 @ 8:00 am</td>
                            </tr>
                            <tr class="light-gray">
                                <td class="black price-highlight" style="border-width:0; padding-bottom:10px"> End Date/Time
                                </td>
                                <td class="text-right dark-gray" style="border-width:0; padding-bottom:10px">Nov 25,2016 @ 12:00 pm</td>
                            </tr>
                            <tr class="light-gray">
                                <td class="black price-highlight" style="border-width:0; padding-bottom:10px"> Passengers
                                </td>
                                <td class="text-right dark-gray" style="border-width:0; padding-bottom:10px">8</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="fees">
                    <h3 class="margin-top-half margin-bottom-half"><i class="bs-icon-bs_dollar-sign bs-icon-lg"></i> Rental Fees</h3>
                    <table class="table table-condensed  small">
                        <tbody>
                            <tr class="light-gray">
                                <td class="font-size-16 black price-highlight" style="border-width:0; padding-bottom:0px; font-weight: normal"> Boat Rental
                                </td>
                                <td class="text-right font-size-16 dark-gray price-highlight" style="border-width:0; padding-bottom:0px; font-weight: normal">$500.00</td>
                            </tr>
                            <tr id="nonrefundableBookingFee" class="">
                                <td style="border-width:0px; padding-bottom:15px">
                                    Booking Fee
                                </td>
                                <td class="text-right" style="border-width:0px; padding-bottom:15px">$37.50</td>
                            </tr>
                            <tr>
                                <td >Subtotal (6%)</td>
                                <td class="text-right">$838.50</td>
                            </tr>
                            <tr>
                                <td style="border-width:0px">Sales Tax (7%)</td>
                                <td style="border-width:0px" class="text-right">$58.70</td>
                            </tr>
                            <tr>
                                <td style="border-width:0px">Insurance & Towing</td>
                                <td style="border-width:0px" class="text-right">$99.00</td>
                            </tr>

                            <tr id="fullPriceDescTotalPrice">
                                <td class="font-size-16 black price-highlight" style="padding-top:12px; font-weight: normal">Rental Total </td>
                                <td class="font-size-18 black text-right price-highlight" style="font-size:21px; font-weight: normal">$ 996.20</td>
                            </tr>
                        </tbody>
                    </table>
                    <p>
                        <a href="" class="btn btn-primary btn-block">
                            Book Now
                        </a>
                    </p>
                    <p class="refundable-deposit">
                        Refundable Deposit <i class="bs-icon-bs_interrogation bs-icon-lg blue"></i>
                        <span class="pull-right">
                            $1,000.00
                        </span>
                    </p>
                    
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 border-left-gray">
            <div class="trip-comparison__trip">
                <div class="boat text-center  border-bottom-gray">
                    <div class="boat-photo margin-bottom-half">
                        <img width="120" height="120" src="http://bs-dashboard/images/placeholders/boat.jpg" alt="" class="img-circle">
                    </div>  
                    <h3 class="boat-name margin-bottom-quarter">
                        Friendzone Demolisher
                    </h3>
                        <p class="rating">
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <i class="fa fa-star yellow"></i>
                            <small>(6 reviews)</small>
                        </p>
                        <p class="location">
                            <i class="bs-icon-bs_location bs-icon-lg"></i>
                            Miami, FL
                        </p>
                </div>
                <div class="captain border-bottom-gray">
                    <!-- <div class="row"> -->

                        <img src="images/placeholders/boat.jpg" class="img-wrapper img-circle">


                        <div class="details text-center">
                            <p class="user-name">
                               <i class="bs-icon-members_captain-01 bs-icon-lg"></i>
                                <strong>The Captain:</strong> <br>
                               Pedro Manuel
                            </p>
                        </div>
                    <!-- </div> -->

                   
                </div>
                <div class="itinerary  border-bottom-gray">
                    <h3 class="margin-top-half margin-bottom-half"><i class="bs-icon-bs_week bs-icon-lg"></i> Itinerary <a href="#" class="small"><span class="gray">(Change)</span></a></h3>
                    <table class="table table-condensed small margin-bottom-half">
                        <tbody>
                            <tr class="light-gray">
                                <td class="black price-highlight" style="border-width:0; padding-bottom:10px"> Start Date/Time
                                </td>
                                <td class="text-right dark-gray" style="border-width:0; padding-bottom:10px">Nov 25,2016 @ 8:00 am</td>
                            </tr>
                            <tr class="light-gray">
                                <td class="black price-highlight" style="border-width:0; padding-bottom:10px"> End Date/Time
                                </td>
                                <td class="text-right dark-gray" style="border-width:0; padding-bottom:10px">Nov 25,2016 @ 12:00 pm</td>
                            </tr>
                            <tr class="light-gray">
                                <td class="black price-highlight" style="border-width:0; padding-bottom:10px"> Passengers
                                </td>
                                <td class="text-right dark-gray" style="border-width:0; padding-bottom:10px">8</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="fees">
                    <h3 class="margin-top-half margin-bottom-half"><i class="bs-icon-bs_dollar-sign bs-icon-lg"></i> Rental Fees</h3>
                    <table class="table table-condensed  small">
                        <tbody>
                            <tr class="light-gray">
                                <td class="font-size-16 black price-highlight" style="border-width:0; padding-bottom:0px; font-weight: normal"> Boat Rental
                                </td>
                                <td class="text-right font-size-16 dark-gray price-highlight" style="border-width:0; padding-bottom:0px; font-weight: normal">$500.00</td>
                            </tr>
                            <tr id="nonrefundableBookingFee" class="">
                                <td style="border-width:0px; padding-bottom:15px">
                                    Booking Fee
                                </td>
                                <td class="text-right" style="border-width:0px; padding-bottom:15px">$37.50</td>
                            </tr>
                            <tr>
                                <td >Subtotal (6%)</td>
                                <td class="text-right">$838.50</td>
                            </tr>
                            <tr>
                                <td style="border-width:0px">Sales Tax (7%)</td>
                                <td style="border-width:0px" class="text-right">$58.70</td>
                            </tr>
                            <tr>
                                <td style="border-width:0px">Insurance & Towing</td>
                                <td style="border-width:0px" class="text-right">$99.00</td>
                            </tr>

                            <tr id="fullPriceDescTotalPrice">
                                <td class="font-size-16 black price-highlight" style="padding-top:12px; font-weight: normal">Rental Total </td>
                                <td class="font-size-18 black text-right price-highlight" style="font-size:21px; font-weight: normal">$ 996.20</td>
                            </tr>
                        </tbody>
                    </table>
                    <p>
                        <a href="" class="btn btn-primary btn-block">
                            Book Now
                        </a>
                    </p>
                    <p class="refundable-deposit">
                        Refundable Deposit <i class="bs-icon-bs_interrogation bs-icon-lg blue"></i>
                        <span class="pull-right">
                            $1,000.00
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>