<div class="dashboard-section renter-favorites">
	<div class="fav-header">
		<h2 class="intro-text">Favorite Boats</h2>
		<i class="bs-icon-renter_dashboard_heart-2"></i>
		<p class="intro-text">Drag and drop to reorder</p>
	</div>
	<div class="fav-container fav-boats clearfix">
	</div>
	<div class="fav-navigation">
		<ul class="fav-nav">
			<li class="active">
				<a href="pages/_renter-dashboard/_fav1.php">1</a>
			</li>
			<li>
				<a href="pages/_renter-dashboard/_fav2.php">2</a>
			</li>
			<li>
				<a href="pages/_renter-dashboard/_fav3.php">3</a>
			</li>
			<li>
				<a href="pages/_renter-dashboard/_fav4.php">4</a>
			</li>
			<li>
				<a href="pages/_renter-dashboard/_fav1.php">></a>
			</li>
			<li>
				<a href="pages/_renter-dashboard/_fav2.php">>></a>
			</li>

		</ul>
	</div>
</div>