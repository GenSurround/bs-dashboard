<div class="dashboard-section section-personal-info">
	<div class="dashboard-section-header">
		<div class="intro-text">
			<i class="bs-icon-bs_billing"></i>
			<h2>Billing Info</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc iaculis molestie metus, ac malesuada massa pulvinar at. </p>
		</div>
	</div>
	<div class="dashboard-section section-saved-credit-card">
		<div class="dashboard-section-header">
			<div class="intro-text">
				<i class="bs-icon-bs_credit-card blue"></i>
				<h3>Saved Credit Cards</h3>
			</div>
		</div>
		<div class="saved-credit-cards-list">

			<div id="no-card" class="saved-credit-card-item">
				<p class="indented-content">You have no saved cards.</p>
			</div>

			<div id="yes-card" class="saved-credit-card-item hidden">
				<div class="row indented-content saved-credit-card-item-header">
					<div class="toggle"></div>
					<div class="col-xs-12 col-xs-push-1  col-sm-5">MasterCard ending in 6558</div>
					<div class="col-xs-6 col-xs-push-1 col-sm-3">Exp Date 05/17</div>
					<div class="col-xs-6 col-xs-push-1 col-sm-3">Primary</div>

				</div>
				<div class="saved-credit-card-item-body collapse">

					<div class="indented-content row">
						<div class="col-xs-12 col-xs-push-1 col-sm-5">
							<p>
								<strong>Name on Card</strong>
							<br> Alvaro J. Morin</p>
						</div>
						<div class="col-xs-6 col-xs-push-1 col-sm-3">
							<p>
								<strong>Billing Address</strong>
								<br> 423 Street Name,
							<br> Miami, FL. 00000</p>
						</div>
						<div class="col-xs-6 col-xs-push-1 col-sm-3">Primary</div>
					</div>
					
					<div class="row indented-content  margin-top-full">
						<div class="col-xs-12 col-sm-6 margin-top-quarter">
							<input id="check1" class="simple" type="checkbox">
							<label for="check1">
								<span>Set as Primary Payment Method</span>
							</label>
						</div>
						<div class="col-xs-6 col-sm-3">
							<a class="btn btn-secondary btn-knockout btn-block">Delete</a> &nbsp;
						</div>
						<div class="col-xs-6 col-sm-3">
							<a class="btn btn-secondary btn-block ">Edit</a>
						</div>
					</div>

				</div>
			</div>
			
		</div>
		<div class="indented-content">
			<button class="btn btn-secondary btn-knockout" data-toggle="collapse" data-target="#add-card">
			Click Here to Add New Card
			</button>
		</div>
		<br>
		<div id="add-card" class="save-credit-card-form margin-top-double collapse">
			<h3>Add New Card</h3>
			<div id="card-info" class="margin-bottom-double">
				<h4>Debit or Credit Card Info</h4>
				<div class="row">
					
					<div class="col-xs-12 col-sm-6">
						<p>
							<label for="name-on-card">Name on Card</label>
							<input id="name-on-card" class="bs-inputs"  type="text" placeholder="Alvaro J. Morin">
						</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<p>
							<label for="credit-card-number">Credit Card Number</label>
							<input id="credit-card-number" class="bs-inputs"  type="text" placeholder="2880 2552 0664 6558">
						</p>
					</div>
					<div class="col-xs-6 col-sm-3">
						<p>
							<label class="bs-select">Expiration Date</label>
							<span class="bs-datepicker">
								<input id="cvv" class="bs-inputs short"  type="text" placeholder="01/10/2017">
							</span>
						</p>
					</div>
					<div class="col-xs-6 col-sm-3">
						<p>
							<label for="cvv">CVV</label>
							<input id="cvv" class="bs-inputs"  type="text" placeholder="288">
						</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<p>
							<br><br>
							<input id="check5" class="simple" type="checkbox">
							<label for="check5">
								<span>Set as Primary Payment Method</span>
							</label>
						</p>
					</div>
				</div>
			</div>
			<div id="billing-address">
				<h4>Billing Address</h4>
				<div class="row">
					<div class="col-xs-12">
						<p>
							<br><br>
							<input id="check3" class="simple" type="checkbox">
							<label for="check3">
								<span>Same address as Personal Info</span>
							</label>
						</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<p>
							<label for="street">Street</label>
							<input id="street" class="bs-inputs"  type="text" placeholder="423 Street Name">
						</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<p>
							<label for="city">City</label>
							<input id="city" class="bs-inputs"  type="text" placeholder="Miami">
						</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<p>
							<label class="bs-select">Country</label>
							<select class="cs-select no-icon cs-skin-underline">
								<option value="" disabled selected>United States of America</option>
								<option value="1">Liberia</option>
								<option value="2">Mongolia</option>
							</select>
						</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<p>
							<label class="bs-select">State</label>
							<select class="cs-select no-icon cs-skin-underline">
								<option value="" disabled selected>Florida</option>
								<option value="1">Ohio</option>
								<option value="2">Mordor</option>
							</select>
						</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<p>
							<label for="zip-code">Zip Code</label>
							<input id="zip-code" class="bs-inputs"  type="text" placeholder="00000">
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-right margin-top-full">
					<a id="save-card" class="btn btn-secondary btn-save btn-normal" >Save Card</a>
				</div>
			</div>
		</div>
	</div>
</div>