<div class="dashboard-section section-personal-info">
	<div class="dashboard-section-header user-pic">
		<div class="user-pic">
			<img class="img-circle" src="images/placeholders/cap1.jpg">
		</div>
		<div>
			<h2>
			Personal Info</h2>
			<p>Upload a great pic and tell us a bit about yourself. We're all boaters here and we love to get to know those who are passionate about boating.
			</p>
			<a class="btn btn-secondary btn-knockout">
				Edit Picture
			</a>
		</div>
	</div>
	<div class="dashboard-section">
		<div class="dashboard-section-header">
			<h3 class="intro-text">
			<i class="bs-icon-bs_tell-us blue"></i>
			Tell us about yourself
			</h3>
			<a id="general-info-edit-trigger" class="pull-right" data-toggle="collapse" data-target="#personal-bio">Edit <i class="nc-icon-outline x075 arrows-1_double-right"></i></a>
		</div>
		<p class="font-size-16">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur accusamus tempora, culpa doloremque placeat laborum sequi quo sed nulla libero quas eum non dicta maiores fugiat cum consectetur explicabo magnam enim magni minus provident! Eaque quo iure, ipsa est rerum? Commodi suscipit, dicta eius, mollitia natus tempora sint dolor, eos numquam veritatis quos.
		</p>
	</div>
	<div class="dashboard-section editable-section no-editable">
		<div class="dashboard-section-header">
			<h3 class="intro-text">
			<i class="bs-icon-bs_general-info blue"></i>
			General Info
			</h3>
			<a id="general-info-edit-trigger" class="pull-right edit-inputs">Edit <i class="nc-icon-outline x075 arrows-1_double-right"></i></a>
		</div>
		<div class="row">
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="name">Name</label>
					<input id="name" class="bs-inputs no-editable"  type="text" value="Alvaro" readonly="readonly">
				</p>
			</div>
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="last-name">Last Name</label>
					<input id="last-name" class="bs-inputs no-editable"  type="text" value="Morin" readonly="readonly">
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="primary-email">Primary Email</label>
					<input id="primary-email" class="bs-inputs no-editable"  type="mail" value="almorin@boatsetter.com" readonly="readonly">
				</p>
			</div>
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="phone-number">Phone Number</label>
					<input id="phone-number" class="bs-inputs no-editable"  type="tel" value="+1 (000) 000 0000" readonly="readonly">
				</p>
			</div>
		</div>
	</div>
	<div class="dashboard-section editable-section no-editable">
		<div class="dashboard-section-header">
			<h3 class="intro-text">
			<i class="bs-icon-bs_location blue"></i>
			Address
			</h3>
			<a id="general-info-edit-trigger" class="pull-right edit-inputs">Edit <i class="nc-icon-outline x075 arrows-1_double-right"></i></a>
		</div>
		<div class="row">
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="address-1">Street Address</label>
					<input id="address-1" class="bs-inputs no-editable"  type="text" value="lorem" readonly="readonly">
				</p>
			</div>
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="address-2">Address 2 <span class="small">(Optional)</span></label>
					<input id="address-2" class="bs-inputs no-editable"  type="text" value="Ipsum" readonly="readonly">
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-xs-6">
				<p>
					<label class="bs-select">Country</label>
					<select class="cs-select no-icon cs-skin-underline">
						<option value="" disabled selected>United States of America</option>
						<option value="1">Liberia</option>
						<option value="2">Mongolia</option>
					</select>
				</p>
			</div>
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="postal-code">Postal Code</label>
					<input id="postal-code" class="bs-inputs no-editable"  type="text" value="007" readonly="readonly">
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-xs-6">
				<p>
					<label class="bs-select">Country</label>
					<select class="cs-select no-icon cs-skin-underline">
						<option value="" disabled selected>City 17</option>
						<option value="1">Racoon City</option>
						<option value="2">Metropolis</option>
					</select>
				</p>
			</div>
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="state">State</label>
					<input id="state" class="bs-inputs no-editable"  type="text" value="Ipsum" readonly="readonly">
				</p>
			</div>
		</div>
	</div>
	<div class="dashboard-section editable-section no-editable">
		<div class="dashboard-section-header">
			<h3 class="intro-text">
			<i class="bs-icon-bs-04 blue"></i>
			Social
			</h3>
			<a id="general-info-edit-trigger" class="pull-right edit-inputs">Edit <i class="nc-icon-outline x075 arrows-1_double-right"></i></a>
		</div>
		<div class="row">
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="facebook"><i class="fa fa-lg fa-facebook blue"></i> Facebook</label>
					<input id="facebook" class="bs-inputs no-editable"  type="text" value="facebook.com/almorin" readonly="readonly">
				</p>
			</div>
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="linkedin"><i class="fa fa-lg fa-linkedin blue"></i> Facebook</label>
					<input id="linkedin" class="bs-inputs no-editable"  type="text" value="Alvaro Morin" readonly="readonly">
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="twitter"><i class="fa fa-lg fa-twitter blue"></i> Facebook</label>
					<input id="twitter" class="bs-inputs no-editable"  type="text" value="almorin" readonly="readonly">
				</p>
			</div>
			<div class="col-xs-12 col-xs-6">
				<p>
					<label for="instagram"><i class="fa fa-lg fa-instagram blue"></i> Instagram</label>
					<input id="instagram" class="bs-inputs no-editable"  type="text" value="almorin" readonly="readonly">
				</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6"><p><a class="btn btn-secondary btn-knockout">Click Here to Change Password</a></p> </div>
		<div class="col-xs-12 col-sm-6"><p class="text-right"><a class="btn btn-secondary"> Save Changes</a></p> </div>
	</div>
</div>